# MemberPort

A membership management system, written in Python with the [Tornado web-framework](https://www.tornadoweb.org/en/stable/) and [Ory Kratos](https://www.ory.sh/kratos) as the authentication and identification handler.

## Setup

### Getting the Docker images
This project uses [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/). Make sure you have installed both. Every Docker image in use can be found in the `docker-compose.yml` file.

Start by building the `memberport` docker image with the command:

```
docker buildx build -t memberport:latest .
```

This will build the image using the instructions given in `Dockerfile`. The `--no-cache` can be used if the Python dependencies have been updated. It will make Docker download them again, otherwise it will use cached versions of the dependencies.

Then you can download all the other images with:

```
docker-compose pull
```

It will download all the Docker images found in `docker-compose.yml`, it will however fail on the MemberPort image as it is built locally and cannot be downloaded. This is expected.

### Building front-end

For the front-end to display correctly it needs to be compiled. This project uses [npm](https://www.npmjs.com/). After install you need to run:

```
npm install
```

It will install all the dependencies found in the `package.json` file. To build the actual front-end you can run the command:

```
npm run build
```

### Ory Kratos Configuration

Ory Kratos will need a configuration file called `ory-config/kratos/kratos.yml`. An example file can be found at `ory-config/kratos/kratos.yml.example`. The example file includes everything but some values will need to be updated later. For now you can simply copy the example file to `ory-config/kratos/kratos.yml`.

### Running MemberPort for the first time

To start MemberPort, use the following command:

```
docker-compose up
```

By default MemberPort should be available on `http://127.0.0.1:8888/`. It won't work as expected the first time as it needs further configuration. You can stop it now with the command:

```
docker-compose down
```

After running MemberPort for the first time, a config file will be created: `config/config.ini`. In this config file you need to include the information for the database if you don't want to use the one in `docker-compose.yml` or have updated the username and password. This are the default values for the database:

```
[PostgreSQL]
hostname = postgres-db
dbname = memberportdb
username = super
password = super
```

Ory Kratos will also need an API-key for a couple of web hooks: `/new-member`, `/new-membership`. This API-key is generated by MemberPort and can be found in the `config/config.ini` file. In the config file, under `WebServer`, you will find `kratos_api_key`. The value need to be copied to the Kratos configuration, `ory-config/kratos/kratos.yml`. It should replace the text that says: `kratos_api_key`.

If you want to remove all Docker volumes afterwards or in the future then run:

```
docker-compose down -v
docker-compose rm -fsv
```

### Development

The default Docker setup is configured as a development environment currently. This means that the source files on your filesystem is shared with the Docker container. Any changes to the source code will appear after the Tornado server has restarted. By turning on debug mode, you can get more information about what the server is doing and it will restart the Tornado server automatically when it detects changes in the source code. To turn it on, edit the `config/config.ini` file:

```
debug = on
```

To help development you can also compile the front-end on every source file changed for it with the command:

```
npm run watch
```

You can also run unit tests with the command:

```
python -m app.test.runtests
```

## Contribution

This section will describe how to contribute to this project.

### Issues

Issues should be created for bug fixes, suggestions, requirements, discussions etc. Use appropiate labels for the issues.

Currently there are 7 design sections and each issue should be tagged with an appropiate section. The sections are:

- Administration, change settings, creating reports, export members etc
- Communication, sending messages to members etc
- General, features that do not fit the other sections
- Geography, municipalities, countries, areas etc
- Membership, handling of memberships in the system
- Organization, choose names for organizations, deal with settings for each organization etc
- Roles, permission management etc

### Merge requests

All changes need to be submitted though pull requests and the issue should be referenced.

- Create a new branch with a name related to the issue
- Create a merge request from the branch to the `develop` branch
- Wait for a code review
- If the changes are approved, they will be merged

### Commit messages

The commit message should follow this standard:

```
tag: Short description (issue number)

Long description
```

* tag: What type of change it is, e.g. feature, refactor, bugfix.
  - feat: new functionallity
  - fix: fixes erroneous functionallity
  - refactor: no functionallity change but nicer looking code
  - config: changes to the build process, e.g. docker etc
  - docs: documentation changes
  - ci: changes to the continuous integration
  - misc: other changes, e.g. README
* issue number: Which issue it relates to, must begin with a hashtag.
* Short description: Should not be longer than 70 characters. Should be written in imperative mood.
* Long description: OPTIONAL, if a longer description is needed write in whatever format you want.

#### Example

```
docs: Add a contribution guide to README (#3)
```

## License

MemberPort is licensed under GNU GPLv3. For more information, see the file `LICENSE`.