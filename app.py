#!/bin/env python3
"""
MemberPort - Member management system
Copyright (C) 2020 Piratpartiet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

import asyncio

from tornado.options import parse_command_line
from app.config import Config
from app.web.web_server import make_application
from app.logger import setup_logger, logger


async def main():
    parse_command_line()

    config = Config.get_config()
    debug = config.getboolean("WebServer", "debug", fallback=False)
    setup_logger(debug)
    app = await make_application()
    port = config.getint("WebServer", "port")
    app.listen(port)

    logger.info("Server was successfully started")

    await asyncio.Event().wait()


if __name__ == "__main__":
    asyncio.run(main())
