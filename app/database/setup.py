from asyncpg import create_pool, UndefinedTableError
from asyncpg.pool import Pool
from app.logger import logger
from app.web.web_app_options import WebAppOptions
from time import sleep


async def try_connect_to_database(options: WebAppOptions) -> Pool | None:
    attempts = 3
    db = None

    while attempts > 0:
        try:
            db = await connect_to_database(options)
        except Exception:
            logger.critical(
                """Error occured when trying to connect to database, check if host, name, username and password is correct in
                config/config.ini""",
                exc_info=True
            )

            if db is not None:
                await db.close()
                db = None

            attempts -= 1

            if attempts > 0:
                sleep(10.0)
                logger.critical("Retrying database connection...")
        else:
            attempts = 0

    if db is not None:
        logger.debug("Database connection initialized...")
        setup = await db_setup(db)

        if not setup:
            await db.close()
            return None
    else:
        logger.error("Database connection failed")
        logger.warning("Running without a database")
        db = None

    return db


def connect_to_database(options: WebAppOptions) -> Pool:
    if options.db_hostname == "" and options.dbname == "":
        return None

    dsn = "postgres://{username}:{password}@{hostname}/{database}".format(
        username=options.db_username,
        password=options.db_password,
        hostname=options.db_hostname,
        database=options.dbname
    )

    return create_pool(dsn, min_size=2, max_size=20, max_inactive_connection_lifetime=1800)


async def db_setup(pool: Pool) -> bool:
    new_version = get_new_version_number()
    current_version = 0

    sql = """SELECT s.version FROM mp_settings s JOIN (
                SELECT version, MAX(created) AS created
                FROM mp_settings se
                GROUP BY version
            ) lastEntry ON s.version = lastEntry.version AND s.created = lastEntry.created;"""

    try:
        async with pool.acquire() as con:
            row = await con.fetchrow(sql)

        if row["version"] is None:
            logger.info("No version info in database found, initializing new database")
            return await initialize_db(pool)
        else:
            current_version = row["version"]
            if new_version > current_version:
                logger.info("Upgrading database from version " + str(current_version) + " to version " + str(new_version))
                return await upgrade_db(pool, current_version, new_version)
            elif new_version < current_version:
                logger.critical(
                    "Database numbers mismatch, the \"new\" version number is older than the current one." +
                    "This could be because of a failed downgrade!"
                )
                raise RuntimeError(
                    "Database numbers mismatch, the \"new\" version number is older than the current one." +
                    "This could be because of a failed downgrade!"
                )
            else:
                logger.info("No action required for database")
                return True
    except UndefinedTableError:
        logger.info("No version info in database found, initializing new database")
        return await initialize_db(pool)

    return False


async def first_setup(pool: Pool) -> bool:
    sql = """SELECT s.initialized FROM mp_settings s JOIN (
                SELECT initialized, MAX(created) AS created
                FROM mp_settings se
                GROUP BY initialized
            ) lastEntry ON s.initialized = lastEntry.initialized AND s.created = lastEntry.created;"""

    try:
        async with pool.acquire() as con:
            row = await con.fetchrow(sql)
    except Exception:
        logger.critical("Could not retrieve settings from database!", exc_info=True)
        return False

    if row["initialized"] is False:
        logger.info("Starting first time setup")
        return True

    return False


def get_new_version_number() -> int:
    version = ""
    last_line = ""
    with open('app/database/sql/db.sql', 'r') as sql_file:
        last_line = sql_file.readlines()[-1]

    for char in last_line:
        if char.isdigit():
            version += char

    return int(version)


async def initialize_db(pool: Pool) -> bool:
    result = await initialize_tables(pool)
    if result is True:
        result = await initialize_geography(pool)
    if result is True:
        logger.info("Succesfully initialized new database!")
        return True
    else:
        logger.critical("Something went wrong when initializing new database!")
        return False


async def initialize_tables(pool: Pool) -> bool:
    with open('app/database/sql/db.sql', 'r') as sql_file:
        sql = sql_file.read()
        try:
            async with pool.acquire() as con:
                await con.execute(sql)
            logger.info("Succesfully initialized essential data and tables!")
        except Exception:
            logger.critical("Could not initialize database due to SQL error!", exc_info=True)
            return False

    return True


async def initialize_geography(pool: Pool) -> bool:
    with open('app/database/sql/geography.sql', 'r') as sql_file:
        sql = sql_file.read()
        try:
            async with pool.acquire() as con:
                await con.execute(sql)
            logger.info("Succesfully initialized geography data!")
        except Exception:
            logger.critical("Could not initialize database due to SQL error!", exc_info=True)
            return False

    return True


async def upgrade_db(pool: Pool, current_version: int, new_version: int) -> bool:
    for version in range(current_version, new_version):
        try:
            with open('app/database/sql/upgrades/' + str(version) + '_to_' + str(version + 1) + '.sql', 'r') as sql_file:
                sql = sql_file.read()
                try:
                    async with pool.acquire() as con:
                        await con.execute(sql)
                    logger.info("Succesfully upgraded database from version " + str(version) + " to " + str(version + 1))
                except Exception:
                    logger.critical(
                        "Could not upgrade database from version " + str(version) + " to " + str(version + 1) + " due to SQL error!",
                        exc_info=True
                    )
                    return False
        except FileNotFoundError:
            logger.critical(
                "Could not upgrade database from version " + str(version) + " to " + str(version + 1) + " due to no upgrade script found!",
                exc_info=True
            )
            return False
    return True
