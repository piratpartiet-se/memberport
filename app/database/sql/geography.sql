-- Countries
INSERT INTO mp_countries (id, name, created) OVERRIDING SYSTEM VALUE
VALUES (1, 'Sverige', localtimestamp);

SELECT setval('mp_countries_id_seq', 1);

INSERT INTO mp_countries (name, created)
VALUES ('Afghanistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Albanien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Algeriet', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Andorra', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Angola', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Antigua och Barbuda', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Argentina', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Armenien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Pakistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Australien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Azerbajdzjan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bahamas', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bahrain', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bangladesh', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Barbados', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Belgien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Belize', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Benin', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bhutan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bolivia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bosnien och Hercegovina', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Botswana', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Brasilien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Brunei', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Bulgarien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Burkina Faso', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Burundi', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Centralafrikanska republiken', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Chile', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Colombia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Costa Rica', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Cypern', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Turkiet', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Danmark', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Djibouti', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Dominica', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Dominikanska republiken', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Ecuador', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Egypten', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Ekvatorialguinea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Elfenbenskusten', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('El Salvador', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Eritrea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Estland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Etiopien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Fiji', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Filippinerna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Finland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Frankrike', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Förenade Arabemiraten', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Gabon', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Gambia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Georgien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Ghana', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Grekland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Grenada', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Guatemala', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Guinea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Guinea-Bissau', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Guyana', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Haiti', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Honduras', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Indien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Indonesien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Irak', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Iran', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Irland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Island', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Israel', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Italien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Jamaica', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Japan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Jemen', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Jordanien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kambodja', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kamerun', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kanada', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kap Verde', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kazakstan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kenya', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kina', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Taiwan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kirgizistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kiribati', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Komorerna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kongo-Brazzaville', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kongo-Kinshasa', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kroatien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kuba', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Kuwait', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Laos', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Lesotho', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Lettland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Libanon', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Liberia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Libyen', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Liechtenstein', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Litauen', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Luxemburg', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Madagaskar', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Malawi', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Malaysia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Maldiverna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Mali', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Malta', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Marocko', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Marshallöarna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Mauretanien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Mauritius', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Mexiko', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Mikronesiens federerade stater|Mikronesiska federationen', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Moçambique', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Moldavien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Monaco', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Montenegro', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Mongoliet', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Myanmar', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Namibia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nauru', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nederländerna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nepal', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nicaragua', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Niger', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nigeria', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nordkorea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sydkorea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nordmakedonien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Norge', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nya Zeeland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Oman', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Pakistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Palau', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Panama', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Papua Nya Guinea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Paraguay', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Peru', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Polen', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Portugal', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Qatar', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Rumänien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Rwanda', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Ryssland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Saint Kitts och Nevis', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Saint Lucia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Saint Vincent och Grenadinerna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Salomonöarna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Samoa', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('San Marino', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('São Tomé och Príncipe', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Saudiarabien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Schweiz', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Senegal', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Seychellerna', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Serbien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sierra Leone', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Singapore', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Slovakien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Slovenien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Somalia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Spanien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sri Lanka', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Storbritannien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sudan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Surinam', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Swaziland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sydafrika', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sydkorea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Nordkorea', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Sydsudan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Syrien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tadzjikistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tanzania', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tchad', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Thailand', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tjeckien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Togo', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tonga', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Trinidad och Tobago', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tunisien', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Turkiet', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Turkmenistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tuvalu', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Tyskland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Uganda', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Ukraina', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Ungern', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Uruguay', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('USA', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Uzbekistan', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Vanuatu', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Vatikanstaten', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Venezuela', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Vietnam', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Vitryssland', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Zambia', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Zimbabwe', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Österrike', localtimestamp);

INSERT INTO mp_countries (name, created)
VALUES ('Östtimor', localtimestamp);

-- Areas
-- Norra
INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (1, 'Norra distriktet', 1, localtimestamp, '1');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (6, 'Norrbottens län', 1, localtimestamp, '1.6');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (7, 'Västerbottens län', 1, localtimestamp, '1.7');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (8, 'Jämtlands län', 1, localtimestamp, '1.8');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (9, 'Västernorrlands län', 1, localtimestamp, '1.9');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (10, 'Gävleborgs län', 1, localtimestamp, '1.10');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (11, 'Dalarnas län', 1, localtimestamp, '1.11');

-- Östra
INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (2, 'Östra distriktet', 1, localtimestamp, '2');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (12, 'Uppsala län', 1, localtimestamp, '2.12');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (13, 'Västmanlands län', 1, localtimestamp, '2.13');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (14, 'Örebro län', 1, localtimestamp, '2.14');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (15, 'Södermanlands län', 1, localtimestamp, '2.15');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (16, 'Östergötlands län', 1, localtimestamp, '2.16');

-- Stockholm
INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (3, 'Stockholms distriktet', 1, localtimestamp, '3');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (18, 'Stockholms län', 1, localtimestamp, '3.18');

-- Västra
INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (4, 'Västra distriktet', 1, localtimestamp, '4');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (19, 'Värmlands län', 1, localtimestamp, '4.19');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (20, 'Västra Götalands län', 1, localtimestamp, '4.20');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (21, 'Hallands län', 1, localtimestamp, '4.21');

-- Södra
INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (5, 'Södra distriktet', 1, localtimestamp, '5');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (17, 'Blekinge län', 1, localtimestamp, '5.17');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (22, 'Skåne län', 1, localtimestamp, '5.22');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (23, 'Jönköpings län', 1, localtimestamp, '5.23');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (24, 'Kronobergs län', 1, localtimestamp, '5.24');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (25, 'Kalmar län', 1, localtimestamp, '5.25');

INSERT INTO mp_areas (id, name, "country", created, path) OVERRIDING SYSTEM VALUE
VALUES (26, 'Gotlands län', 1, localtimestamp, '5.26');

SELECT setval('mp_areas_id_seq', 26);

-- Municipalities
INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ale', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Alingsås', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Alvesta', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Aneby', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Arboga', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Arjeplog', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Arvidsjaur', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Arvika', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Askersund', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Avesta', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bengtsfors', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Berg', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bjurholm', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bjuv', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Boden', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bollebygd', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bollnäs', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Borgholm', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Borlänge', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Borås', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Botkyrka', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Boxholm', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bromölla', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Bräcke', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Burlöv', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Båstad', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Dals-Ed', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Danderyd', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Degerfors', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Dorotea', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Eda', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ekerö', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Eksjö', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Emmaboda', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Enköping', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Eskilstuna', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Eslöv', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Essunga', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Fagersta', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Falkenberg', localtimestamp, 1, 21);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Falköping', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Falun', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Filipstad', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Finspång', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Flen', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Forshaga', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Färgelanda', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gagnef', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gislaved', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gnesta', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gnosjö', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gotland', localtimestamp, 1, 26);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Grums', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Grästorp', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gullspång', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gällivare', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Gävle', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Göteborg', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Götene', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Habo', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hagfors', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hallsberg', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hallstahammar', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Halmstad', localtimestamp, 1, 21);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hammarö', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Haninge', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Haparanda', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Heby', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hedemora', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Helsingborg', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Herrljunga', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hjo', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hofors', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Huddinge', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hudiksvall', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hultsfred', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hylte', localtimestamp, 1, 21);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hällefors', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Härjedalen', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Härnösand', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Härryda', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hässleholm', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Håbo', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Höganäs', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Högsby', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Hörby', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Höör', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Jokkmokk', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Järfälla', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Jönköping', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kalix', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kalmar', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Karlsborg', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Karlshamn', localtimestamp, 1, 17);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Karlskoga', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Karlskrona', localtimestamp, 1, 17);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Karlstad', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Katrineholm', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kil', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kinda', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kiruna', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Klippan', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Knivsta', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kramfors', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kristianstad', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kristinehamn', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Krokom', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kumla', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kungsbacka', localtimestamp, 1, 21);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kungsör', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kungälv', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Kävlinge', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Köping', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Laholm', localtimestamp, 1, 21);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Landskrona', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Laxå', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lekeberg', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Leksand', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lerum', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lessebo', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lidingö', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lidköping', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lilla Edet', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lindesberg', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Linköping', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ljungby', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ljusdal', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ljusnarsberg', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lomma', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ludvika', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Luleå', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lund', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lycksele', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Lysekil', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Malmö', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Malung-Sälen', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Malå', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mariestad', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mark', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Markaryd', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mellerud', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mjölby', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mora', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Motala', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mullsjö', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Munkedal', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Munkfors', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mölndal', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mönsterås', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Mörbylånga', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nacka', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nora', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Norberg', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nordanstig', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nordmaling', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Norrköping', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Norrtälje', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Norsjö', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nybro', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nykvarn', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nyköping', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nynäshamn', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Nässjö', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ockelbo', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Olofström', localtimestamp, 1, 17);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Orsa', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Orust', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Osby', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Oskarshamn', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ovanåker', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Oxelösund', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Pajala', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Partille', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Perstorp', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Piteå', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ragunda', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Robertsfors', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ronneby', localtimestamp, 1, 17);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Rättvik', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sala', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Salem', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sandviken', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sigtuna', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Simrishamn', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sjöbo', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Skara', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Skellefteå', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Skinnskatteberg', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Skurup', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Skövde', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Smedjebacken', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sollefteå', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sollentuna', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Solna', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sorsele', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sotenäs', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Staffanstorp', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Stenungsund', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Stockholm', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Storfors', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Storuman', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Strängnäs', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Strömstad', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Strömsund', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sundbyberg', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sundsvall', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sunne', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Surahammar', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Svalöv', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Svedala', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Svenljunga', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Säffle', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Säter', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sävsjö', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Söderhamn', localtimestamp, 1, 10);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Söderköping', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Södertälje', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Sölvesborg', localtimestamp, 1, 17);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tanum', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tibro', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tidaholm', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tierp', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Timrå', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tingsryd', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tjörn', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tomelilla', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Torsby', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Torsås', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tranemo', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tranås', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Trelleborg', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Trollhättan', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Trosa', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Tyresö', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Täby', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Töreboda', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Uddevalla', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ulricehamn', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Umeå', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Upplands Väsby', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Upplands-Bro', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Uppsala', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Uppvidinge', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vadstena', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vaggeryd', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Valdemarsvik', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vallentuna', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vansbro', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vara', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Varberg', localtimestamp, 1, 21);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vaxholm', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vellinge', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vetlanda', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vilhelmina', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vimmerby', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vindeln', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vingåker', localtimestamp, 1, 15);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vänersborg', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vännäs', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Värmdö', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Värnamo', localtimestamp, 1, 23);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Västervik', localtimestamp, 1, 25);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Västerås', localtimestamp, 1, 13);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Växjö', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Vårgårda', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ydre', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ystad', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Åmål', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ånge', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Åre', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Årjäng', localtimestamp, 1, 19);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Åsele', localtimestamp, 1, 7);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Åstorp', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Åtvidaberg', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Älmhult', localtimestamp, 1, 24);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Älvdalen', localtimestamp, 1, 11);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Älvkarleby', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Älvsbyn', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ängelholm', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Öckerö', localtimestamp, 1, 20);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Ödeshög', localtimestamp, 1, 16);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Örebro', localtimestamp, 1, 14);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Örkelljunga', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Örnsköldsvik', localtimestamp, 1, 9);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Östersund', localtimestamp, 1, 8);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Österåker', localtimestamp, 1, 18);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Östhammar', localtimestamp, 1, 12);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Östra Göinge', localtimestamp, 1, 22);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Överkalix', localtimestamp, 1, 6);

INSERT INTO mp_municipalities (name, created, "country", "area")
VALUES ('Övertorneå', localtimestamp, 1, 6);