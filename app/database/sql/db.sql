-- Database structure for Memberport
-- SQL is written with PostgreSQL syntax

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS ltree;

CREATE TABLE mp_organizations
(
    id             UUID PRIMARY KEY,
    name           TEXT UNIQUE NOT NULL,
    description    TEXT,
    active         BOOLEAN NOT NULL,
    created        TIMESTAMP WITH TIME ZONE NOT NULL,
    show_on_signup BOOLEAN NOT NULL,
    path           ltree
);

CREATE INDEX mp_organization_path_idx ON mp_organizations USING GIST (path);

CREATE SEQUENCE mp_membernumber MINVALUE 1;

CREATE TABLE mp_memberships
(
    id             UUID UNIQUE NOT NULL,
    "organization" UUID REFERENCES mp_organizations(id),
    "user"         UUID REFERENCES identities(id),
    created        TIMESTAMP WITH TIME ZONE NOT NULL,
    renewal        TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY ("organization", "user")
);

CREATE TABLE mp_ended_memberships
(
    id             UUID PRIMARY KEY,
    "organization" UUID REFERENCES mp_organizations(id),
    reason         TEXT NOT NULL,
    ended          TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE mp_roles
(
    id          INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name        TEXT UNIQUE NOT NULL,
    description TEXT,
    created TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE mp_permissions
(
    id          TEXT PRIMARY KEY,
    name        TEXT NOT NULL
);

CREATE TABLE mp_user_roles
(
    id             UUID PRIMARY KEY,
    "user"         UUID REFERENCES identities(id) NOT NULL,
    "role"         INTEGER REFERENCES mp_roles(id) NOT NULL,
    "organization" UUID REFERENCES mp_organizations(id),
    CONSTRAINT unique_combination UNIQUE ("user", "role", "organization")
);

CREATE INDEX idx_user_role ON mp_user_roles ("user", "role");

CREATE TABLE mp_role_permissions
(
    "role" INTEGER REFERENCES mp_roles(id),
    "permission" TEXT REFERENCES mp_permissions(id),
    PRIMARY KEY ("role", "permission")
);

CREATE TABLE mp_settings
(
    initialized          BOOLEAN NOT NULL,
    created              TIMESTAMP WITH TIME ZONE NOT NULL PRIMARY KEY,
    default_organization UUID REFERENCES mp_organizations(id),
    feed_url             TEXT NOT NULL,
    version              INTEGER NOT NULL
);

CREATE TABLE mp_countries
(
    id      INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name    TEXT NOT NULL,
    created TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE mp_areas
(
    id        INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name      TEXT NOT NULL,
    "country" INTEGER REFERENCES mp_countries(id) NOT NULL,
    path      ltree,
    created   TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX mp_area_path_idx ON mp_areas USING GIST (path);

CREATE TABLE mp_municipalities
(
    id              INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name            TEXT NOT NULL,
    "country"       INTEGER REFERENCES mp_countries(id) NOT NULL,
    "area"          INTEGER REFERENCES mp_areas(id),
    created         TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE mp_organization_country
(
    "organization" UUID REFERENCES mp_organizations(id),
    "country"      INTEGER REFERENCES mp_countries(id),
    PRIMARY KEY ("organization", "country")
);

CREATE TABLE mp_organization_area
(
    "organization" UUID REFERENCES mp_organizations(id),
    "area"         INTEGER REFERENCES mp_areas(id),
    PRIMARY KEY ("organization", "area")
);

CREATE TABLE mp_organization_municipality
(
    "organization" UUID REFERENCES mp_organizations(id),
    "municipality" INTEGER REFERENCES mp_municipalities(id),
    PRIMARY KEY ("organization", "municipality")
);

CREATE TABLE mp_user_roles_country
(
    "user_role" UUID REFERENCES mp_user_roles(id),
    "country"   INTEGER REFERENCES mp_countries(id),
    PRIMARY KEY ("user_role", "country")
);

CREATE TABLE mp_user_roles_area
(
    "user_role" UUID REFERENCES mp_user_roles(id),
    "area"      INTEGER REFERENCES mp_areas(id),
    PRIMARY KEY ("user_role", "area")
);


CREATE TABLE mp_user_roles_municipality
(
    "user_role"    UUID REFERENCES mp_user_roles(id),
    "municipality" INTEGER REFERENCES mp_municipalities(id),
    PRIMARY KEY ("user_role", "municipality")
);

CREATE TABLE mp_posts
(
    id      UUID PRIMARY KEY,
    title   TEXT NOT NULL,
    content TEXT NOT NULL,
    author  UUID REFERENCES identities(id) NOT NULL,
    created TIMESTAMP WITH TIME ZONE NOT NULL,
    updated TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE mp_post_organization
(
    "post"         UUID REFERENCES mp_posts(id),
    "organization" UUID REFERENCES mp_organizations(id),
    PRIMARY KEY ("post", "organization")
);

CREATE TABLE mp_post_country
(
    "post"    UUID REFERENCES mp_posts(id),
    "country" INTEGER REFERENCES mp_countries(id),
    PRIMARY KEY ("post", "country")
);

CREATE TABLE mp_post_area
(
    "post" UUID REFERENCES mp_posts(id),
    "area" INTEGER REFERENCES mp_areas(id),
    PRIMARY KEY ("post", "area")
);

CREATE TABLE mp_post_municipality
(
    "post"         UUID REFERENCES mp_posts(id),
    "municipality" INTEGER REFERENCES mp_municipalities(id),
    PRIMARY KEY ("post", "municipality")
);

CREATE TABLE mp_ics_links
(
    id          UUID PRIMARY KEY,
    description TEXT NOT NULL,
    ics_url     TEXT NOT NULL,
    created     TIMESTAMP WITH TIME ZONE NOT NULL
);

-- Create an administrator role
INSERT INTO mp_roles (name, description, created)
VALUES ('Admin', 'Default role for admins.', localtimestamp);

INSERT INTO mp_permissions (id, name)
VALUES ('communicate_news', 'Send out information through the website');

INSERT INTO mp_permissions (id, name)
VALUES ('communicate_meeting', 'Send out call to meeting, will go to all members');

INSERT INTO mp_permissions (id, name)
VALUES ('communicate_sms', 'Send out information through SMS');

INSERT INTO mp_permissions (id, name)
VALUES ('get_members', 'Get information about other members');

INSERT INTO mp_permissions (id, name)
VALUES ('edit_members', 'Edit information about other members');

INSERT INTO mp_permissions (id, name)
VALUES ('delete_user_data', 'Delete accounts and their user data');

INSERT INTO mp_permissions (id, name)
VALUES ('edit_organizations', 'Edit, add and delete organizations');

INSERT INTO mp_permissions (id, name)
VALUES ('edit_geography', 'Edit geography tree');

INSERT INTO mp_permissions (id, name)
VALUES ('edit_calendar', 'Edit calendars');

INSERT INTO mp_permissions (id, name)
VALUES ('edit_roles', 'Edit roles');

INSERT INTO mp_permissions (id, name)
VALUES ('system', 'Overrides all permissions, only meant for system admins');

INSERT INTO mp_role_permissions ("role", "permission")
VALUES (1, 'system');

INSERT INTO mp_settings (initialized, created, default_organization, feed_url, version)
VALUES (FALSE, localtimestamp, NULL, '', 3);