from app.logger import logger

from datetime import datetime
from uuid import UUID, uuid4

from asyncpg.exceptions import UniqueViolationError

from app.models import Membership
from app.database.dao.member_org import MemberOrgDao


class MembersDao(MemberOrgDao):
    async def create_membership(self, user_id: UUID, organization_id: UUID) -> Membership | None:
        sql = "INSERT INTO mp_memberships (id, \"user\", \"organization\", created, renewal) VALUES ($1, $2, $3, $4, $5);"

        membership_id = uuid4()
        created = datetime.now()
        renewal = datetime(created.year + 1, created.month, created.day)

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, membership_id, user_id, organization_id, created, renewal)
        except UniqueViolationError as exc:
            logger.debug(str(exc))
            logger.warning("Tried to create membership with user ID: " + str(user_id) +
                           " and organization ID: " + str(organization_id) + " but it already existed")
            return None
        except Exception:
            logger.error("An error occured when trying to create new membership!", exc_info=True)
            return None

        membership = Membership()
        membership.id = membership_id
        membership.user_id = user_id
        membership.organization_id = organization_id
        membership.created = created
        membership.renewal = renewal

        return membership

    async def get_membership_by_id(self, membership_id: UUID) -> Membership | None:
        sql = 'SELECT "organization", "user", created, renewal FROM mp_memberships WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                row = await con.fetchrow(sql, membership_id)
        except Exception:
            logger.error("An error occured when trying to retrieve membership!", stack_info=True)
            return None

        membership = Membership()
        membership.id = membership_id
        membership.organization_id = row["organization"]
        membership.user_id = row["user"]
        membership.created = row["created"]
        membership.renewal = row["renewal"]

        return membership

    async def renew_membership(self, membership_id: UUID) -> Membership | None:
        sql = """
              UPDATE mp_memberships SET renewal = renewal + INTERVAL '1 year'
              WHERE EXTRACT(YEAR FROM renewal) <= EXTRACT(YEAR FROM CURRENT_DATE) AND id = $1;
              """

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, membership_id)
        except Exception:
            logger.error("An error occured when trying to renew membership!", stack_info=True)
            return None

        return await self.get_membership_by_id(membership_id)

    async def remove_membership(self, user_id: UUID, organization_id: UUID, reason: str | None) -> bool:
        sql = 'DELETE FROM mp_memberships WHERE "user" = $1 AND "organization" = $2;'
        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, user_id, organization_id)
        except Exception:
            logger.error("An error occured when trying to delete a membership!", stack_info=True)
            return False

        sql = 'INSERT INTO mp_ended_memberships (id, "organization", reason, ended) VALUES ($1, $2, $3, $4);'

        id = uuid4()
        ended = datetime.now()
        reason = "" if reason is None else reason

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, id, organization_id, reason, ended)
        except Exception:
            logger.error("An error occured when trying to delete a membership!", stack_info=True)
            return False

        return True

    async def get_member_count(self, organization_id: UUID) -> int:
        """
        Get how many users are currently registered
        :return: An int with the current user count
        """
        sql = "SELECT count(*) as members FROM mp_memberships WHERE \"organization\" = $1;"

        async with self.pool.acquire() as con:
            row = await con.fetchrow(sql, organization_id)

        return row["members"]

    async def get_memberships_for_user(self, user_id: UUID) -> list[Membership]:
        sql = "SELECT id, \"organization\", created, renewal FROM mp_memberships WHERE \"user\" = $1"

        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql, user_id)
        except Exception:
            logger.error("An error occured when trying to retrieve memberships for an user!", stack_info=True)
            return list()

        if rows is None:
            return list()

        memberships = list()

        for row in rows:
            membership = Membership()
            membership.id = row["id"]
            membership.user_id = user_id
            membership.organization_id = row["organization"]
            membership.created = row["created"]
            membership.renewal = row["renewal"]
            memberships.append(membership)

        return memberships

    async def count_expired_memberships(self) -> int:
        sql = "SELECT COUNT(\"id\") FROM mp_memberships WHERE renewal < $1;"
        current_date = datetime.now()

        try:
            async with self.pool.acquire() as con:
                count = await con.fetchval(sql, current_date)
        except Exception:
            logger.critical("An error occured when trying to count expired memberships!")
            return 0

        return count

    async def remove_expired_memberships(self) -> None:
        sql = "DELETE FROM mp_memberships WHERE renewal < $1;"
        current_date = datetime.now()

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, current_date)
        except Exception:
            logger.critical("An error occured when trying to delete expired memberships!")
            raise
