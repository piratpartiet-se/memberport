from datetime import datetime

from asyncpg.exceptions import DataError, UniqueViolationError

from app.logger import logger
from app.models import Area, Country, Municipality
from app.database.dao.base import BaseDao


class GeographyDao(BaseDao):
    async def create_area(self, name: str, country_id: int, parent_id: int | None) -> Area | None:
        sql = 'INSERT INTO mp_areas (name, created, "country") VALUES ($1, $2, $3) RETURNING id;'
        sql_set_path = 'UPDATE mp_areas SET path = $1 WHERE id = $2;'
        created = datetime.now()
        area_id = -1
        path = await self._get_path(parent_id)

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    area_id = await con.fetchval(sql, name, created, country_id)
                    path += str(area_id)
                    await con.execute(sql_set_path, path, area_id)
        except Exception as exc:
            logger.debug(exc)
            logger.error("An error occured when trying to create new area!", stack_info=True)
            return None

        if area_id == -1:
            return None

        area = Area()
        area.id = area_id
        area.name = name
        area.created = created
        area.country_id = country_id
        area.path = path

        return area

    async def delete_area(self, area_id: int) -> bool:
        sql_municipality = """
            DELETE FROM mp_municipalities WHERE "area" IN (SELECT id FROM mp_areas
            WHERE path <@ (SELECT path FROM mp_areas WHERE id = $1));
        """
        sql_area = 'DELETE FROM mp_areas WHERE path <@ (SELECT path FROM mp_areas WHERE id = $1);'

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    await con.execute(sql_municipality, area_id)
                    await con.execute(sql_area, area_id)
        except Exception:
            logger.error("An error occured when trying to delete an area!", exc_info=True)
            return False

        return True

    async def get_area_by_id(self, area_id: int) -> Area | None:
        sql = 'SELECT name, created, "country", path FROM mp_areas WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                row = await con.fetchrow(sql, area_id)
        except DataError:
            logger.error("area_id was not an integer", stack_info=True)
            return None
        except Exception:
            logger.error("An error occured when trying to retrieve area: %i", area_id, stack_info=True)
            return None

        area = None

        if row is not None:
            area = Area()
            area.id = area_id
            area.name = row["name"]
            area.created = row["created"]
            area.country_id = row["country"]
            area.path = row["path"]

        return area

    async def get_areas(self) -> list[Area]:
        sql = 'SELECT id, name, created, "country", path FROM mp_areas ORDER BY name;'

        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql)
        except Exception:
            logger.error("An error occured when trying to retrieve areas!", stack_info=True)
            return list()

        areas = []

        for row in rows:
            area = Area()
            area.id = row["id"]
            area.name = row["name"]
            area.created = row["created"]
            area.country_id = row["country"]
            area.path = row["path"]
            areas.append(area)

        return areas

    async def get_areas_by_country(self, country_id: int) -> list[Area]:
        sql = 'SELECT id, name, created, path FROM mp_areas WHERE "country" = $1 ORDER BY name;'

        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql, country_id)
        except Exception:
            logger.error("An error occured when trying to create new area!", stack_info=True)
            return list()

        areas = []

        for row in rows:
            area = Area()
            area.id = row["id"]
            area.name = row["name"]
            area.created = row["created"]
            area.country_id = country_id
            area.path = row["path"]
            areas.append(area)

        return areas

    async def get_parent_areas_from_municipality(self, municipality_id: int) -> list[int]:
        sql = 'SELECT "area" FROM mp_municipalities WHERE municipality_id = $1;'
        try:
            async with self.pool.acquire() as con:
                parent_area = await con.fetchrow(sql, municipality_id)
        except Exception:
            logger.warning("No municipality with ID: " + str(municipality_id) + " was found!")
            return list()

        sql = 'SELECT id FROM mp_areas WHERE path @> (SELECT path FROM mp_areas WHERE id = $1);'
        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql, parent_area["area"])
        except Exception:
            logger.warning("No area with ID: " + str(parent_area["area"]) + " was found!")
            return list()

        areas = list()

        for row in rows:
            areas.append(row["id"])

        return areas

    async def _get_path(self, parent_id: int | None) -> str:
        if parent_id is None:
            return ""

        path = ""
        sql = 'SELECT path FROM mp_areas WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                path = await con.fetchval(sql, parent_id)
        except Exception:
            logger.error("An error occured when trying to retrieve the path for new area!", stack_info=True)
            return ""

        path += "."
        logger.debug("Path for new area: " + path)

        return path

    async def update_area(self, area_id: int, name: str, country_id: int, path: str) -> Area | None:
        sql = 'UPDATE mp_areas SET name = $2, "country" = $3 WHERE id = $1 RETURNING created;'
        sql_path = 'SELECT path FROM mp_areas WHERE id = $1;'

        created = datetime.now()

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    created = await con.fetchval(sql, area_id, name, country_id)
                    current_path = await con.fetchval(sql_path, area_id)

                    logger.info(f"current_path: {current_path} path: {path}")

                    if path != current_path:  # Update paths for the entire branch
                        path = "" if str(area_id) == path else path.removesuffix('.' + str(area_id))
                        path_ids_str_list = path.split('.') if len(path) > 0 else []
                        path_ids_list = list(map(int, path_ids_str_list))

                        if len(path) > 0 and area_id in path_ids_list:
                            logger.error("Path: %s not allowed for area: %i\nAn area can't be a child to itself!", path, area_id)
                            raise ValueError

                        area_count = await con.fetchval('SELECT COUNT(id) FROM mp_areas WHERE id = ANY($1);', path_ids_list)
                        if area_count is not len(path_ids_list):
                            logger.error("Path: %s not allowed for area: %i\nPath must contain existing areas!", path, area_id)
                            raise ValueError

                        sql = 'UPDATE mp_areas SET path = $1 || SUBPATH(path, nlevel($2)-1) WHERE path <@ $2;'
                        await con.execute(sql, path, current_path)
        except Exception:
            logger.error(
                "Something went wrong when trying to update area with ID: " + str(area_id),
                exc_info=True
            )
            return None

        area = Area()
        area.id = area_id
        area.name = name
        area.country_id = country_id
        area.path = path
        area.created = created

        return area

    async def create_municipality(self, name: str, country_id: int, area_id: int | None) -> Municipality | None:
        sql = 'INSERT INTO mp_municipalities (name, created, "country", "area") VALUES ($1, $2, $3, $4) RETURNING id;'
        created = datetime.now()

        municipality_id = -1

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    municipality_id = await con.fetchval(sql, name, created, country_id, area_id)
        except UniqueViolationError as exc:
            logger.debug(str(exc))
            logger.warning(
                "Tried to create municipality with name: " + name + " but it already existed"
            )
            return None
        except Exception:
            logger.error("An error occured when trying to create new municipality!\n", stack_info=True)
            return None

        if municipality_id == -1:
            return None

        municipality = Municipality()
        municipality.id = municipality_id
        municipality.name = name
        municipality.created = created
        municipality.country_id = country_id
        municipality.area_id = area_id

        return municipality

    async def delete_municipality(self, municipality_id: int) -> bool:
        sql = 'DELETE FROM mp_municipalities WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, municipality_id)
        except Exception:
            logger.error("An error occured when trying to delete a municipality!", exc_info=True)
            return False

        return True

    async def get_municipality_by_id(self, municipality_id: int) -> Municipality | None:
        sql = 'SELECT name, created, "country", "area" FROM mp_municipalities WHERE id = $1;'

        async with self.pool.acquire() as con:
            row = await con.fetchrow(sql, municipality_id)

        municipality = Municipality()

        if row is not None:
            municipality.id = municipality_id
            municipality.name = row["name"]
            municipality.created = row["created"]
            municipality.country_id = row["country"]
            municipality.area_id = row["area"]
        else:
            return None

        return municipality

    async def get_municipality_by_name(self, name: str) -> Municipality | None:
        sql = 'SELECT id, created, "country", "area" FROM mp_municipalities WHERE name = $1;'

        async with self.pool.acquire() as con:
            row = await con.fetchrow(sql, name)

        municipality = Municipality()

        if row is not None:
            municipality.id = row["id"]
            municipality.name = name
            municipality.created = row["created"]
            municipality.country_id = row["country"]
            municipality.area_id = row["area"]
        else:
            return None

        return municipality

    async def get_municipalities_by_country(self, country_id: int) -> list[Municipality]:
        sql = 'SELECT id, name, created, "area" FROM mp_municipalities WHERE "country" = $1 ORDER BY name;'

        async with self.pool.acquire() as con:
            rows = await con.fetch(sql, country_id)

        municipalities = []

        for row in rows:
            municipality = Municipality()
            municipality.id = row["id"]
            municipality.name = row["name"]
            municipality.created = row["created"]
            municipality.country_id = country_id
            municipality.area_id = row["area"]
            municipalities.append(municipality)

        return municipalities

    async def get_municipalities(self) -> list[Municipality]:
        sql = 'SELECT id, name, created, "country", "area" FROM mp_municipalities ORDER BY name;'

        async with self.pool.acquire() as con:
            rows = await con.fetch(sql)

        municipalities = []

        for row in rows:
            municipality = Municipality()
            municipality.id = row["id"]
            municipality.name = row["name"]
            municipality.created = row["created"]
            municipality.country_id = row["country"]
            municipality.area_id = row["area"]
            municipalities.append(municipality)

        return municipalities

    async def update_municipality(
        self,
        municipality_id: int,
        name: str,
        country_id: int,
        area_id: int | None
    ) -> Municipality | None:
        sql = 'UPDATE mp_municipalities SET name = $1, "country" = $2, "area" = $3 WHERE id = $4 RETURNING created;'
        created = datetime.now()

        try:
            async with self.pool.acquire() as con:
                created = await con.fetchval(sql, name, country_id, area_id, municipality_id)
        except Exception:
            logger.error(
                "Something went wrong when trying to update municipality with ID: " + str(municipality_id),
                stack_info=True
            )
            return None

        municipality = Municipality()
        municipality.id = municipality_id
        municipality.name = name
        municipality.country_id = country_id
        municipality.area_id = area_id
        municipality.created = created

        return municipality

    async def create_country(self, name: str) -> Country | None:
        sql = "INSERT INTO mp_countries (name, created) VALUES($1, $2) RETURNING id;"
        created = datetime.now()
        country_id = -1

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    country_id = await con.fetchval(sql, name, created)
        except UniqueViolationError as exc:
            logger.debug(str(exc))
            logger.warning(
                "Tried to create country with name: " + name + " but it already existed"
            )
            return None
        except Exception:
            logger.error("An error occured when trying to create new country!", stack_info=True)
            return None

        if country_id == -1:
            return None

        country = Country()
        country.id = country_id
        country.name = name
        country.created = created

        return country

    async def delete_country(self, country_id: int) -> bool:
        sql_area = 'DELETE FROM mp_areas WHERE "country" = $1;'
        sql_municipality = 'DELETE FROM mp_municipalities WHERE "country" = $1;'
        sql = 'DELETE FROM mp_countries WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    await con.execute(sql_municipality, country_id)
                    await con.execute(sql_area, country_id)
                    await con.execute(sql, country_id)
        except Exception:
            logger.error("An error occured when trying to delete a country!", exc_info=True)
            return False

        return True

    async def get_country_by_id(self, country_id: int) -> Country | None:
        sql = 'SELECT name, created FROM mp_countries WHERE id = $1;'

        async with self.pool.acquire() as con:
            row = await con.fetchrow(sql, country_id)

        country = Country()

        if row is not None:
            country.id = country_id
            country.name = row["name"]
            country.created = row["created"]
        else:
            return None

        return country

    async def get_country_by_name(self, country_name: str) -> Country | None:
        sql = 'SELECT id, created FROM mp_countries WHERE name = $1;'

        async with self.pool.acquire() as con:
            row = await con.fetchrow(sql, country_name)

        country = Country()

        if row is not None:
            country.id = row["id"]
            country.name = country_name
            country.created = row["created"]
        else:
            return None

        return country

    async def get_default_country(self) -> Country | None:
        sql = 'SELECT name, created FROM mp_countries WHERE id = $1;'

        async with self.pool.acquire() as con:
            row = await con.fetchrow(sql, 1)

        country = Country()

        if row is not None:
            country.id = 1
            country.name = row["name"]
            country.created = row["created"]
        else:
            return None

        return country

    async def get_countries(self) -> list[Country]:
        sql = 'SELECT id, name, created FROM mp_countries ORDER BY name;'

        async with self.pool.acquire() as con:
            rows = await con.fetch(sql)

        countries = []

        for row in rows:
            country = Country()
            country.id = row["id"]
            country.name = row["name"]
            country.created = row["created"]

            if country.id != 1:
                countries.append(country)
            else:
                countries.insert(0, country)  # Insert default country at the beginning of the list

        return countries

    async def update_country(self, country_id: int, name: str) -> Country | None:
        sql = 'UPDATE mp_countries SET name = $2 WHERE id = $1 RETURNING created;'
        created = datetime.now()

        try:
            async with self.pool.acquire() as con:
                created = await con.fetchval(sql, country_id, name)
        except Exception:
            logger.error("Something went wrong when trying to update name for country with ID: " + str(country_id), stack_info=True)
            return None

        country = Country()
        country.id = country_id
        country.name = name
        country.created = created

        return country
