from app.logger import logger

from datetime import datetime
from uuid import UUID, uuid4
from app.models import Role, UserRole
from app.models import Permission

from asyncpg.exceptions import UniqueViolationError
from app.database.dao.base import BaseDao


class RolesDao(BaseDao):
    async def create_role(self, name: str, description: str, permissions: list[Permission]) -> Role | None:
        sql = "INSERT INTO mp_roles (name, description, created) VALUES ($1, $2, $3) RETURNING id;"

        created = datetime.now()

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    role_id = await con.fetchval(sql, name, description, created)

                    for permission in permissions:
                        sql = 'INSERT INTO mp_role_permissions ("role", "permission") VALUES ($1, $2);'
                        await con.execute(sql, role_id, permission.id)
        except UniqueViolationError:
            logger.debug("Permissions contained duplicates")
            return None
        except Exception:
            logger.error("An error occured when trying to create role %s!", name, stack_info=True)
            return None

        role = Role()
        role.id = role_id
        role.name = name
        role.description = description
        role.created = created

        return role

    async def get_role_by_id(self, role_id: int) -> Role | None:
        sql = "SELECT name, description, created FROM mp_roles WHERE id = $1;"

        try:
            async with self.pool.acquire() as con:
                row = await con.fetchrow(sql, role_id)
        except Exception:
            logger.error("An error occured when trying to retrieve role %i!", id, stack_info=True)
            return None

        if row is None:
            return None

        role = Role()
        role.id = role_id
        role.name = row["name"]
        role.description = row["description"]
        role.created = row["created"]

        return role

    async def get_roles(self) -> list:
        sql = "SELECT id, name, description, created FROM mp_roles;"

        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql)
        except Exception:
            logger.error("An error occured when trying to retrieve roles!", stack_info=True)
            return list()

        roles = list()

        for row in rows:
            role = Role()
            role.id = row["id"]
            role.name = row["name"]
            role.description = row["description"]
            role.created = row["created"]
            roles.append(role)

        return roles

    async def add_role_to_user(self, user_id: UUID, role_id: int, organization_id: UUID | None) -> bool:
        sql = 'INSERT INTO mp_user_roles (id, "user", "role", "organization") VALUES ($1, $2, $3, $4);'

        user_role_id = uuid4()

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, user_role_id, user_id, role_id, organization_id)
        except Exception:
            logger.error("An error occured when trying to add role to user!", exc_info=True)
            return False

        return True

    async def update_role(self, role_id: int, name: str, description: str, permissions: list[Permission]) -> Role | None:
        sql = "UPDATE mp_roles SET name = $2, description = $3 WHERE id = $1 RETURNING created;"

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    created = await con.fetchval(sql, role_id, name, description)

                    sql = 'DELETE FROM mp_role_permissions WHERE "role" = $1;'
                    await con.execute(sql, role_id)

                    for permission in permissions:
                        sql = 'INSERT INTO mp_role_permissions ("role", "permission") VALUES ($1, $2);'
                        await con.execute(sql, role_id, permission.id)
        except UniqueViolationError:
            logger.debug("Permissions contained duplicates")
            return None
        except Exception:
            logger.error("An error occured when trying to update role %i!", role_id, stack_info=True)
            return None

        role = Role()
        role.id = role_id
        role.name = name
        role.description = description
        role.created = created

        return role

    async def delete_role(self, role_id: int) -> bool:
        sql_permissions = 'DELETE FROM mp_role_permissions WHERE "role" = $1;'
        sql_users = 'DELETE FROM mp_user_roles WHERE "role" = $1;'
        sql_role = 'DELETE FROM mp_roles WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    await con.execute(sql_permissions, role_id)
                    await con.execute(sql_users, role_id)
                    await con.execute(sql_role, role_id)
        except Exception:
            logger.error("Something went wrong when trying to delete role with ID: %i", role_id)
            return False

        return True

    async def get_permissions(self) -> list:
        sql = "SELECT id, name FROM mp_permissions;"

        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql)
        except Exception:
            logger.error("An error occured when trying to retrieve permissions!", stack_info=True)
            return list()

        permissions = list()

        for row in rows:
            permission = Permission()
            permission.id = row["id"]
            permission.name = row["name"]
            permissions.append(permission)

        return permissions

    async def get_permissions_by_role(self, role_id: int) -> list:
        sql = """
              SELECT p.id, p.name
              FROM mp_role_permissions rp
              JOIN mp_permissions p ON rp.permission = p.id
              WHERE rp.role = $1;
              """

        try:
            async with self.pool.acquire() as con:
                rows = await con.fetch(sql, role_id)
        except Exception:
            logger.error("An error occured when trying to retrieve permissions by role!", stack_info=True)
            return list()

        permissions = list()

        for row in rows:
            permission = Permission()
            permission.id = row["id"]
            permission.name = row["name"]
            permissions.append(permission)

        return permissions

    async def add_permission_to_role(self, role_id: int, permission_id: str):
        sql = 'INSERT INTO mp_role_permissions ("role", "permission") VALUES ($1, $2);'
        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, role_id, permission_id)
        except UniqueViolationError:
            logger.debug("Permission " + permission_id + " was already added to role: " + str(role_id))
        except Exception:
            logger.error("An error occured when trying to add permission to role!", stack_info=True)

    async def remove_permission_from_role(self, role_id: int, permission_id: str):
        sql = 'DELETE FROM mp_role_permissions WHERE "role" = $1 AND "permission" = $2;'
        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, role_id, permission_id)
        except Exception:
            logger.error("An error occured when trying to remove permission from role!", stack_info=True)

    async def create_user_role(self, user_id: UUID, role_id: int, org_id: UUID | None) -> UserRole | None:
        user_role_id = uuid4()

        sql = 'INSERT INTO mp_user_roles (id, "user", "role", "organization") VALUES ($1, $2, $3, $4);'

        try:
            async with self.pool.acquire() as con:
                async with con.transaction():
                    await con.execute(sql, user_role_id, user_id, role_id, org_id)
        except UniqueViolationError:
            logger.info("User role u: %s, r: %i already existed", str(user_id), role_id)
            return None
        except Exception:
            logger.error("An error occured when trying to create user role for user: %s with role ID: %i", str(user_id), role_id)
            return None

        user_role = UserRole()
        user_role.id = user_role_id
        user_role.user_id = user_id
        user_role.role_id = role_id
        user_role.organization_id = org_id

        return user_role

    async def delete_user_role(self, user_role_id: UUID) -> bool:
        sql = 'DELETE FROM mp_user_roles WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                await con.execute(sql, user_role_id)
        except Exception:
            logger.error("An error occured when trying to delete user role %s", str(user_role_id))
            return False

        return True

    async def get_user_role_by_id(self, user_role_id: UUID) -> UserRole | None:
        sql = 'SELECT "role", "user", "organization" FROM mp_user_roles WHERE id = $1;'

        try:
            async with self.pool.acquire() as con:
                row = await con.fetchrow(sql, user_role_id)
        except Exception:
            logger.error("An error occured when trying to retrieve user role %s", str(user_role_id))
            return None

        if row is None:
            return None

        user_role = UserRole()
        user_role.id = user_role_id
        user_role.user_id = row["user"]
        user_role.role_id = row["role"]
        user_role.organization_id = row["organization"]

        return user_role

    async def get_user_roles(self) -> list[UserRole]:
        sql = 'SELECT id, "user", "role", "organization" FROM mp_user_roles;'

        async with self.pool.acquire() as con:
            rows = await con.fetch(sql)

        user_roles = list()

        for row in rows:
            user_role = UserRole()
            user_role.id = row["id"]
            user_role.user_id = row["user"]
            user_role.role_id = row["role"]
            user_role.organization_id = row["organization"]
            user_roles.append(user_role)

        return user_roles

    async def check_user_permission(self, user_id: UUID, permission_id: str) -> bool:
        sql = 'SELECT "role" FROM mp_user_roles WHERE "user" = $1;'

        async with self.pool.acquire() as con:
            rows = await con.fetch(sql, user_id)

        for role in rows:
            sql = 'SELECT "permission" FROM mp_role_permissions WHERE "role" = $1;'

            async with self.pool.acquire() as con:
                permissions = await con.fetch(sql, role["role"])

            for p in permissions:
                if permission_id == p["permission"] or p["permission"] == "system":
                    return True

        return False
