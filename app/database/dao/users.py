import json
import ory_kratos_client

from app.logger import logger

from datetime import date
from uuid import UUID

from app.models import Bot, User, Gender
from app.database.dao.base import BaseDao

from ory_kratos_client.api import identity_api
from ory_kratos_client.configuration import Configuration
from ory_kratos_client.rest import ApiException


class UsersDao(BaseDao):
    async def get_new_member_number(self, flow_id: UUID) -> int:
        logger.debug("Assign new member number for flow: " + str(flow_id))

        sql = "SELECT nextval('mp_membernumber');"

        async with self.pool.acquire() as con:
            number = await con.fetchval(sql)

        return number

    async def check_user_admin(self, user_id: UUID) -> bool:
        """
        Checks if the user is in a role that has any privileged permissions
        :returns A boolean, true if user needs access to admin view
        """

        sql = 'SELECT "role" FROM mp_user_roles WHERE "user" = $1'

        async with self.pool.acquire() as con:
            rows = await con.fetch(sql, user_id)

        for role in rows:
            sql = 'SELECT "permission" FROM mp_role_permissions WHERE "role" = $1'

            async with self.pool.acquire() as con:
                permissions = await con.fetch(sql, role["role"])

            if len(permissions) > 0:
                return True

        return False

    async def get_user_by_id(self, user_id: UUID) -> User | Bot | None:
        user: User | Bot | None = None

        configuration = Configuration()
        configuration.host = "http://pirate-kratos:4434"

        with ory_kratos_client.ApiClient(configuration) as api_client:
            api_instance = identity_api.IdentityApi(api_client)
            try:
                api_response = api_instance.get_identity(str(user_id))
                schema_id = api_response.get("schema_id")

                if schema_id == "bot":
                    user = Bot()
                    user.name = api_response["traits"]["name"]
                else:
                    user = User()
                    user.name.first = api_response["traits"]["name"]["first"]
                    user.name.last = api_response["traits"]["name"]["last"]
                    user.phone = api_response["traits"].get("phone", "")
                    user.postal_address.street = api_response["traits"]["postal_address"]["street"]
                    user.postal_address.postal_code = api_response["traits"]["postal_address"]["postal_code"]
                    user.postal_address.city = api_response["traits"]["postal_address"]["city"]
                    user.municipality_id = api_response["traits"]["municipality_id"]
                    user.gender = Gender[api_response["traits"]["gender"]]
                    user.birthday = date.fromisoformat(api_response["traits"]["birthday"])

                    metadata = api_response.get("metadata_public")
                    if metadata is not None:
                        user.number = metadata.get("member_number", -1)
                    else:
                        user.number = -1

                user.id = UUID(api_response["id"])
                user.schema = schema_id
                user.email = api_response["traits"]["email"]
                user.verified = api_response["verifiable_addresses"][0]["verified"]
                user.created = api_response["created_at"]
            except ApiException as exc:
                logger.error("Exception when calling IdentityApi->get_identity: %s\n", exc)
                return None
            except Exception as exc:
                logger.error(str(exc))
                logger.error("Something went wrong when trying to retrieve user: " + str(user_id))
                return None

        return user

    async def get_users(
        self,
        include_bots: bool = True,
        filter_user_id: list[UUID] | None = None,
        filter_email: str | None = None,
        filter_name: str | None = None,
        filter_organizations: list[UUID] | None = None
    ) -> list[User | Bot]:
        sql = """SELECT DISTINCT ON (identities.id)
                 identities.id AS id,
                 identities.schema_id AS schema_id,
                 identities.traits AS traits,
                 identities.metadata_public AS metadata_public,
                 identities.created_at AS created_at,
                 identity_verifiable_addresses.verified AS verified
                 FROM identities
                 INNER JOIN identity_verifiable_addresses ON identities.id = identity_verifiable_addresses.identity_id
                 """

        parameters = 0

        if filter_organizations is not None and len(filter_organizations) > 0:
            sql += "INNER JOIN mp_memberships ON identities.id = mp_memberships.user\n"
            sql += "WHERE mp_memberships.organization = ANY($1)\n"
            parameters += 1

        if include_bots is False:
            if parameters == 0:
                sql += "WHERE "
            else:
                sql += "AND "
            sql += "identities.schema_id = 'member'"

        sql += ";"

        async with self.pool.acquire() as con:
            if parameters > 0:
                rows = await con.fetch(sql, filter_organizations)
            else:
                rows = await con.fetch(sql)

        users = list()

        for row in rows:
            schema_id = row["schema_id"]

            traits = json.loads(row["traits"])
            user: User | Bot | None = None

            if schema_id == "bot":
                user = Bot()
                user.name = traits["name"]
            else:
                user = User()
                user.name.first = traits["name"]["first"]
                user.name.last = traits["name"]["last"]
                user.phone = traits.get("phone", "")
                user.postal_address.street = traits["postal_address"]["street"]
                user.postal_address.postal_code = traits["postal_address"]["postal_code"]
                user.postal_address.city = traits["postal_address"]["city"]
                user.municipality_id = traits["municipality_id"]
                user.gender = Gender[traits["gender"]]
                user.birthday = date.fromisoformat(traits["birthday"])

                metadata = json.loads(row["metadata_public"])
                if metadata is not None:
                    user.number = metadata.get("member_number", -1)
                else:
                    user.number = -1

            user.id = row["id"]
            user.schema = schema_id
            user.email = traits["email"]
            user.verified = row["verified"]
            user.created = row["created_at"]
            users.append(user)

        return users
