import os.path
import asyncpg
import tornado.ioloop
import tornado.web

from tornado.routing import _RuleList

from app.config import Config
from app.database.setup import first_setup, try_connect_to_database
from app.logger import logger
from app.web.handlers.admin.admin import AdminHandler
from app.web.handlers.admin.add_member import AddMemberHandler
from app.web.handlers.admin.calendar import CalendarHandler
from app.web.handlers.admin.geography import GeographyHandler
from app.web.handlers.admin.members import MembersHandler
from app.web.handlers.admin.organizations import OrganizationsHandler
from app.web.handlers.admin.add_organization import AddOrganizationHandler
from app.web.handlers.admin.edit_organization import EditOrganizationHandler
from app.web.handlers.admin.roles import RolesHandler
from app.web.handlers.api.calendar import APICalendarHandler, APICalendarsHandler
from app.web.handlers.api.feed.post import APIPostHandler
from app.web.handlers.api.geography.area import APIAreaHandler
from app.web.handlers.api.geography.areas import APIAreasHandler
from app.web.handlers.api.geography.country import APICountryHandler
from app.web.handlers.api.geography.countries import APICountriesHandler
from app.web.handlers.api.geography.municipalities import APIMunicipalitiesHandler
from app.web.handlers.api.geography.municipality import APIMunicipalityHandler
from app.web.handlers.api.geography.postal_code import APIPostalCodeHandler
from app.web.handlers.api.health import APIHealthHandler
from app.web.handlers.api.membership import APIMemberShipHandler
from app.web.handlers.api.organization import APIOrganizationHandler
from app.web.handlers.api.organizations import APIOrganizationsHandler
from app.web.handlers.api.role import APIRoleHandler
from app.web.handlers.api.roles import APIRolesHandler
from app.web.handlers.api.user import APIUserHandler
from app.web.handlers.api.users import APIUsersHandler
from app.web.handlers.api.user_memberships import APIUserMembershipsHandler
from app.web.handlers.api.user_role import APIUserRoleHandler, APIUserRolesHandler
from app.web.handlers.authentication import RecoveryHandler, SignInHandler, SignUpHandler
from app.web.handlers.error import Error404Handler, ErrorDatabase
from app.web.handlers.error_kratos import ErrorKratosHandler
from app.web.handlers.feed.feed import FeedHandler
from app.web.handlers.feed.post import PostHandler
from app.web.handlers.integrity import IntegrityHandler
from app.web.handlers.kratos import KratosHandler
from app.web.handlers.main import MainHandler
from app.web.handlers.new_member import NewMemberHandler, NewMembershipHandler
from app.web.handlers.profile import ProfileHandler
from app.web.handlers.setup import SetupHandler
from app.web.handlers.verify import VerifyHandler
from app.web.web_app_options import WebAppOptions


class Application(tornado.web.Application):
    db: asyncpg.Pool | None

    def __init__(self, options: WebAppOptions, db: asyncpg.Pool | None, initialize_admin_user: bool):
        handlers: _RuleList = [
            (r"/", MainHandler),
            (r"/kratos/(.*)", KratosHandler),
            (r"/admin", AdminHandler),
            (r"/admin/add-member", AddMemberHandler),
            (r"/admin/calendar", CalendarHandler),
            (r"/admin/geography", GeographyHandler),
            (r"/admin/members", MembersHandler),
            (r"/admin/organizations", OrganizationsHandler),
            (r"/admin/add-organization", AddOrganizationHandler),
            (r"/admin/edit-organization", EditOrganizationHandler),
            (r"/admin/roles", RolesHandler),
            (r"/api/calendar", APICalendarHandler),
            (r"/api/calendar/(?P<id>[^\/]+)", APICalendarHandler),
            (r"/api/calendars", APICalendarsHandler),
            (r"/api/membership", APIMemberShipHandler),
            (r"/api/membership/(?P<id>[^\/]+)", APIMemberShipHandler),
            (r"/api/feed/post", APIPostHandler),
            (r"/api/feed/post/(?P<id>[^\/]+)", APIPostHandler),
            (r"/api/geography/area", APIAreaHandler),
            (r"/api/geography/area/(?P<id>[^\/]+)", APIAreaHandler),
            (r"/api/geography/areas", APIAreasHandler),
            (r"/api/geography/country", APICountryHandler),
            (r"/api/geography/country/(?P<id>[^\/]+)", APICountryHandler),
            (r"/api/geography/countries", APICountriesHandler),
            (r"/api/geography/municipality", APIMunicipalityHandler),
            (r"/api/geography/municipality/(?P<id>[^\/]+)", APIMunicipalityHandler),
            (r"/api/geography/municipalities", APIMunicipalitiesHandler),
            (r"/api/geography/postal_code/(?P<postal_code>[^\/]+)", APIPostalCodeHandler),
            (r"/api/health", APIHealthHandler),
            (r"/api/organization", APIOrganizationHandler),
            (r"/api/organization/(?P<id>[^\/]+)", APIOrganizationHandler),
            (r"/api/organizations", APIOrganizationsHandler),
            (r"/api/role", APIRoleHandler),
            (r"/api/role/(?P<role_id>[^\/]+)", APIRoleHandler),
            (r"/api/roles", APIRolesHandler),
            (r"/api/user", APIUserHandler),
            (r"/api/user/memberships/(?P<id>[^\/]+)", APIUserMembershipsHandler),
            (r"/api/user/role", APIUserRoleHandler),
            (r"/api/user/role/(?P<id>[^\/]+)", APIUserRoleHandler),
            (r"/api/user/roles", APIUserRolesHandler),
            (r"/api/user/(?P<id>[^\/]+)", APIUserHandler),
            (r"/api/users", APIUsersHandler),
            (r"/auth/login", SignInHandler),
            (r"/auth/registration", SignUpHandler),
            (r"/error", ErrorKratosHandler),
            (r"/feed", FeedHandler),
            (r"/feed/post", PostHandler),
            (r"/integrity", IntegrityHandler),
            (r"/login", tornado.web.RedirectHandler, dict(url=r"/auth/login")),
            (r"/new-member", NewMemberHandler),
            (r"/new-membership", NewMembershipHandler),
            (r"/profile", ProfileHandler),
            (r"/recovery", RecoveryHandler),
            (r"/verify", VerifyHandler),
        ]

        if initialize_admin_user:
            handlers.clear()
            handlers.append((r"/kratos/(.*)", KratosHandler))
            handlers.append((r"/", SetupHandler))

        self.db = db

        if self.db is None and options.test is False:
            handlers.clear()
            handlers.append((r"/", ErrorDatabase))

        super().__init__(
            handlers,
            default_handler_class=Error404Handler,
            cookie_secret=options.cookie_secret,
            template_path=os.path.join(os.path.dirname(__file__), "..", "..", "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "..", "..", "static"),
            login_url="/auth/login",
            xsrf_cookies=options.xsrf,
            debug=options.debug
        )


async def make_application() -> Application:
    logger.info("Starting server...")

    config = Config.get_config()

    options = WebAppOptions()
    options.debug = config.getboolean("WebServer", "debug", fallback=False)
    options.https = config.getboolean("WebServer", "https", fallback=False)
    options.port = config.getint("WebServer", "port")
    options.cookie_secret = config.get("WebServer", "cookie_secret")
    options.db_username = config.get("PostgreSQL", "username")
    options.db_password = config.get("PostgreSQL", "password")
    options.db_hostname = config.get("PostgreSQL", "hostname")
    options.dbname = config.get("PostgreSQL", "dbname")

    db = None
    initialize_admin_user = False

    if options.test is False:
        db = await try_connect_to_database(options)
        if db is not None:
            initialize_admin_user = await first_setup(db)

    app = Application(options, db, initialize_admin_user)

    return app
