import tornado.web

from tornado.web import RequestHandler
from app.logger import logger


class KratosHandler(RequestHandler):
    async def prepare(self):
        """
        Do not call manually. This runs on every request before get/post/etc.
        """
        pass

    def check_xsrf_cookie(_xsrf):
        pass

    async def get(self, url: str = ""):
        url = "http://pirate-kratos:4433/" + url + "?" + self.request.query

        logger.debug("GET to Kratos: " + url)

        req = tornado.httpclient.HTTPRequest(url, follow_redirects=False, headers=self.request.headers)
        client = tornado.httpclient.AsyncHTTPClient()
        response = await client.fetch(req, raise_error=False)

        self.set_status(response.code)

        for header in response.headers:
            if header.lower() == 'content-length':
                response_body_length = len(response.body)
                header_content_length = response.headers.get(header)
                max_length = response_body_length

                if header_content_length is not None:
                    content_length = int(header_content_length)
                    max_length = max(response_body_length, content_length)

                self.set_header(header, str(max_length))
            else:
                if header.lower() == 'set-cookie':
                    cookie_strings = response.headers.get(header)

                    if cookie_strings is not None:
                        cookies: list[str] = list()
                        for cookie in cookie_strings.split(","):
                            if (cookie[0] == ' '):
                                cookies[-1] = cookies[-1] + cookie
                            else:
                                cookies.append(cookie)
                        for cookie in cookies:
                            self.add_header(header, cookie)
                elif header.lower() != 'transfer-encoding':
                    header_content = response.headers.get(header)

                    if header_content is not None:
                        self.set_header(header, header_content)

        self.write(response.body)
        self.finish()

    async def post(self, url: str = ""):
        url = 'http://pirate-kratos:4433/' + url + '?' + self.request.query

        logger.debug("POST to Kratos: " + url)

        req = tornado.httpclient.HTTPRequest(url, method='POST', body=self.request.body, request_timeout=0,
                                             follow_redirects=False, headers=self.request.headers)

        client = tornado.httpclient.AsyncHTTPClient()
        response = await client.fetch(req, raise_error=False)

        self.set_status(response.code)

        for header in response.headers:
            if header.lower() == 'content-length':
                response_body_length = len(response.body)
                header_content_length = response.headers.get(header)
                max_length = response_body_length

                if header_content_length is not None:
                    content_length = int(header_content_length)
                    max_length = max(response_body_length, content_length)

                self.set_header(header, str(max_length))
            else:
                if header.lower() == 'set-cookie':
                    cookie_strings = response.headers.get(header)

                    if cookie_strings is not None:
                        cookies: list[str] = list()
                        for cookie in cookie_strings.split(","):
                            if (cookie[0] == ' '):
                                cookies[-1] = cookies[-1] + cookie
                            else:
                                cookies.append(cookie)
                        for cookie in cookies:
                            self.add_header(header, cookie)
                elif header.lower() != 'transfer-encoding':
                    header_content = response.headers.get(header)

                    if header_content is not None:
                        self.set_header(header, header_content)

        if url.endswith("/logout"):
            self.clear_cookie("ory_kratos_session")

        self.write(response.body)
        self.finish()
