import tornado.web

from app.database.dao.roles import RolesDao
from app.web.handlers.base import BaseHandler


class RolesHandler(BaseHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = RolesDao(self.db)
        edit_roles = await dao.check_user_permission(self.current_user.user_id, "edit_roles")

        if edit_roles is False:
            return self.respond("Permission denied", 403, None, True)

        await self.render("admin/roles.html", admin=True, title="Roller")
