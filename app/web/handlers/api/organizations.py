import tornado.web

from app.database.dao.geography import GeographyDao
from app.database.dao.organizations import OrganizationsDao
from app.models import organization_to_json
from app.web.handlers.base import APIHandler


class APIOrganizationsHandler(APIHandler):
    @tornado.web.authenticated
    async def get(self):
        municipality_id = self.get_argument("municipality", None)

        if municipality_id is not None:
            try:
                municipality_id = int(municipality_id)

                geo_dao = GeographyDao(self.db)

                municipality = await geo_dao.get_municipality_by_id(municipality_id)
                country = await geo_dao.get_country_by_id(municipality.country_id)
                areas = await geo_dao.get_parent_areas_from_municipality(municipality.id)

                org_dao = OrganizationsDao(self.db)
                organizations = await org_dao.get_organizations_in_area(country.id, areas, municipality.id)
            except ValueError:
                return self.respond("MUNICIPALITY ID WAS NOT AN INTEGER", 400)
            except Exception:
                return self.respond("MUNICIPALITY WAS NOT FOUND", 404)
        else:
            organizations = await OrganizationsDao(self.db).get_organizations()

        json_organizations = dict()

        for organization in organizations:
            json_org = organization_to_json(organization)
            json_organizations[json_org["id"]] = json_org

        return self.respond("RETRIEVED ORGANIZATIONS", 200, json_organizations)
