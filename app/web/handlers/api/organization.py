import tornado.web

from app.web.handlers.base import APIHandler, has_permissions
from app.database.dao.organizations import OrganizationsDao
from app.logger import logger
from app.models import organization_to_json
from uuid import UUID


class APIOrganizationHandler(APIHandler):
    @tornado.web.authenticated
    async def get(self, id: str):
        org_id = self.check_uuid(id)
        if org_id is None:
            return self.respond("ORGANIZATION UUID IS MISSING", 400)

        organization = await OrganizationsDao(self.db).get_organization_by_id(org_id)

        if organization is None:
            return self.respond("ORGANIZATION NOT FOUND", 404)

        return self.respond("RETRIEVED ORGANIZATION", 200, organization_to_json(organization))

    @tornado.web.authenticated
    @has_permissions("create_organizations")
    async def post(self):
        name = self.get_argument("name")
        description = self.get_argument("description")
        parent_id = self.get_argument("parent_id", None)
        parent_id = self.check_uuid(parent_id)
        active = self.get_argument("active", False)
        active = active == 'true'
        show_on_signup = self.get_argument("show_on_signup", False)
        show_on_signup = show_on_signup == 'true'

        org_dao = OrganizationsDao(self.db)

        organization = await org_dao.create_organization(name, description, active, show_on_signup, parent_id)

        if organization is None:
            return self.respond("SOMETHING WENT WRONG WHEN TRYING TO CREATE ORGANIZATION", 500, None)

        countries_id = self.get_argument("countries", None)
        areas_id = self.get_argument("areas", None)
        municipalities_id = self.get_argument("municipalities", None)

        if countries_id is not None or areas_id is not None or municipalities_id is not None:
            try:
                countries_id = list(map(int, countries_id.split(','))) if countries_id != "" and countries_id is not None else list()
                areas_id = list(map(int, areas_id.split(','))) if areas_id != "" and areas_id is not None else list()

                if municipalities_id and municipalities_id is not None != "":
                    municipalities_id = list(map(int, municipalities_id.split(',')))
                else:
                    municipalities_id = list()

            except ValueError:
                logger.error(
                    "Recruitment areas contained invalid ID when trying to set them for org: %s",
                    str(organization.id),
                    stack_info=True
                )
                return self.respond("COUNTRY, AREA AND MUNICIPALITY ID NEEDS TO BE INTEGERS", 400, None)

            success = await org_dao.set_recruitment_areas(organization.id, countries_id, areas_id, municipalities_id)
            if success is False:
                return self.respond("SOMETHING WENT WRONG WHEN TRYING TO SET RECRUITMENT AREAS", 500, None)

        return self.respond("ORGANIZATION CREATED", 200, organization_to_json(organization))

    @tornado.web.authenticated
    @has_permissions("edit_organizations")
    async def put(self, id: str):
        org_id = self.check_uuid(id)
        if org_id is None:
            return self.respond("ORGANIZATION UUID IS MISSING", 400)

        name = self.get_argument("name", None)
        description = self.get_argument("description", None)
        active_str = self.get_argument("active", "false")
        active = active_str == "true"
        show_on_signup_str = self.get_argument("show_on_signup", "false")
        show_on_signup = show_on_signup_str == "true"
        update_parent = False

        parent_id_str = self.get_argument("parent_id", None)
        parent_id: UUID | None = None

        if parent_id_str is not None:
            update_parent = True

            parent_id = self.check_uuid(parent_id_str)
            if parent_id is None:
                return self.respond("PARENT ID WAS NOT A VALID UUID", 400)

        if name is None:
            return self.respond("NAME IS MISSING", 422)
        if description is None:
            return self.respond("DESCRIPTION IS MISSING", 422)

        countries_id_str = self.get_argument("countries", None)
        areas_id_str = self.get_argument("areas", None)
        municipalities_id_str = self.get_argument("municipalities", None)

        org_dao = OrganizationsDao(self.db)

        if countries_id_str is not None or areas_id_str is not None or municipalities_id_str is not None:
            try:
                if countries_id_str != "" and countries_id_str is not None:
                    countries_id = list(map(int, countries_id_str.split(',')))
                else:
                    countries_id = list()

                if areas_id_str != "" and areas_id_str is not None:
                    areas_id = list(map(int, areas_id_str.split(',')))
                else:
                    areas_id = list()

                if municipalities_id_str != "" and municipalities_id_str is not None:
                    municipalities_id = list(map(int, municipalities_id_str.split(',')))
                else:
                    municipalities_id = list()

            except ValueError:
                logger.error(
                    "Recruitment areas contained invalid values when trying to set them for org: %s",
                    str(org_id),
                    stack_info=True
                )
                return self.respond("COUNTRY, AREA AND MUNICIPALITY ID NEEDS TO BE INTEGERS", 400, None)

            success = await org_dao.set_recruitment_areas(org_id, countries_id, areas_id, municipalities_id)
            if success is False:
                return self.respond("SOMETHING WENT WRONG WHEN TRYING TO SET RECRUITMENT AREAS", 500, None)

        organization = await org_dao.update_organization(
            org_id,
            name,
            description,
            active,
            show_on_signup,
            update_parent,
            parent_id
        )
        if organization is None:
            return self.respond("SOMETHING WENT WRONG WHEN TRYING TO UPDATE ORGANIZATION", 500, None)

        return self.respond("ORGANIZATION UPDATED", 200, organization_to_json(organization))

    @tornado.web.authenticated
    @has_permissions("delete_organizations")
    async def delete(self, id: str):
        org_id = self.check_uuid(id)
        if org_id is None:
            return self.respond("ORGANIZATION UUID IS MISSING", 400)
        if not await OrganizationsDao(self.db).delete_organization(org_id):
            return self.respond("INTERNAL SERVER ERROR", 500)
        return self.respond("ORGANIZATION DELETED", 204)
