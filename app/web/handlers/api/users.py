import tornado.web

from app.database.dao.users import UsersDao
from app.models import bot_to_json, user_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIUsersHandler(APIHandler):
    @tornado.web.authenticated
    @has_permissions("get_members")
    async def get(self):
        dao = UsersDao(self.db)

        include_bots = self.get_argument("bots", None)
        organization_ids = self.get_argument("organization_ids", [])

        if organization_ids != []:
            organization_ids = organization_ids.split(",")

        for i in range(0, len(organization_ids)):
            result = self.check_uuid(organization_ids[i])

            if result is None:
                organization_ids = []
                break
            else:
                organization_ids[i] = result

        if include_bots is None:
            include_bots = False
        elif include_bots == "true":
            include_bots = True

        users = await dao.get_users(include_bots=include_bots, filter_organizations=organization_ids)
        response = list()

        for user in users:
            if user.schema == "bot":
                response.append(bot_to_json(user))
            else:
                response.append(user_to_json(user))

        return self.respond("USERS RETURNED", 200, response)
