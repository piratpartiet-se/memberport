import tornado.web

from app.database.dao.roles import RolesDao
from app.logger import logger
from app.models import user_role_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIUserRoleHandler(APIHandler):
    @tornado.web.authenticated
    @has_permissions("get_members")
    async def get(self, id: str):
        user_role_id = self.check_uuid(id)

        if user_role_id is None:
            return self.respond("USER ROLE ID WAS NOT VALID", 400, None)

        dao = RolesDao(self.db)
        user_role = await dao.get_user_role_by_id(user_role_id)

        if user_role is None:
            return self.respond("USER ROLE NOT FOUND", 404, None)

        return self.respond("USER ROLE RETURNED", 200, user_role_to_json(user_role))

    @tornado.web.authenticated
    @has_permissions("get_members", "edit_roles")
    async def post(self):
        role_id_str = self.args.get("role_id", "")
        user_id_str = self.args.get("user_id", "")
        org_id_str = self.args.get("organization_id", "")

        if role_id_str == "" or user_id_str == "":
            return self.respond("ROLE OR USER ID WAS MISSING", 400, None)

        org_id = self.check_uuid(org_id_str)

        try:
            role_id = int(role_id_str)
            user_id = self.check_uuid(user_id_str)

            if user_id is None:
                raise ValueError
        except ValueError:
            return self.respond("ROLE ID OR USER ID WAS NOT VALID", 400, None)

        try:
            org_id = self.check_uuid(org_id)
        except ValueError:
            return self.respond("GEODATA ID WAS NOT AN INTEGER", 400, None)

        role_dao = RolesDao(self.db)
        user_role = await role_dao.create_user_role(user_id, role_id, org_id)

        if user_role is None:
            return self.respond("ROLE WAS ALREADY ASSIGNED TO USER", 400, None)

        response = user_role_to_json(user_role)

        return self.respond("USER ROLE CREATED", 201, response)

    @tornado.web.authenticated
    @has_permissions("get_members", "edit_roles")
    async def delete(self, id: str):
        user_role_id = self.check_uuid(id)

        if user_role_id is None:
            return self.respond("USER ROLE ID WAS NOT VALID", 400, None)

        role_dao = RolesDao(self.db)
        result = await role_dao.delete_user_role(user_role_id)

        if result is False:
            logger.critical("SOMETHING WENT WRONG WHEN TRYING TO DELETE USER ROLE")

        return self.respond("USER ROLE DELETED", 200, None)


class APIUserRolesHandler(APIHandler):
    @tornado.web.authenticated
    @has_permissions("get_members")
    async def get(self):
        role_dao = RolesDao(self.db)

        user_roles = await role_dao.get_user_roles()
        response = list()

        for user_role in user_roles:
            json_data = user_role_to_json(user_role)
            response.append(json_data)

        return self.respond("USER ROLES RETURNED", 200, response)
