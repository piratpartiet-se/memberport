import tornado.web

from app.database.dao.members import MembersDao
from app.database.dao.roles import RolesDao
from app.models import Session, membership_to_json
from app.web.handlers.base import APIHandler


class APIUserMembershipsHandler(APIHandler):
    @tornado.web.authenticated
    async def get(self, id: str):
        user_id = self.check_uuid(id)
        if user_id is None:
            return self.respond("USER UUID IS MISSING", 400)

        current_user: Session = self.current_user  # type: ignore  # user is logged in here

        if user_id != current_user.user_id:
            roles_dao = RolesDao(self.db)

            result = await roles_dao.check_user_permission(current_user.user_id, "get_members")

            if result is False:
                return self.respond("PERMISSION DENIED", 403)

        member_dao = MembersDao(self.db)
        memberships = await member_dao.get_memberships_for_user(user_id)
        memberships_json = list()

        for membership in memberships:
            memberships_json.append(membership_to_json(membership))

        return self.respond("MEMBERSHIPS RETURNED", 200, memberships_json)
