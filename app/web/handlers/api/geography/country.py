import tornado

from app.database.dao.geography import GeographyDao
from app.models import country_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APICountryHandler(APIHandler):
    async def get(self, id: str):
        try:
            country_id = int(id)
        except Exception:
            return self.respond("COUNTRY ID IS MISSING", 400)

        geo_dao = GeographyDao(self.db)

        country = await geo_dao.get_country_by_id(country_id)

        if country is None:
            return self.respond("COUNTRY WITH SPECIFIED ID NOT FOUND", 404, None)

        return self.respond("RETRIEVED COUNTRY", 200, country_to_json(country))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def post(self):
        country_name = self.args.get("name", "")

        if country_name == "":
            return self.respond("COUNTRY NAME IS MISSING", 400)

        geo_dao = GeographyDao(self.db)
        country = await geo_dao.create_country(country_name)

        if country is None:
            return self.respond("SOMETHING WENT WRONG WHEN TRYING TO CREATE COUNTRY", 500)

        return self.respond("COUNTRY CREATED", 201, country_to_json(country))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def put(self, id: str):
        try:
            country_id = int(id)
        except Exception:
            return self.respond("COUNTRY ID IS MISSING", 400)

        name = self.args.get("name", "")

        if name == "":
            return self.respond("NAME WAS MISSING", 400)

        geo_dao = GeographyDao(self.db)

        country = await geo_dao.update_country(country_id, name)

        if country is None:
            return self.respond("COUNTRY NOT FOUND", 404)

        return self.respond("COUNTRY UPDATED", 200, country_to_json(country))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def delete(self, id: str):
        try:
            country_id = int(id)
        except Exception:
            return self.respond("COUNTRY ID IS MISSING", 400)

        geo_dao = GeographyDao(self.db)

        result = await geo_dao.delete_country(country_id)
        if result is False:
            return self.respond("COULD NOT DELETE COUNTRY! ORGANIZATION COULD BE ACTIVE IN COUNTRY", 403)

        return self.respond("COUNTRY DELETED", 204)
