import tornado

from app.database.dao.geography import GeographyDao
from app.models import area_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIAreaHandler(APIHandler):
    async def get(self, id: str):
        try:
            area_id = int(id)
        except ValueError:
            return self.respond("AREA ID WAS INVALID", 400)

        geo_dao = GeographyDao(self.db)

        area = await geo_dao.get_area_by_id(area_id)

        if area is None:
            return self.respond("AREA NOT FOUND", 404, None)

        return self.respond("RETRIEVED AREA", 200, area_to_json(area))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def post(self):
        area_name = self.args.get("name", "")
        country_id_str = self.args.get("country_id", "")
        parent_id_str = self.args.get("parent_id", "")

        if area_name == "" or country_id_str == "":
            return self.respond("NAME OR COUNTRY ID IS MISSING", 400)

        try:
            country_id = int(country_id_str)
            if parent_id_str != "":
                parent_id = int(parent_id_str)
            else:
                parent_id = None
        except Exception:
            return self.respond("PARENT AND COUNTRY ID MUST BE INTEGERS", 400)

        geo_dao = GeographyDao(self.db)
        area = await geo_dao.create_area(area_name, country_id, parent_id)

        if area is None:
            return self.respond("SOMETHING WENT WRONG WHEN TRYING TO CREATE AREA", 500)

        return self.respond("AREA CREATED", 201, area_to_json(area))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def put(self, id: str):
        name = self.args.get("name", "")
        path = self.args.get("path", "")
        country_id_str = self.args.get("country_id", "")

        try:
            area_id = int(id)
            country_id = int(country_id_str)
        except Exception:
            return self.respond("NOT A VALID AREA OR COUNTRY ID", 400)

        if name is None or name == "" or path is None:
            return self.respond("NOT A VALID NAME", 400)

        geo_dao = GeographyDao(self.db)

        area = await geo_dao.update_area(area_id, name, country_id, path)

        if area is None:
            return self.respond("AREA WAS NOT FOUND", 404, None)

        return self.respond("AREA UPDATED", 200, area_to_json(area))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def delete(self, id: str):
        if id is None or id == "":
            return self.respond("AREA UUID IS MISSING", 400)

        try:
            area_id = int(id)
        except Exception:
            return self.respond("NOT A VALID AREA ID", 400)

        geo_dao = GeographyDao(self.db)

        result = await geo_dao.delete_area(area_id)
        if result is False:
            return self.respond("COULD NOT DELETE AREA! ORGANIZATION COULD BE ACTIVE IN AREA", 403)

        return self.respond("AREA DELETED", 204)
