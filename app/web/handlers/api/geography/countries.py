from app.database.dao.geography import GeographyDao
from app.models import country_to_json
from app.web.handlers.base import APIHandler


class APICountriesHandler(APIHandler):
    async def get(self):
        geo_dao = GeographyDao(self.db)
        countries = await geo_dao.get_countries()

        response = {}

        for country in countries:
            response[str(country.id)] = country_to_json(country)

        return self.respond("RETRIEVED COUNTRIES", 200, response)
