import tornado

from app.database.dao.geography import GeographyDao
from app.models import municipality_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIMunicipalitiesHandler(APIHandler):
    async def get(self):
        country = self.get_argument("country", None)

        geo_dao = GeographyDao(self.db)

        if country is None:
            municipalities = await geo_dao.get_municipalities()
        else:
            try:
                country_id = int(country)
            except Exception:
                country = await geo_dao.get_country_by_name(country)

                if country is None:
                    return self.respond("COUNTRY ID IS NOT AN INTEGER OR A MATCHING NAME", 400)
                else:
                    country_id = country.id

            municipalities = await geo_dao.get_municipalities_by_country(country_id)

        response = {}

        for municipality in municipalities:
            response[str(municipality.id)] = municipality_to_json(municipality)

        return self.respond("MUNICIPALITIES SUCCESSFULLY RETRIEVED", 200, response)

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def put(self):
        dao = GeographyDao(self.db)
        updated_municipalities = {}
        data = self.args

        for municipality_id in data:
            area_id = None
            country_id = None
            name = ""

            if "name" not in data[municipality_id] or "country_id" not in data[municipality_id]:
                continue

            try:
                municipality_id_int = int(municipality_id)
                country_id = int(data[municipality_id]["country_id"])
                name = data[municipality_id]["name"]

                if "area_id" in data[municipality_id]:
                    area_id = int(data[municipality_id]["area_id"])
                else:
                    area_id = None
            except ValueError:
                return self.respond("COUNTRY, AREA AND MUNICIPALITY ID NEEDS TO BE AN INTEGER", 400)

            result = await dao.update_municipality(municipality_id_int, name, country_id, area_id)
            if not result:
                return self.respond("SOMETHING WENT WRONG WHEN TRYING TO UPDATE MUNICIPALITIES", 500, updated_municipalities)
            else:
                municipality = await dao.get_municipality_by_id(municipality_id_int)
                if municipality is not None:
                    updated_municipalities[municipality_id] = municipality_to_json(municipality)

        return self.respond("MUNICIPALITIES UPDATED", 200, updated_municipalities)
