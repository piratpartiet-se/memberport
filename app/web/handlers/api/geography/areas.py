import tornado

from app.database.dao.geography import GeographyDao
from app.models import area_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIAreasHandler(APIHandler):
    @tornado.web.authenticated
    async def get(self):
        country = self.get_argument("country", None)

        dao = GeographyDao(self.db)

        if country is None:
            areas = await dao.get_areas()
        else:
            try:
                country_id = int(country)
            except Exception:
                country = await dao.get_country_by_name(country)

                if country is None:
                    return self.respond("COUNTRY ID IS NOT AN INTEGER OR A MATCHING NAME", 400)
                else:
                    country_id = country.id

            areas = await dao.get_areas_by_country(country_id)

        response = {}

        for area in areas:
            response[str(area.id)] = area_to_json(area)

        return self.respond("AREAS SUCCESSFULLY RETRIEVED", 200, response)

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def put(self):
        dao = GeographyDao(self.db)
        updated_areas = {}
        data = self.args

        for area_id_str in data:
            if "path" not in data[area_id_str] or "name" not in data[area_id_str] or "country_id" not in data[area_id_str]:
                continue

            try:
                area_id = int(area_id_str)
                country_id = int(data[area_id_str]["country_id"])
                name = data[area_id_str]["name"]
                path = data[area_id_str]["path"]
            except Exception:
                return self.respond("NOT A VALID AREA OR COUNTRY ID", 400)

            result = await dao.update_area(area_id, name, country_id, path)
            if not result:
                return self.respond("SOMETHING WENT WRONG WHEN TRYING TO UPDATE AREAS", 500, None)

        for area_id in data:
            area = await dao.get_area_by_id(int(area_id))
            if area is not None:
                updated_areas[area_id] = area_to_json(area)
            else:
                return self.respond("COULD NOT RETRIEVE UPDATED AREAS, SOMETHING COULD HAVE GONE WRONG", 500, None)

        return self.respond("AREAS UPDATED", 200, updated_areas)
