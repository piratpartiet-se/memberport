import tornado

from app.database.dao.geography import GeographyDao
from app.models import municipality_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIMunicipalityHandler(APIHandler):
    async def get(self, id: str):
        try:
            municipality_id = int(id)
        except Exception:
            return self.respond("MUNICIPALITY ID IS MISSING", 400)

        geo_dao = GeographyDao(self.db)

        municipality = await geo_dao.get_municipality_by_id(municipality_id)

        if municipality is None:
            return self.respond("MUNICIPALITY WITH SPECIFIED ID NOT FOUND", 404, None)

        return self.respond("RETRIEVED MUNICIPALITY", 200, municipality_to_json(municipality))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def post(self):
        municipality_name = self.args.get("name", None)
        country_id = self.args.get("country_id", None)
        area_id = self.args.get("area_id", "")

        if municipality_name is None or country_id is None or len(municipality_name) < 1:
            return self.respond("MUNICIPALITY OR COUNTRY ID IS MISSING", 400)

        try:
            if area_id != "":
                area_id = int(area_id)
            else:
                area_id = None

            country_id = int(country_id)
        except ValueError:
            return self.respond("AREA AND COUNTRY ID MUST BE AN INTEGER", 400)

        geo_dao = GeographyDao(self.db)
        municipality = await geo_dao.create_municipality(municipality_name, country_id, area_id)

        if municipality is None:
            return self.respond("SOMETHING WENT WRONG WHEN TRYING TO CREATE MUNICIPALITY", 500)

        return self.respond("MUNICIPALITY CREATED", 201, municipality_to_json(municipality))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def put(self, id: str):
        try:
            municipality_id = int(id)
        except Exception:
            return self.respond("MUNICIPALITY ID IS MISSING", 400)

        name = self.args.get("name", "")
        country_id_str = self.args.get("country_id", "")
        area_id_str = self.args.get("area_id", "")

        if name == "":
            return self.respond("NOT A VALID NAME", 400)
        if country_id_str == "":
            return self.respond("COUNTRY ID WAS MISSING", 400)

        try:
            country_id = int(country_id_str)

            if area_id_str != "":
                area_id = int(area_id_str)
            else:
                area_id = None
        except Exception:
            return self.respond("COUNTRY AND AREA ID NEED TO BE INTEGERS", 400)

        geo_dao = GeographyDao(self.db)

        municipality = await geo_dao.update_municipality(municipality_id, name, country_id, area_id)

        if municipality is None:
            return self.respond("MUNICIPALITY NOT FOUND", 404)

        return self.respond("MUNICIPALITY UPDATED", 200, municipality_to_json(municipality))

    @tornado.web.authenticated
    @has_permissions("edit_geography")
    async def delete(self, id: str):
        try:
            municipality_id = int(id)
        except Exception:
            return self.respond("MUNICIPALITY ID IS MISSING", 400)

        geo_dao = GeographyDao(self.db)

        result = await geo_dao.delete_municipality(municipality_id)
        if result is False:
            return self.respond("COULD NOT DELETE MUNICIPALITY! ORGANIZATION COULD BE ACTIVE IN MUNICIPALITY", 403)

        return self.respond("MUNICIPALITY DELETED", 204)
