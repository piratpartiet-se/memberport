import ory_kratos_client
import tornado.web

from app.database.dao.geography import GeographyDao
from app.database.dao.users import UsersDao
from app.logger import logger
from app.models import Bot, bot_to_json, kratos_user_to_python, user_to_json
from app.web.handlers.base import APIHandler, has_permissions

from ory_kratos_client.api import identity_api
from ory_kratos_client.configuration import Configuration
from ory_kratos_client.model.create_identity_body import CreateIdentityBody
from ory_kratos_client.model.identity_state import IdentityState
from ory_kratos_client.model.update_identity_body import UpdateIdentityBody


class APIUserHandler(APIHandler):
    @tornado.web.authenticated
    @has_permissions("get_members")
    async def get(self, id: str):
        user_id = self.check_uuid(id)
        if user_id is None:
            return self.respond("USER UUID IS MISSING", 400)

        dao = UsersDao(self.db)

        user = await dao.get_user_by_id(user_id)

        if user is None:
            return self.respond("USER NOT FOUND", 404)

        if isinstance(user, Bot):
            response = bot_to_json(user)
        else:
            response = user_to_json(user)

        return self.respond("USER RETURNED", 200, response)

    @tornado.web.authenticated
    @has_permissions("create_members")
    async def post(self):
        try:
            first = self.args["name"]["first"]
            last = self.args["name"]["last"]
            email = self.args["email"]
            phone = self.args.get("phone", None)
            street = self.args["postal_address"]["street"]
            postal_code = self.args["postal_address"]["postal_code"]
            city = self.args["postal_address"]["city"]
            municipality_id = int(self.args["municipality_id"])
            gender = self.args["gender"]
            birthday = self.args["birthday"]

            geo_dao = GeographyDao(self.db)
            municipality = await geo_dao.get_municipality_by_id(municipality_id)

            if municipality is None:
                return self.respond("MUNICIPALITY NOT FOUND", 404)
        except Exception as exc:
            logger.error("Could not load data into JSON object, bad request?")
            logger.debug(exc)
            return self.respond("WRONG DATA FORMAT", 400)

        configuration = Configuration(
            host="http://pirate-kratos:4434"
        )

        dao = UsersDao(self.db)

        traits = {
            "name": {
                "first": first,
                "last": last
            },
            "email": email,
            "municipality_id": municipality_id,
            "postal_address": {
                "street": street,
                "postal_code": postal_code,
                "city": city
            },
            "gender": gender,
            "birthday": birthday
        }

        if phone is not None and phone != "":
            traits["phone"] = phone

        create_identity_body = CreateIdentityBody(
            metadata_admin=None,
            metadata_public={
                "member_number": str(await dao.get_new_member_number(self.current_user.id))
            },
            schema_id="member",
            state=IdentityState("active"),
            traits=traits
        )

        with ory_kratos_client.ApiClient(configuration) as api_client:
            api_instance = identity_api.IdentityApi(api_client)
            try:
                api_response = api_instance.create_identity(create_identity_body=create_identity_body)
                created_user, schema_id = kratos_user_to_python(api_response)
            except ory_kratos_client.ApiException as e:
                if e.status == 400:
                    return self.respond("WRONG DATA FORMAT", 400)
                elif e.status == 409:
                    return self.respond("USER ALREADY EXISTS", 409)

                logger.debug("Exception when calling IdentityApi->create_identity: %s\n" % e)
                return self.respond("SOMETHING WENT WRONG", 500)

        if schema_id == "member":
            return self.respond("USER CREATED", 201, user_to_json(created_user))
        else:
            return self.respond("BOT CREATED", 201, bot_to_json(created_user))

    @tornado.web.authenticated
    @has_permissions("edit_members")
    async def put(self, id: str):
        user_id = self.check_uuid(id)

        if user_id is None:
            return self.respond("INVALID USER ID", 400)

        try:
            first = self.args["name"]["first"]
            last = self.args["name"]["last"]
            email = self.args["email"]
            phone = self.args.get("phone", None)
            street = self.args["postal_address"]["street"]
            postal_code = self.args["postal_address"]["postal_code"]
            city = self.args["postal_address"]["city"]
            municipality_id = int(self.args["municipality_id"])
            gender = self.args["gender"]
            birthday = self.args["birthday"]

            geo_dao = GeographyDao(self.db)
            municipality = await geo_dao.get_municipality_by_id(municipality_id)

            if municipality is None:
                return self.respond("MUNICIPALITY NOT FOUND", 404)
        except Exception as exc:
            logger.error("Could not load data into JSON object, bad request?")
            logger.debug(exc)
            return self.respond("WRONG DATA FORMAT", 400)

        configuration = Configuration(
            host="http://pirate-kratos:4433"
        )

        traits = {
            "name": {
                "first": first,
                "last": last
            },
            "email": email,
            "municipality_id": municipality_id,
            "postal_address": {
                "street": street,
                "postal_code": postal_code,
                "city": city
            },
            "gender": gender,
            "birthday": birthday
        }

        if phone is not None and phone != "":
            traits["phone"] = phone

        with ory_kratos_client.ApiClient(configuration) as api_client:
            api_instance = identity_api.IdentityApi(api_client)
            try:
                api_response = api_instance.get_identity(str(user_id))

                update_identity_body = UpdateIdentityBody(
                    metadata_admin=api_response.get("metadata_admin", None),
                    metadata_public=api_response.get("metadata_public", None),
                    schema_id=api_response["schema_id"],
                    state=api_response["state"],
                    traits=traits
                )
            except ory_kratos_client.ApiException as e:
                if e.status == 404:
                    return self.respond("USER NOT FOUND", 400)

                logger.debug("Exception when calling IdentityApi->get_identity: %s\n" % e)
                return self.respond("SOMETHING WENT WRONG", 500)

            try:
                api_response = api_instance.update_identity(str(user_id), update_identity_body=update_identity_body)
                updated_user, schema_id = kratos_user_to_python(api_response)
            except ory_kratos_client.ApiException as e:
                if e.status == 409:
                    return self.respond("EMAIL OR PHONE WAS CONFLICTING", 400)

                logger.debug("Exception when calling IdentityApi->update_identity: %s\n" % e)
                return self.respond("SOMETHING WENT WRONG", 500)

        if schema_id == "member":
            return self.respond("USER UPDATED", 200, user_to_json(updated_user))  # type: ignore  # updates_user is of type User
        else:
            return self.respond("BOT UPDATED", 200, bot_to_json(updated_user))  # type: ignore  # updates_user is of type Bot
