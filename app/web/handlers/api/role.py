import tornado.web

from app.database.dao.roles import RolesDao
from app.logger import logger
from app.models import role_to_json, Permission, permission_to_json
from app.web.handlers.base import APIHandler, has_permissions


class APIRoleHandler(APIHandler):
    @tornado.web.authenticated
    async def get(self, role_id: int):
        try:
            role_id = int(role_id)
        except ValueError:
            return self.respond("ROLE ID WAS NOT AN INTEGER", 400, None)

        dao = RolesDao(self.db)

        role = await dao.get_role_by_id(role_id)

        if role is None:
            return self.respond("ROLE NOT FOUND", 404)

        permissions = await dao.get_permissions_by_role(role.id)
        json_permissions = list()

        for permission in permissions:
            json_permissions.append(permission_to_json(permission))

        json_role = role_to_json(role)
        json_role["permissions"] = json_permissions

        return self.respond("ROLE RETURNED", 200, json_role)

    @tornado.web.authenticated
    @has_permissions("edit_roles")
    async def post(self):
        try:
            name = self.args["name"]
            description = self.args["description"]
            json_permissions = self.args["permissions"]
            permissions = list()

            for p in json_permissions:
                permission = Permission()
                permission.id = p["id"]

                if "name" in p:
                    permission.name = p["name"]

                permissions.append(permission)
        except Exception as exc:
            logger.error("Could not load data into JSON object, bad request?")
            logger.debug(exc)
            return self.respond("WRONG DATA FORMAT", 400)

        dao = RolesDao(self.db)

        role = await dao.create_role(name, description, permissions)

        if role is None:
            return self.respond("SOMETHING WENT WRONG WHEN CREATING ROLE", 500, None)

        json_role = role_to_json(role)
        json_role["permissions"] = json_permissions

        return self.respond("ROLE CREATED", 201, json_role)

    @tornado.web.authenticated
    @has_permissions("edit_roles")
    async def put(self, role_id: int):
        try:
            role_id = int(role_id)

            name = self.args["name"]
            description = self.args["description"]
            json_permissions = self.args["permissions"]
            permissions = list()

            for p in json_permissions:
                permission = Permission()
                permission.id = p["id"]

                if "name" in p:
                    permission.name = p["name"]

                permissions.append(permission)
        except Exception as exc:
            logger.error("Could not load data into JSON object, bad request?")
            logger.debug(exc)
            return self.respond("WRONG DATA FORMAT", 400)

        dao = RolesDao(self.db)

        role = await dao.update_role(role_id, name, description, permissions)

        if role is None:
            return self.respond("SOMETHING WENT WRONG WHEN CREATING ROLE", 500, None)

        json_role = role_to_json(role)
        json_role["permissions"] = json_permissions

        return self.respond("ROLE UPDATED", 200, json_role)

    @tornado.web.authenticated
    @has_permissions("edit_roles")
    async def delete(self, role_id: int):
        try:
            role_id = int(role_id)
        except ValueError:
            return self.respond("ROLE ID WAS NOT AN INTEGER", 400, None)

        dao = RolesDao(self.db)
        result = await dao.delete_role(role_id)

        if result:
            return self.respond("ROLE DELETED", 200, None)
        else:
            return self.respond("ROLE WAS NOT FOUND", 404, None)
