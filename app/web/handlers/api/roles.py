import tornado.web

from app.database.dao.roles import RolesDao
from app.models import role_to_json, permission_to_json
from app.web.handlers.base import APIHandler


class APIRolesHandler(APIHandler):
    @tornado.web.authenticated
    async def get(self):
        dao = RolesDao(self.db)

        roles = await dao.get_roles()

        response = list()

        for role in roles:
            permissions = await dao.get_permissions_by_role(role.id)
            json_permissions = list()

            for permission in permissions:
                json_permissions.append(permission_to_json(permission))

            json_role = role_to_json(role)
            json_role["permissions"] = json_permissions
            response.append(json_role)

        return self.respond("ROLES RETURNED", 200, response)
