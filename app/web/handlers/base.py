import json

from datetime import date, datetime
from typing import Any, Callable
from uuid import UUID

from asyncpg.pool import Pool
from ory_kratos_client.api_client import ApiClient
from ory_kratos_client.api import frontend_api
from ory_kratos_client.configuration import Configuration
from tornado.web import RequestHandler
from tornado.httpclient import AsyncHTTPClient, HTTPRequest

from app.logger import logger
from app.database.dao.users import UsersDao
from app.database.dao.roles import RolesDao
from app.models import Bot, Gender, kratos_user_to_python, Session, User


def has_permissions(*permissions: str) -> Callable:
    """
    Decorator that checks if the current user has the permissions listed
    :returns 403, PERMISSION DENIED, if the current user doesn't have all of the permissions
    """
    def has_permissions_wrapper(func):
        async def permission_check(self, *args, **kwargs):
            roles_dao = RolesDao(self.db)

            for permission in permissions:
                if not await roles_dao.check_user_permission(self.current_user.user_id, permission):
                    return self.respond("PERMISSION DENIED", 403, None)

            return await func(self, *args, **kwargs)
        return permission_check
    return has_permissions_wrapper


class BaseHandler(RequestHandler):
    def data_received(self, chunk):
        # IDE seems to want to override this method, sure then
        pass

    @property
    def db(self) -> Pool:
        return self.application.db  # type: ignore

    async def prepare(self):
        """
        Do not call manually. This runs on every request before get/post/etc.
        """
        self.current_user = await self.get_current_user()

    def is_authenticated(self) -> bool:
        """
        Check if current request is authenticated

        :returns: a boolean
        """
        return self.current_user is not None

    async def get_current_user(self) -> Session | None:
        """
        Do not use this method to get the current user session, use the property `self.current_user` instead.
        """
        session_hash = self.session_hash

        if session_hash is None:
            return None

        kratos_host_user = "http://pirate-kratos:4433/sessions/whoami"
        kratos_host_logout = "http://pirate-kratos:4433/self-service/logout/browser"
        http_client = AsyncHTTPClient()
        http_request_user = HTTPRequest(url=kratos_host_user, headers={"Cookie": session_hash})
        http_request_logout = HTTPRequest(url=kratos_host_logout, headers={"Cookie": session_hash})

        try:
            response_user = await http_client.fetch(http_request_user)
            response_logout = await http_client.fetch(http_request_logout)

            api_response = json.loads(response_user.body)
            api_response_logout = json.loads(response_logout.body)
        except Exception as e:
            logger.critical("Error when retrieving session: %s" % e)
            return None

        time_format = "%Y-%m-%dT%H:%M:%S.%fZ"

        session = Session()
        session.id = UUID(api_response["id"])
        session.hash = session_hash
        session.issued_at = datetime.strptime(api_response["issued_at"], time_format)
        session.expires_at = datetime.strptime(api_response["expires_at"], time_format)

        identity = api_response.get("identity", {})
        schema_id = identity.get("schema_id", "")
        identity_traits = identity.get("traits", {})
        metadata = identity.get("metadata_public", {})

        if schema_id == "bot":
            session.user = None
            bot = Bot()
            bot.id = UUID(identity["id"])
            bot.schema = schema_id
            bot.email = identity_traits["email"]
            bot.verified = identity["verifiable_addresses"][0]["verified"]
            bot.created = datetime.strptime(identity["created_at"], time_format)
            bot.name = identity_traits["name"]

            session.user_id = bot.id
            session.created = bot.created
            session.verified = bot.verified
            session.bot = bot
        else:
            session.bot = None

            user = User()
            user.id = UUID(identity["id"])
            user.schema = schema_id
            user.email = identity_traits["email"]
            user.verified = identity["verifiable_addresses"][0]["verified"]
            user.created = datetime.strptime(identity["created_at"], time_format)
            user.name.first = identity_traits["name"]["first"]
            user.name.last = identity_traits["name"]["last"]
            user.phone = identity_traits.get("phone", "")
            user.postal_address.street = identity_traits["postal_address"]["street"]
            user.postal_address.postal_code = identity_traits["postal_address"]["postal_code"]
            user.postal_address.city = identity_traits["postal_address"]["city"]
            user.municipality_id = int(identity_traits["municipality_id"])
            user.gender = Gender[identity_traits["gender"]]
            user.birthday = date.fromisoformat(identity_traits["birthday"])
            user.number = metadata.get("member_number", -1)

            session.user_id = user.id
            session.created = user.created
            session.verified = user.verified
            session.user = user

        session.logout_url = api_response_logout["logout_url"]
        logger.debug("Session user: " + str(session.user_id))

        return session

    @property
    def current_user(self) -> Session | None:
        """
        Get the current user session object that includes a object for the current user.
        :return: A Session object if authenticated, otherwise None
        """
        return super().current_user

    @current_user.setter
    def current_user(self, value: Session | None) -> None:
        self._current_user = value

    @property
    def session_hash(self) -> str | None:
        session_hash = self.get_cookie("ory_kratos_session")

        if session_hash is not None:
            session_hash = "ory_kratos_session=" + session_hash

        return session_hash

    def clear_session_cookie(self):
        self.clear_cookie("ory_kratos_session")

    @staticmethod
    def check_uuid(uuid: Any) -> UUID | None:
        if uuid is None:
            logger.warning("UUID is None")
            return None
        if type(uuid) is str:
            try:
                uuid = UUID(uuid)
            except ValueError:
                logger.warning("Badly formatted UUID string: " + uuid)
                return None
        elif type(uuid) is not UUID:
            logger.warning("UUID is wrong type: " + str(type(uuid)))
            return None

        return uuid

    async def permission_check(self) -> bool:
        if self.current_user is None:
            return False

        dao = UsersDao(self.db)
        return await dao.check_user_admin(self.current_user.user_id)

    def respond(self, message: str, status_code: int = 200, json_data: None | dict | list = None,
                show_error_page: bool = False):
        if show_error_page is True and status_code >= 400:
            return self.send_error(status_code, error_message=message)

        self.set_status(status_code, message)

        if status_code >= 400:
            self.write({'success': False, 'reason': message, 'data': json_data})
        elif status_code != 204:
            self.write({'success': True, 'reason': message, 'data': json_data})

    def write_error(self, status_code: int, error_message: str = "", **kwargs: Any) -> None:
        message = ""

        if error_message != "":
            message = error_message
        elif kwargs["exc_info"] is not None:
            message = str(kwargs["exc_info"])

        self.set_status(status_code, message)

        template = "error/" + str(status_code) + ".html"

        self.render(template)


class APIHandler(BaseHandler):
    @property
    def session_hash(self) -> str | None:
        session_hash = self.get_cookie("ory_kratos_session")

        if session_hash is not None:
            session_hash = "ory_kratos_session=" + session_hash
        else:
            session_hash = self.request.headers.get('Authentication', None)

            if session_hash is not None:
                logger.debug(session_hash)

        return session_hash

    def check_xsrf_cookie(self) -> None:
        if self.session_hash is not None and self.session_hash.startswith("Bearer "):
            return
        else:
            super().check_xsrf_cookie()

    async def get_current_user(self) -> Session | None:
        """
        Do not use this method to get the current user session, use the property `self.current_user` instead.
        """
        session_hash = self.session_hash

        if session_hash is None:
            return None

        configuration = Configuration(
            host="http://pirate-kratos:4433"
        )

        # Enter a context with an instance of the API client
        with ApiClient(configuration) as api_client:
            api_instance = frontend_api.FrontendApi(api_client)

            if session_hash.startswith("Bearer "):
                session_hash = session_hash.replace("Bearer ", "")

                try:
                    api_response = api_instance.to_session(x_session_token=session_hash)
                except Exception as e:
                    logger.error("Exception when calling FrontendApi->to_session: %s\n" % e)
                    return None
            else:
                try:
                    api_response = api_instance.to_session(cookie=session_hash)
                except Exception as e:
                    logger.error("Exception when calling FrontendApi->to_session: %s\n" % e)
                    return None

        identity = api_response.get("identity", None)

        if identity is None:
            return None

        session = Session()
        session.id = UUID(api_response["id"])
        session.hash = session_hash
        session.issued_at = api_response["issued_at"]
        session.expires_at = api_response["expires_at"]

        account, schema_id = kratos_user_to_python(identity)

        session.user_id = account.id
        session.created = account.created
        session.verified = account.verified

        if schema_id == "bot":
            session.user = None
            session.bot = account  # type: ignore  # account is of the type bot here
        else:
            session.bot = None
            session.user = account  # type: ignore  # account is of the type user here

        session.logout_url = ""
        logger.debug("Session user: " + str(session.user_id))

        return session

    async def prepare(self):
        """
        Do not call manually. This runs on every request before get/post/etc.
        """
        self.current_user = await self.get_current_user()
        self.args = {}

        if len(self.request.body) > 0 and self.request.headers.get("Content-Type", "").startswith("application/json"):
            try:
                self.args = json.loads(self.request.body)
            except Exception:
                self.respond("JSON data could not be parsed", 400, None)
                self.finish()
