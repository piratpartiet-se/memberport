class WebAppOptions:
    debug: bool = False
    https: bool = False
    port: int = 8888
    xsrf: bool = True
    cookie_secret: str = ""
    db_username: str = ""
    db_password: str = ""
    db_hostname: str = ""
    dbname: str = ""
    test: bool = False
