from asyncpg import Connection
from asyncpg.pool import Pool
from datetime import date, datetime, timedelta
from unittest.mock import MagicMock, Mock
from tornado.testing import AsyncHTTPTestCase
from uuid import UUID, uuid4

from app.models import Gender, Session, User
from app.web.web_server import Application, WebAppOptions


class MagicMockContext(MagicMock):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc, traceback):
        if exc is not None:
            raise exc

        return self


class WebTestCase(AsyncHTTPTestCase):
    def __init__(self, methodName: str) -> None:
        self.connection = MagicMockContext(Connection)
        super().__init__(methodName=methodName)

    def get_app(self):
        options = WebAppOptions()
        options.debug = True
        options.xsrf = False
        options.test = True
        options.cookie_secret = "ccd70ecea6d9f0833b07688e69bf2368f86f9127de17de102e17788a805afb7f"  # noqa: S105 # nosec

        app = Application(options, None, False)
        app.db = Mock(Pool)
        app.db.acquire.return_value = self.connection

        return app

    def assert_datetime(self, field_name, datetime_str):
        try:
            time_format = "%Y-%m-%d %H:%M:%S"
            self.assertTrue(isinstance(datetime.strptime(datetime_str, time_format), datetime))
        except ValueError:
            self.fail("'" + field_name + "' field was wrong format")

    def assert_uuid(self, field_name, uuid_str):
        try:
            UUID(uuid_str)
        except Exception:
            self.fail("'" + field_name + "' field was wrong UUID format")


def get_mock_session():
    session = Session()
    session.id = uuid4()
    session.hash = "ccd70ecea6d9f0833b07688e69bf2368f86f9127de17de102e17788a805afb7f"
    session.issued_at = datetime.now()
    session.expires_at = datetime.now() + timedelta(days=1)

    user = User()
    user.id = UUID("94983a62-8b07-4446-9753-8ba3a80d6000")
    user.schema = "member"
    user.name.first = "Barbro"
    user.name.last = "Pirat"
    user.email = "barbro.pirat@piratpartiet.se"
    user.phone = "070 00 00 000"
    user.postal_address.street = "Påhittad 8A"
    user.postal_address.postal_code = "22464"
    user.postal_address.city = "Lund"
    user.municipality_id = 120
    user.gender = Gender.female
    user.birthday = date(1999, 1, 1)
    user.verified = True
    user.created = session.issued_at
    user.number = 112

    session.user_id = user.id
    session.verified = user.verified
    session.created = user.created
    session.bot = None
    session.user = user

    return session


def get_kratos_json_traits():
    session = get_mock_session()

    json_traits = {
        "id": str(session.user_id),
        "schema_id": session.user.schema,
        "traits": {
            "name": {
                "first": session.user.name.first,
                "last": session.user.name.last
            },
            "email": session.user.email,
            "phone": session.user.phone,
            "postal_address": {
                "street": session.user.postal_address.street,
                "postal_code": session.user.postal_address.postal_code,
                "city": session.user.postal_address.city
            },
            "municipality_id": session.user.municipality_id,
            "gender": str(session.user.gender),
            "birthday": session.user.birthday.isoformat()
        },
        "metadata_public": {
            "member_number": str(session.user.number)
        },
        "verifiable_addresses": [
            {
                "verified": str(session.user.verified)
            }
        ],
        "created_at": session.user.created
    }

    return json_traits


def set_permissions(*permissions: str) -> callable:
    def set_permissions_wrapper(func):
        def return_permissions(self, *args, **kwargs):
            side_effects = [[{"role": uuid4()}]]
            permission_list = []

            for permission in permissions:
                permission_list.append({"permission": permission})

            side_effects.append(permission_list)

            self.connection.fetch.side_effect = side_effects
            return func(self, *args, **kwargs)
        return return_permissions
    return set_permissions_wrapper
