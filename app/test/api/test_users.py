import json

from app.test.web_testcase import WebTestCase, get_mock_session, get_kratos_json_traits, set_permissions
from unittest.mock import patch


class UsersTest(WebTestCase):
    def setUp(self):
        self.user = get_mock_session().user

        return super().setUp()

    @set_permissions("get_members")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    @patch('ory_kratos_client.api.identity_api.IdentityApi.get_identity', return_value=get_kratos_json_traits())
    def test_get_user(self, get_current_user, get_identity):
        session = get_mock_session()

        response = self.fetch(
            '/api/user/94983a62-8b07-4446-9753-8ba3a80d6000',
            method="GET"
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "USER RETURNED")
        self.assertEqual(json_body["data"]["id"], str(session.user.id))
        self.assertEqual(json_body["data"]["name"]["first"], session.user.name.first)
        self.assertEqual(json_body["data"]["name"]["last"], session.user.name.last)
        self.assertEqual(json_body["data"]["email"], session.user.email)
        self.assertEqual(json_body["data"]["gender"], str(session.user.gender))
        self.assertEqual(200, response.code)

    @set_permissions("get_members")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_get_users(self, get_current_user):
        session = get_mock_session()

        kratos_json = get_kratos_json_traits()
        kratos_json_traits = str(kratos_json["traits"]).replace("'", "\"")
        kratos_json_metadata = str(kratos_json["metadata_public"]).replace("'", "\"")

        side_effects = list(self.connection.fetch.side_effect)

        side_effects.append(
            [
                {
                    "id": session.user_id,
                    "schema_id": session.user.schema,
                    "traits": kratos_json_traits,
                    "metadata_public": kratos_json_metadata,
                    "verified": session.verified,
                    "created_at": session.created
                }
            ]
        )

        self.connection.fetch.side_effect = side_effects

        response = self.fetch(
            '/api/users',
            method="GET"
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "USERS RETURNED")
        self.assertEqual(json_body["data"][0]["id"], str(session.user.id))
        self.assertEqual(json_body["data"][0]["name"]["first"], session.user.name.first)
        self.assertEqual(json_body["data"][0]["name"]["last"], session.user.name.last)
        self.assertEqual(json_body["data"][0]["email"], session.user.email)
        self.assertEqual(json_body["data"][0]["gender"], str(session.user.gender))
        self.assertEqual(200, response.code)
