import json

from app.models import Area, Country, Municipality
from app.test.web_testcase import MagicMockContext, WebTestCase, get_mock_session, set_permissions
from asyncpg.exceptions import ForeignKeyViolationError
from asyncpg.transaction import Transaction
from datetime import datetime, timedelta
from unittest.mock import patch


class GeographyTest(WebTestCase):
    def setUp(self):
        self.country = Country()
        self.country.id = 1
        self.country.name = "Sverige"
        self.country.created = datetime(2020, 1, 1)

        self.area = Area()
        self.area.id = 2
        self.area.name = "Norra distriktet"
        self.area.created = datetime(2020, 1, 2)
        self.area.country_id = self.country.id
        self.area.path = "2"

        self.municipality = Municipality()
        self.municipality.id = 28
        self.municipality.name = "Lund"
        self.municipality.created = datetime(2020, 1, 2)
        self.municipality.country_id = self.country.id
        self.municipality.area_id = self.area.id

        return super().setUp()

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_create_country(self, get_current_user):
        new_id = 2
        new_country = "Atlantis"

        self.connection.fetchval.return_value = new_id

        arguments = {
            "name": new_country
        }

        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': len(json.dumps(arguments))
        }

        response = self.fetch(
            '/api/geography/country',
            method="POST",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.maxDiff = None

        try:
            int(json_body["data"]["id"])
        except ValueError:
            self.assertTrue(False, "Country ID was not an integer")

        self.connection.fetchval.assert_called()
        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "COUNTRY CREATED")
        self.assertEqual(json_body["data"]["id"], str(new_id))
        self.assertEqual(json_body["data"]["name"], new_country)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(201, response.code)

    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_retrieve_country(self, get_current_user):
        self.connection.fetchrow.return_value = {
            "id": 1,
            "name": self.country.name,
            "created": self.country.created,
        }

        response = self.fetch('/api/geography/country/' + str(self.country.id), method="GET")

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.maxDiff = None

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "RETRIEVED COUNTRY")
        self.assertEqual(json_body["data"]["id"], str(self.country.id))
        self.assertEqual(json_body["data"]["name"], self.country.name)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_update_country_name(self, get_current_user):
        new_name = "Norge"
        arguments = {
            "name": new_name
        }

        # For some reason, headers must be set for PUT requests but not POST
        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': len(json.dumps(arguments))
        }

        self.connection.fetchval.return_value = self.country.created

        response = self.fetch(
            '/api/geography/country/1',
            method="PUT",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.maxDiff = None

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "COUNTRY UPDATED")
        self.assertEqual(json_body["data"]["id"], str(self.country.id))
        self.assertEqual(json_body["data"]["name"], new_name)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_delete_country(self, get_current_user):
        self.connection.transaction.return_value = MagicMockContext(Transaction)

        response = self.fetch(
            '/api/geography/country/' + str(self.country.id),
            method="DELETE"
        )

        body = response.body.decode('raw_unicode_escape')
        self.assertEqual(len(body), 0)

        self.connection.execute.assert_called()
        self.assertEqual(response.reason, "COUNTRY DELETED")
        self.assertEqual(204, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_delete_fail_country(self, get_current_user):
        self.connection.transaction.return_value = MagicMockContext(Transaction)
        self.connection.execute.side_effect = ForeignKeyViolationError()

        response = self.fetch(
            '/api/geography/country/' + str(self.country.id),
            method="DELETE"
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.connection.execute.assert_called_once()
        self.assertEqual(json_body["success"], False)
        self.assertEqual(json_body["reason"], "COULD NOT DELETE COUNTRY! ORGANIZATION COULD BE ACTIVE IN COUNTRY")
        self.assertEqual(json_body["data"], None)
        self.assertEqual(403, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_create_area(self, get_current_user):
        new_id = 5
        self.connection.fetchval.side_effect = [str(self.area.id), new_id]
        new_area = "Österbotten"

        arguments = {
            "name": new_area,
            "country_id": str(self.country.id),
            "parent_id": str(self.area.id)
        }

        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': len(json.dumps(arguments))
        }

        response = self.fetch(
            '/api/geography/area',
            method="POST",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.maxDiff = None

        self.connection.fetchval.assert_called()
        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "AREA CREATED")
        self.assertEqual(json_body["data"]["id"], str(new_id))
        self.assertEqual(json_body["data"]["name"], new_area)
        self.assertEqual(json_body["data"]["country_id"], str(self.country.id))
        self.assertEqual(json_body["data"]["path"], str(self.area.id) + "." + str(new_id))
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(201, response.code)

    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_retrieve_area(self, get_current_user):
        self.connection.fetchrow.return_value = {
            "id": self.area.id,
            "name": self.area.name,
            "created": self.area.created,
            "country": self.area.country_id,
            "path": self.area.path
        }

        response = self.fetch('/api/geography/area/' + str(self.area.id), method="GET")

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "RETRIEVED AREA")
        self.assertEqual(json_body["data"]["id"], str(self.area.id))
        self.assertEqual(json_body["data"]["name"], self.area.name)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_update_area_name(self, get_current_user):
        new_name = "Södra distriktet"
        arguments = {
            "name": new_name,
            "country_id": self.area.country_id,
            "path": self.area.path
        }

        # For some reason, headers must be set for PUT requests but not POST
        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': len(json.dumps(arguments))
        }

        self.connection.fetchval.side_effect = [
            self.area.created,
            self.area.path
        ]

        response = self.fetch(
            '/api/geography/area/2',
            method="PUT",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "AREA UPDATED")
        self.assertEqual(json_body["data"]["name"], new_name)
        self.assertEqual(json_body["data"]["id"], str(self.area.id))
        self.assertEqual(json_body["data"]["country_id"], str(self.area.country_id))
        self.assertEqual(json_body["data"]["path"], self.area.path)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_delete_area(self, get_current_user):
        self.connection.transaction.return_value = MagicMockContext(Transaction)

        response = self.fetch(
            '/api/geography/area/' + str(self.area.id),
            method="DELETE"
        )

        body = response.body.decode('raw_unicode_escape')
        self.assertEqual(len(body), 0)

        self.connection.execute.assert_called()
        self.assertEqual(response.reason, "AREA DELETED")
        self.assertEqual(204, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_delete_fail_area(self, get_current_user):
        self.connection.transaction.return_value = MagicMockContext(Transaction)
        self.connection.execute.side_effect = ForeignKeyViolationError()

        response = self.fetch(
            '/api/geography/area/' + str(self.area.id),
            method="DELETE"
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.connection.execute.assert_called_once()
        self.assertEqual(json_body["success"], False)
        self.assertEqual(json_body["reason"], "COULD NOT DELETE AREA! ORGANIZATION COULD BE ACTIVE IN AREA")
        self.assertEqual(json_body["data"], None)
        self.assertEqual(403, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_create_municipality(self, get_current_user):
        new_id = 8
        new_municipality = "Oslo"

        self.connection.fetchval.return_value = new_id

        arguments = {
            "name": new_municipality,
            "country_id": str(self.country.id),
            "area_id": str(self.area.id)
        }

        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': len(json.dumps(arguments))
        }

        response = self.fetch(
            '/api/geography/municipality',
            method="POST",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.maxDiff = None

        try:
            int(json_body["data"]["id"])
        except ValueError:
            self.assertTrue(False, "Municipality ID was not an integer")

        self.connection.fetchval.assert_called_once()

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "MUNICIPALITY CREATED")
        self.assertEqual(json_body["data"]["id"], str(new_id))
        self.assertEqual(json_body["data"]["name"], new_municipality)
        self.assertEqual(json_body["data"]["country_id"], str(self.country.id))
        self.assertEqual(json_body["data"]["area_id"], str(self.area.id))
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(201, response.code)

    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_retrieve_municipality(self, get_current_user):
        self.connection.fetchrow.return_value = {
            "id": 268,
            "name": self.municipality.name,
            "created": self.municipality.created,
            "country": self.municipality.country_id,
            "area": self.municipality.area_id
        }

        response = self.fetch('/api/geography/municipality/' + str(self.municipality.id), method="GET")

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.maxDiff = None

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "RETRIEVED MUNICIPALITY")
        self.assertEqual(json_body["data"]["id"], str(self.municipality.id))
        self.assertEqual(json_body["data"]["name"], self.municipality.name)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(json_body["data"]["country_id"], str(self.municipality.country_id))
        self.assertEqual(json_body["data"]["area_id"], str(self.municipality.area_id))
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_update_municipality_name(self, get_current_user):
        new_name = "Luleå"
        arguments = {
            "name": new_name,
            "country_id": str(self.municipality.country_id),
            "area_id": str(self.municipality.area_id)
        }

        # For some reason, headers must be set for PUT requests but not POST
        headers = {
            'Content-Type': 'application/json; charset=UTF-8',
            'Content-Length': len(json.dumps(arguments))
        }

        self.connection.fetchval.return_value = self.municipality.created

        response = self.fetch(
            '/api/geography/municipality/' + str(self.municipality.id),
            method="PUT",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "MUNICIPALITY UPDATED")
        self.assertEqual(json_body["data"]["id"], str(self.municipality.id))
        self.assertEqual(json_body["data"]["name"], new_name)
        self.assert_datetime("created", json_body["data"]["created"])
        self.assertEqual(json_body["data"]["country_id"], str(self.municipality.country_id))
        self.assertEqual(json_body["data"]["area_id"], str(self.municipality.area_id))
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_delete_municipality(self, get_current_user):
        response = self.fetch(
            '/api/geography/municipality/' + str(self.municipality.id),
            method="DELETE"
        )

        body = response.body.decode('raw_unicode_escape')
        self.assertEqual(len(body), 0)

        self.connection.execute.assert_called_once()
        self.assertEqual(response.reason, "MUNICIPALITY DELETED")
        self.assertEqual(204, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_delete_fail_municipality(self, get_current_user):
        self.connection.execute.side_effect = ForeignKeyViolationError()

        response = self.fetch(
            '/api/geography/municipality/' + str(self.municipality.id),
            method="DELETE"
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.connection.execute.assert_called_once()
        self.assertEqual(json_body["success"], False)
        self.assertEqual(json_body["reason"], "COULD NOT DELETE MUNICIPALITY! ORGANIZATION COULD BE ACTIVE IN MUNICIPALITY")
        self.assertEqual(json_body["data"], None)
        self.assertEqual(403, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_update_areas_path(self, get_current_user):
        created1 = self.area.created
        created2 = self.area.created + timedelta(days=1)

        self.connection.fetchval.side_effect = [
            created1,
            self.area.path,
            1,
            created2,
            "2",
            1
        ]
        self.connection.fetchrow.side_effect = [
            {
                "name": self.area.name,
                "country": 1,
                "path": "3.2.1",
                "created": self.area.created
            },
            {
                "name": "Östra distriktet",
                "country": 1,
                "path": "3.2",
                "created": self.area.created
            }
        ]

        arguments = {
            "1": {"path": "2.1"},
            "2": {"path": "3.2"}
        }

        # For some reason, headers must be set for PUT requests but not POST
        headers = {
            'Content-Type': 'application/json',
            'Content-Length': len(json.dumps(arguments))
        }

        response = self.fetch(
            '/api/geography/areas',
            method="PUT",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "AREAS UPDATED")
        self.assertEqual(json_body["data"]["2"]["path"], "3.2")
        self.assertEqual(json_body["data"]["1"]["path"], "3.2.1")
        self.assertEqual(200, response.code)

    @set_permissions("edit_geography")
    @patch('app.web.handlers.base.APIHandler.get_current_user', return_value=get_mock_session())
    def test_update_municipalities_areas(self, get_current_user):
        municipality_id = 258

        self.connection.fetchrow.side_effect = [
            {
                "id": 5,
                "name": self.municipality.name,
                "created": self.municipality.created,
                "country": self.country.id,
                "area": self.area.id
            },
            {
                "id": 11,
                "name": "Luleå",
                "created": self.municipality.created,
                "country": self.country.id,
                "area": "3"
            }
        ]

        arguments = {
            str(self.municipality.id): {
                "name": self.municipality.name,
                "country_id": str(self.country.id),
                "area_id": str(self.area.id)
            },
            str(municipality_id): {
                "name": "Luleå",
                "country_id": str(self.country.id),
                "area_id": "3"
            }
        }

        # For some reason, headers must be set for PUT requests but not POST
        headers = {
            'Content-Type': 'application/json',
            'Content-Length': len(json.dumps(arguments))
        }

        response = self.fetch(
            '/api/geography/municipalities',
            method="PUT",
            body=json.dumps(arguments),
            headers=headers
        )

        body = response.body.decode('raw_unicode_escape')
        json_body = json.loads(body)

        self.assertEqual(json_body["success"], True)
        self.assertEqual(json_body["reason"], "MUNICIPALITIES UPDATED")
        self.assertEqual(200, response.code)
        self.assertEqual(json_body["data"][str(municipality_id)]["area_id"], "3")
        self.assertEqual(json_body["data"][str(self.municipality.id)]["area_id"], str(self.area.id))
