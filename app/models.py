from enum import Enum
from datetime import date, datetime
from uuid import UUID


class Name:
    first: str
    last: str


def name_to_json(name: Name):
    return {
        'first': name.first,
        'last': name.last
    }


class PostalAddress:
    street: str
    postal_code: str
    city: str


def postal_address_to_json(postal_address: PostalAddress):
    return {
        'street': postal_address.street,
        'postal_code': postal_address.postal_code,
        'city': postal_address.city
    }


class Bot:
    id: UUID
    schema: str
    name: str
    email: str

    verified: bool
    created: datetime


def bot_to_json(bot: Bot) -> dict:
    return {
        'id': str(bot.id),
        'schema': bot.schema,
        'name': bot.name,
        'email': bot.email,
        'verified': str(bot.verified).lower(),
        'created': bot.created.isoformat(' ', 'seconds')
    }


class Gender(Enum):
    male = 1
    female = 2
    other = 3
    unknown = 4

    def __str__(self):
        match self.value:
            case 1:
                return 'male'
            case 2:
                return 'female'
            case 3:
                return 'other'
            case _:
                return 'unknown'


class User:
    def __init__(self):
        self.name = Name()
        self.postal_address = PostalAddress()

    id: UUID
    schema: str
    number: int
    name: Name
    email: str
    phone: str
    postal_address: PostalAddress
    municipality_id: int
    gender: Gender
    birthday: date
    verified: bool

    created: datetime


def user_to_json(user: User) -> dict:
    return {
        'id': str(user.id),
        'schema': user.schema,
        'number': str(user.number),
        'name': name_to_json(user.name),
        'email': user.email,
        'phone': user.phone,
        'postal_address': postal_address_to_json(user.postal_address),
        'municipality_id': str(user.municipality_id),
        'verified': str(user.verified).lower(),
        'gender': str(user.gender),
        'birthday': user.birthday.isoformat(),
        'created': user.created.isoformat(' ', 'seconds')
    }


def kratos_user_to_python(api_response: dict) -> tuple[User, str] | tuple[Bot, str]:
    schema_id = api_response.get("schema_id", "")
    identity_traits = api_response.get("traits", {})
    metadata = api_response.get("metadata_public", {})

    if schema_id == "bot":
        bot = Bot()
        bot.id = UUID(api_response["id"])
        bot.schema = schema_id
        bot.email = identity_traits["email"]
        bot.verified = api_response["verifiable_addresses"][0]["verified"]
        bot.created = api_response["created_at"]
        bot.name = identity_traits["name"]

        return bot, schema_id
    else:
        user = User()
        user.id = UUID(api_response["id"])
        user.schema = schema_id
        user.email = identity_traits["email"]
        user.verified = api_response["verifiable_addresses"][0]["verified"]
        user.created = api_response["created_at"]
        user.name.first = identity_traits["name"]["first"]
        user.name.last = identity_traits["name"]["last"]
        user.phone = identity_traits.get("phone", "")
        user.postal_address.street = identity_traits["postal_address"]["street"]
        user.postal_address.postal_code = identity_traits["postal_address"]["postal_code"]
        user.postal_address.city = identity_traits["postal_address"]["city"]
        user.municipality_id = identity_traits["municipality_id"]
        user.gender = Gender[identity_traits["gender"]]
        user.birthday = date.fromisoformat(identity_traits["birthday"])
        user.number = metadata.get("member_number", -1)

        return user, schema_id


class Organization:
    id: UUID
    name: str
    description: str
    active: bool
    created: datetime
    show_on_signup: bool
    path: str

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return hash(self.id)


def organization_to_json(organization: Organization) -> dict:
    return {
        'id': str(organization.id),
        'name': organization.name,
        'description': organization.description,
        'active': str(organization.active).lower(),
        'created': organization.created.isoformat(' ', 'seconds'),
        'show_on_signup': str(organization.show_on_signup).lower(),
        'path': organization.path
    }


class Membership:
    id: UUID
    organization_id: UUID
    user_id: UUID
    created: datetime
    renewal: datetime


def membership_to_json(membership: Membership) -> dict:
    return {
        'id': str(membership.id),
        'organization_id': str(membership.organization_id),
        'user_id': str(membership.user_id),
        'created': membership.created.isoformat(' ', 'seconds'),
        'renewal': membership.renewal.isoformat(' ', 'seconds')
    }


class Role:
    id: int
    name: str
    description: str
    created: datetime


def role_to_json(role: Role) -> dict:
    return {
        'id': str(role.id),
        'name': role.name,
        'description': role.description,
        'created': role.created.isoformat(' ', 'seconds')
    }


class Permission:
    id: str
    name: str

    def __eq__(self, other):
        return self.id == other.id


def permission_to_json(permission: Permission) -> dict:
    return {
        'id': permission.id,
        'name': permission.name
    }


class UserRole:
    id: UUID
    user_id: UUID
    role_id: int
    geodata_id: int | None
    organization_id: UUID | None


def user_role_to_json(user_role: UserRole) -> dict:
    return {
        'id': str(user_role.id),
        'user': str(user_role.user_id),
        'role': str(user_role.role_id),
        'organization': None if user_role.organization_id is None else str(user_role.organization_id)
    }


class Session:
    id: UUID

    user_id: UUID  # This is the ID of the user/bot model
    verified: bool  # This is the verified state of the user/bot model
    created: datetime  # This is the created time of the user/bot model

    user: User | None
    bot: Bot | None

    hash: str
    issued_at: datetime
    expires_at: datetime
    logout_url: str
    last_ip: str


class Country:
    id: int
    name: str
    created: datetime


def country_to_json(country: Country) -> dict:
    return {
        'id': str(country.id),
        'name': str(country.name),
        'created': country.created.isoformat(' ', 'seconds')
    }


class Area:
    id: int
    name: str
    created: datetime
    country_id: int
    path: str


def area_to_json(area: Area) -> dict:
    return {
        'id': str(area.id),
        'name': str(area.name),
        'created': area.created.isoformat(' ', 'seconds'),
        'country_id': str(area.country_id),
        'path': area.path
    }


class Municipality:
    id: int
    name: str
    created: datetime
    country_id: int
    area_id: int | None


def municipality_to_json(municipality: Municipality) -> dict:
    return {
        'id': str(municipality.id),
        'name': municipality.name,
        'created': municipality.created.isoformat(' ', 'seconds'),
        'country_id': str(municipality.country_id),
        'area_id': str(municipality.area_id) if municipality.area_id is not None else ''
    }


class Post:
    id: UUID
    title: str
    content: str
    author: UUID
    created: datetime
    updated: datetime


def post_to_json(post: Post) -> dict:
    return {
        'id': str(post.id),
        'title': post.title,
        'content': post.content,
        'author': str(post.author),
        'created': post.created.isoformat(' ', 'seconds'),
        'updated': post.updated.isoformat(' ', 'seconds'),
    }


class Event:
    id: UUID | None
    title: str
    description: str
    host: UUID | None
    start: datetime
    end: datetime
    created: datetime
    all_day: bool
    url: str


def event_to_json(event: Event) -> dict:
    return {
        'id': str(event.id) if event.id is not None else '',
        'title': event.title,
        'description': event.description,
        'host': str(event.host) if event.host is not None else '',
        'start': event.start.isoformat(' ', 'seconds'),
        'end': event.end.isoformat(' ', 'seconds'),
        'created': event.created.isoformat(' ', 'seconds'),
        'allDay': str(event.all_day).lower(),
        'url': event.url
    }


class Calendar:
    id: UUID
    description: str
    ics_url: str
    created: datetime


def calendar_to_json(calendar: Calendar) -> dict:
    return {
        'id': str(calendar.id),
        'description': calendar.description,
        'ics_url': calendar.ics_url,
        'created': calendar.created.isoformat(' ', 'seconds')
    }


def ui_placeholders(button_label: str) -> dict:
    return {
        "password": "Lösenord",
        "traits.name": "Namn",
        "traits.name.first": "Förnamn",
        "traits.name.last": "Efternamn",
        "traits.postal_address.street": "Gatuadress",
        "traits.postal_address.postal_code": "Postnummer",
        "traits.postal_address.city": "Postort",
        "traits.phone": "Telefonnummer",
        "traits.email": "E-post",
        "email": "E-post",
        "traits.municipality_id": "Kommun",
        "traits.gender": "Självupplevt kön",
        "traits.birthday": "Födelsedag",
        "code": "Kod",
        "method": button_label
    }


def ui_positions() -> dict:
    return {
        "csrf_token": 0,
        "traits.name": 1,
        "traits.name.first": 1,
        "traits.name.last": 2,
        "traits.email": 3,
        "traits.phone": 4,
        "password": 5,
        "traits.postal_address.street": 6,
        "traits.postal_address.postal_code": 7,
        "traits.postal_address.city": 8,
        "traits.municipality_id": 9,
        "traits.birthday": 10,
        "traits.gender": 11,
        "code": 12,
        "method": 13
    }
