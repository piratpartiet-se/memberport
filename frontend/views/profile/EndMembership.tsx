import { sendEndMembershipRequest } from '@memberport/utils/api'
import React, {
  useState
} from 'react'

interface EndMembershipProps {
  id?: string
  className?: string
  membershipID: string | null
  onCancel: () => void
  onEndMembership: () => void
}

export const EndMembership: React.FC<EndMembershipProps> = (props) => {
  const {
    id = 'end-membership',
    className,
    membershipID,
    onCancel,
    onEndMembership
  } = props

  const [reason, setReason] = useState<string>('')

  function endMembership (): void {
    if (membershipID === null) {
      return
    }

    sendEndMembershipRequest(membershipID, reason).then((response) => {
      setReason('')

      if (response.status === 204) {
        onEndMembership()
      } else {
        onCancel()
      }
    }).catch(console.error)
  }

  return (
    <div id={id} className={membershipID !== null ? 'modal is-active ' + className : 'modal ' + className}>
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Avsluta medlemskap</p>
          <button className="delete" aria-label="close" onClick={onCancel} />
        </header>
        <section className="modal-card-body">
          <div className="content">
            <p><b>Anledning till varför du vill avsluta ditt medlemskap</b></p>
          </div>
          <textarea
            name="reason"
            className="textarea"
            placeholder="e.g. Jag avslutar mitt medlemskap på grund av att..."
            value={reason} onChange={(event) => {
              setReason(event.target.value)
            }}
          />
        </section>
        <footer className="modal-card-foot">
          <button id="endMembershipButton" className="button is-danger" onClick={endMembership}>
            Avsluta medlemskap
          </button>
          <button className="button is-success" onClick={onCancel}>Avbryt</button>
        </footer>
      </div>
      <button className="modal-close is-large" aria-label="close" />
    </div>
  )
}

export default EndMembership
