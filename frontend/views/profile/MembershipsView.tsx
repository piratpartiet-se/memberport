import { sendCreateMembershipRequest, sendMembershipsRequest, sendOrganizationsRequest, sendRenewMembershipRequest } from '@memberport/utils/api'
import { type MembershipData } from '@memberport/utils/membership/membershipdata'
import { type OrgData } from '@memberport/utils/organization/orgdata'
import classNames from 'classnames'
import React, {
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react'
import EndMembership from './EndMembership'

interface MembershipsViewProps {
  id?: string
  className?: string
  userID: string
  municipalityID: string | null
  preloadedOrgs: Record<string, OrgData> | null
}

interface MembershipRow {
  id: string
  organization: string
  created: string
  renewal: string
}

interface FormMessage {
  type: 'success' | 'warning' | 'danger' | 'info'
  message: string
}

export const MembershipsView: React.FC<MembershipsViewProps> = (props) => {
  const {
    id = 'memberships-view',
    className,
    userID,
    municipalityID,
    preloadedOrgs
  } = props

  const [rows, setRows] = useState<MembershipRow[]>([])
  const [memberships, setMemberships] = useState<MembershipData[]>([])
  const [organizations, setOrganizations] = useState<Record<string, OrgData>>({})
  const [joinableOrganizations, setJoinableOrganizations] = useState<Record<string, OrgData>>({})
  const [selectedMembership, setSelectedMembership] = useState<string | null>(null)
  const [formMessage, setFormMessage] = useState<FormMessage | null>(null)

  const timeoutRef = useRef<NodeJS.Timeout | null>(null)

  function getMemberships (): void {
    if (userID === '') {
      return
    }

    sendMembershipsRequest(userID).then(async (response) => {
      if (response.status === 200) {
        const respJson = await response.json()

        const membershipsData: MembershipData[] = respJson.data

        setMemberships(membershipsData)
      }
    }).catch(console.error)
  }

  function getOrganizations (): void {
    sendOrganizationsRequest().then(async (response) => {
      if (response.status === 200) {
        const respJson = await response.json()
        const organizationsData: Record<string, OrgData> = respJson.data

        setOrganizations(organizationsData)
      }
    }).catch(console.error)
  }

  function renewMembership (membershipID: string): void {
    sendRenewMembershipRequest(membershipID).then(async (response) => {
      if (response.status === 200) {
        getMemberships()

        setFormMessage({
          message: 'Medlemskap förnyat',
          type: 'success'
        })
      } else {
        setFormMessage({
          message: 'Något gick fel när medlemskapet skulle förnyas',
          type: 'danger'
        })
      }

      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)
    }).catch(console.error)
  }

  function joinOrganization (organizationID: string): void {
    sendCreateMembershipRequest(userID, organizationID).then(async (response) => {
      if (response.status === 201) {
        setFormMessage({
          message: 'Nytt medlemskap godkänt!',
          type: 'success'
        })

        getMemberships()
      } else if (response.status === 409) {
        setFormMessage({
          message: 'Du är redan medlem i denna förening',
          type: 'warning'
        })
      }

      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)
    }).catch(console.error)
  }

  useEffect(() => {
    if (preloadedOrgs === null) {
      getOrganizations()
    } else {
      setOrganizations(preloadedOrgs)
    }

    if (municipalityID !== null) {
      sendOrganizationsRequest(municipalityID).then(async (response) => {
        if (response.status === 200) {
          const respJson = await response.json()
          const organizationsData: Record<string, OrgData> = respJson.data

          setJoinableOrganizations(organizationsData)
        }
      }).catch(console.error)
    }

    getMemberships()

    return () => {
      if (timeoutRef.current !== null) {
        clearTimeout(timeoutRef.current)
      }
    }
  }, [props.userID, preloadedOrgs])

  useEffect(() => {
    const rows: MembershipRow[] = memberships.map((membership) => {
      const orgName = organizations[membership.organization_id]?.name ?? membership.organization_id

      return {
        id: membership.id,
        organization: orgName,
        created: membership.created,
        renewal: membership.renewal
      }
    })

    setRows(rows)
  }, [memberships, organizations])

  const listJoinableOrganizations = useMemo<React.JSX.Element[]>(() => {
    const organizationsList = Object.values(joinableOrganizations).filter(organization => {
      for (let index = 0; index < memberships.length; index++) {
        if (memberships[index].organization_id === organization.id) {
          return false
        }
      }

      return true
    })

    const listItems = organizationsList.map(organization =>
      <button key={organization.id} type="button" className="button is-success" onClick={() => { joinOrganization(organization.id) }}>
        {organization.name}
      </button>
    )

    return listItems
  }, [joinableOrganizations, memberships])

  return (
    <div>
      <EndMembership
        membershipID={selectedMembership}
        onCancel={() => { setSelectedMembership(null) }}
        onEndMembership={() => {
          setSelectedMembership(null)
          getMemberships()
          setFormMessage({
            message: 'Medlemskap avslutat',
            type: 'danger'
          })
          timeoutRef.current = setTimeout(() => {
            setFormMessage(null)
          }, 3000)
        }}
      />
      <table
        id={id}
        className={classNames(
          'table table-calendars is-fullwidth is-striped',
          { 'is-hoverable': rows.length > 0 },
          className
        )}
      >
        <thead>
          <tr>
            <th>Förening</th>
            <th>Medlem sedan</th>
            <th>Löper ut</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {rows.map(({ id, organization, created, renewal }) => (
            <tr key={id}>
              <td>{organization}</td>
              <td>{created.slice(0, 10)}</td>
              <td>{renewal.slice(0, 10)}</td>
              <td className="has-text-right">
                <button
                  type="button"
                  className="button is-success mr-1"
                  onClick={() => {
                    renewMembership(id)
                  }}
                >Förnya
                </button>
                <button
                  type="button"
                  className="button is-danger"
                  onClick={() => {
                    setSelectedMembership(id)
                  }}
                >Avsluta
                </button>
              </td>
            </tr>
          ))}
          {rows.length === 0 &&
            <tr>
              <td colSpan={3}><em>Inga medlemskap hittades</em></td>
            </tr>
          }
        </tbody>
      </table>
      {listJoinableOrganizations.length > 0 ? <div className="content mb-1"><b>Gå med: </b></div> : null}
      <div className="buttons mb-1">
        {listJoinableOrganizations}
      </div>
      {formMessage !== null &&
        <div className={classNames('notification', `is-${formMessage.type}`)}>
          {formMessage.message}
        </div>
      }
    </div>
  )
}

export default MembershipsView
