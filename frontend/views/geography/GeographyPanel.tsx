import { GEO_TYPES, type GeoData } from '@memberport/utils/geography/geodata'
import classNames from 'classnames'
import React, {
  useMemo,
  useRef,
  useState
} from 'react'

interface GeographyPanelProps {
  className?: string
  geodata: GeoData
  tree: Record<string, GeoData[]>
  onSelectionChanged: (geodata: GeoData, panelElement: HTMLDivElement | null) => void
  selectedCountries: Record<string, boolean>
  selectedAreas: Record<string, boolean>
  selectedMunicipalities: Record<string, boolean>
}

export const GeographyPanel: React.FC<GeographyPanelProps> = (props) => {
  const {
    className,
    geodata,
    tree,
    onSelectionChanged,
    selectedCountries,
    selectedAreas,
    selectedMunicipalities
  } = props

  const panelElement = useRef<HTMLDivElement>(null)
  const [showChildren, setShowChildren] = useState<boolean>(true)

  function handleSelected (event): void {
    event.stopPropagation()
    onSelectionChanged(geodata, panelElement.current)
  }

  function getIDWithType (id: string, type: GEO_TYPES): string {
    if (type === GEO_TYPES.COUNTRY) {
      return id
    } else if (type === GEO_TYPES.AREA) {
      return 'a-' + id
    } else {
      return 'm-' + id
    }
  }

  const selected = useMemo<boolean>(() => {
    let selected: boolean | undefined

    if (geodata.type === GEO_TYPES.COUNTRY) {
      selected = selectedCountries[geodata.id]
    } else if (geodata.type === GEO_TYPES.AREA) {
      selected = selectedAreas[geodata.id]
    } else if (geodata.type === GEO_TYPES.MUNICIPALITY) {
      selected = selectedMunicipalities[geodata.id]
    }

    if (selected === undefined) {
      selected = false
    }

    return selected
  }, [geodata, selectedCountries, selectedMunicipalities, selectedAreas])

  return (
    <div geo-id={getIDWithType(geodata.id, geodata.type)} className={classNames(selected ? 'selected' : 'not-selected', className)} ref={panelElement}>
      <div className="panel-block">
        <button type="button" className="no-button" onClick={handleSelected}>
          <label className="checkbox">
            <input type="checkbox" checked={selected} readOnly />
          </label>
          <span className="panel-icon" style={{ verticalAlign: 'middle' }}>
            <i className={classNames('fas', geodata.type === GEO_TYPES.COUNTRY ? 'fa-flag' : '', geodata.type === GEO_TYPES.AREA ? 'fa-layer-group' : '', geodata.type === GEO_TYPES.MUNICIPALITY ? 'fa-home' : '')} />
          </span>
          {geodata.name}
        </button>
        <button className="is-flex is-flex-grow-1 is-justify-content-flex-end is-align-items-center is-align-self-stretch no-button" onClick={() => { setShowChildren(!showChildren) }}>
          <span className="is-align-self-stretch">
            <i className={classNames('fas', showChildren ? 'fa-angle-down' : 'fa-angle-up')} />
          </span>
        </button>
      </div>
      {showChildren && geodata.type !== GEO_TYPES.MUNICIPALITY
        ? tree[getIDWithType(geodata.id, geodata.type)].map((geography) => (
          <GeographyPanel
            key={getIDWithType(geography.id, geography.type)}
            geodata={geography}
            className="indent"
            tree={tree}
            onSelectionChanged={onSelectionChanged}
            selectedCountries={selectedCountries}
            selectedAreas={selectedAreas}
            selectedMunicipalities={selectedMunicipalities}
          />
        ))
        : null}
    </div>
  )
}

export default GeographyPanel
