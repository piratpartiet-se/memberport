import React, {
  useEffect,
  useMemo,
  useState
} from 'react'
import GeographyPanel from './GeographyPanel'
import { GEO_TYPES, type GeoData } from '@memberport/utils/geography/geodata'
import classNames from 'classnames'

interface GeographyTreeProps {
  id?: string
  className?: string
  onSelectionChanged: (countriesSelected: Record<string, boolean>, areasSelected: Record<string, boolean>, municipalitiesSelected: Record<string, boolean>) => void
  countries: Record<string, GeoData>
  areas: Record<string, GeoData>
  municipalities: Record<string, GeoData>
}

export const GeographyTree: React.FC<GeographyTreeProps> = (props) => {
  const {
    id = 'geography-tree',
    className,
    onSelectionChanged,
    countries,
    areas,
    municipalities
  } = props

  const [searchText, setSearchText] = useState<string>('')
  const [selectedCountries, setSelectedCountries] = useState<Record<string, boolean>>({})
  const [selectedAreas, setSelectedAreas] = useState<Record<string, boolean>>({})
  const [selectedMunicipalities, setSelectedMunicipalities] = useState<Record<string, boolean>>({})

  const onGeographySelected = (geodata: GeoData, panelElement: HTMLDivElement | null): void => {
    const selectedC = selectedCountries
    const selectedA = selectedAreas
    const selectedM = selectedMunicipalities

    let changedCountries = false
    let changedAreas = false
    let changedMunicipalities = false
    let newSelectedValue = false

    if (geodata.type === GEO_TYPES.COUNTRY) {
      newSelectedValue = !selectedC[geodata.id]
      selectedC[geodata.id] = newSelectedValue
      changedCountries = true
    } else if (geodata.type === GEO_TYPES.AREA) {
      newSelectedValue = !selectedA[geodata.id]
      selectedA[geodata.id] = newSelectedValue
      changedAreas = true
    } else {
      newSelectedValue = !selectedM[geodata.id]
      selectedM[geodata.id] = newSelectedValue
      changedMunicipalities = true
    }

    if (geodata.type !== GEO_TYPES.MUNICIPALITY && panelElement !== null) {
      const children = newSelectedValue ? panelElement.getElementsByClassName('not-selected') : panelElement.getElementsByClassName('selected')

      for (let index = 0; index < children.length; index++) {
        const geoID = children[index].getAttribute('geo-id')

        if (geoID !== null && geoID.startsWith('a-')) {
          selectedA[geoID.slice(2)] = newSelectedValue
          changedAreas = true
        } else if (geoID !== null && geoID.startsWith('m-')) {
          selectedM[geoID.slice(2)] = newSelectedValue
          changedMunicipalities = true
        }
      }
    }

    if (geodata.type !== GEO_TYPES.COUNTRY && panelElement !== null) {
      const id = geodata.type === GEO_TYPES.AREA ? 'a-' + geodata.id : 'm-' + geodata.id
      const currentParents = parents[id]

      if (!newSelectedValue) {
        currentParents.forEach((parent) => {
          if (parent.startsWith('a-')) {
            if (selectedA[parent.slice(2)]) {
              selectedA[parent.slice(2)] = false
              changedAreas = true
            }
          } else {
            if (selectedC[parent]) {
              selectedC[parent] = false
              changedCountries = true
            }
          }
        })
      } else {
        let currentPanel: HTMLElement | null | undefined = panelElement.parentElement

        while (currentPanel !== null && currentPanel !== undefined) {
          const geoID = currentPanel?.getAttribute('geo-id')

          if (geoID === null) {
            currentPanel = null
          } else {
            const notSelected = currentPanel.querySelectorAll(':scope > .not-selected')

            if (notSelected.length < 2) {
              if (geoID.startsWith('a-')) {
                selectedA[geoID.slice(2)] = true
                changedAreas = true
              } else {
                selectedC[geoID] = true
                changedCountries = true
              }
            } else {
              currentPanel = null
            }
          }

          currentPanel = currentPanel?.parentElement
        }
      }
    }

    if (changedCountries) {
      setSelectedCountries({ ...selectedC })
    }

    if (changedAreas) {
      setSelectedAreas({ ...selectedA })
    }

    if (changedMunicipalities) {
      setSelectedMunicipalities({ ...selectedM })
    }
  }

  function onSearchChange (event): void {
    const value = event.target.value

    setSearchText(value)
  }

  useEffect(() => {
    const selectedC: Record<string, boolean> = {}
    const selectedA: Record<string, boolean> = {}
    const selectedM: Record<string, boolean> = {}

    Object.values(countries).forEach((country: GeoData): void => {
      selectedC[country.id] = true
    })

    Object.values(areas).forEach((area: GeoData): void => {
      selectedA[area.id] = true
    })

    Object.values(municipalities).forEach((municipality: GeoData): void => {
      selectedM[municipality.id] = true
    })

    setSelectedCountries(selectedC)
    setSelectedAreas(selectedA)
    setSelectedMunicipalities(selectedM)
  }, [countries, areas, municipalities])

  const tree = useMemo<Record<string, GeoData[]>>(() => {
    const tree: Record<string, GeoData[]> = {}

    Object.values(countries).forEach((country) => {
      tree[country.id] = []
    })

    Object.values(areas).forEach((area) => {
      if (area.path === undefined || area.country_id === undefined) {
        console.warn('Area with ID: ' + area.id + ' had path or country_id as undefined')
        return
      }

      const id = 'a-' + area.id
      const countryID = area.country_id
      const paths = area.path.split('.')

      if (!tree[id]) {
        tree[id] = []
      }

      if (paths.length === 1) {
        if (!tree[countryID]) {
          tree[countryID] = []
        }

        tree[countryID].push(area)
      } else {
        const parentID = 'a-' + paths[paths.length - 2]

        if (!tree[parentID]) {
          tree[parentID] = []
        }

        tree[parentID].push(area)
      }
    })

    Object.values(municipalities).forEach((municipality) => {
      if (municipality.country_id === undefined) {
        console.warn('Municipality with ID: ' + municipality.id + ' had country_id as undefined')
        return
      }

      const countryID = municipality.country_id
      const areaID = municipality.area_id

      if (areaID !== undefined) {
        if (!tree['a-' + areaID]) {
          tree['a-' + areaID] = []
        }

        tree['a-' + areaID].push(municipality)
      } else {
        if (!tree[countryID]) {
          tree[countryID] = []
        }

        tree[countryID].push(municipality)
      }
    })

    return tree
  }, [countries, areas, municipalities])

  const parents = useMemo<Record<string, string[]>>(() => {
    const parents: Record<string, string[]> = {}

    Object.values(areas).forEach((area) => {
      const countryID = area.country_id as string

      parents['a-' + area.id] = [countryID]

      const path = (area.path as string).split('.')
      path.forEach((areaID, index) => {
        if (index === path.length - 1) {
          return
        }

        parents['a-' + area.id].push('a-' + areaID)
      })
    })

    Object.values(municipalities).forEach((municipality) => {
      const countryID = municipality.country_id as string
      const areaID = municipality.area_id

      if (areaID !== undefined && parents['a-' + areaID]) {
        parents['m-' + municipality.id] = parents['a-' + areaID].slice(0)
        parents['m-' + municipality.id].push('a-' + areaID)
      } else {
        parents['m-' + municipality.id] = [countryID]
      }
    })

    return parents
  }, [countries, areas, municipalities])

  useEffect(() => {
    onSelectionChanged(selectedCountries, selectedAreas, selectedMunicipalities)
  }, [selectedCountries, selectedAreas, selectedMunicipalities])

  return (
    <div id={id} className={classNames(className, 'panel', 'is-primary')}>
      <p className="panel-heading">
        Geografi
      </p>
      <div className="panel-block">
        <p className="control has-icons-left">
          <input className="input is-primary" type="text" placeholder="Sök" value={searchText} onChange={onSearchChange} />
          <span className="icon is-left">
            <i className="fas fa-search" aria-hidden="true" />
          </span>
        </p>
      </div>
      <div style={{ maxHeight: '13rem', overflow: 'scroll' }}>
        {Object.values(countries).map(country => (
          <GeographyPanel
            key={country.id}
            geodata={country}
            tree={tree}
            onSelectionChanged={onGeographySelected}
            selectedCountries={selectedCountries}
            selectedAreas={selectedAreas}
            selectedMunicipalities={selectedMunicipalities}
          />
        ))}
      </div>
    </div>
  )
}

export default GeographyTree
