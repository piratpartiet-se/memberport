import React, {
  useEffect,
  useState
} from 'react'
import { type RoleData } from '@memberport/utils/user/roledata'
import { sendOrganizationsRequest, sendRolesRequest, sendUserRolesRequest, sendUsersRequest } from '@memberport/utils/api'
import RolesTable from './RolesTable'
import RolesUsersTable from './RolesUsersTable'
import type RoleItem from './RoleItem'
import { RoleDataToItem } from './RoleItem'
import RoleForm from './RoleForm'
import type UserRole from './UserRole'
import { type UserData } from '@memberport/utils/user/userdata'
import { type OrgData } from '@memberport/utils/organization/orgdata'
import UserRoleForm from './UserRoleForm'

interface RolesViewProps {
  id?: string
  className?: string
}

export const RolesView: React.FC<RolesViewProps> = (props) => {
  const {
    id = 'roles-view'
  } = props

  const [roles, setRoles] = useState<RoleItem[]>([])
  const [rolesRecord, setRolesRecord] = useState<Record<string, RoleData>>({})
  const [userRoles, setUserRoles] = useState<UserRole[]>([])
  const [users, setUsers] = useState<UserData[]>([])
  const [usersRecord, setUsersRecord] = useState<Record<string, UserData>>({})
  const [organizations, setOrganizations] = useState<OrgData[]>([])
  const [organizationsRecord, setOrganizationsRecord] = useState<Record<string, OrgData>>({})

  const [editRole, setEditRole] = useState<RoleItem | null>(null)

  function getRoles (): void {
    sendRolesRequest()
      .then(async (response) => {
        const respJson = await response.json()
        const rolesData: RoleData[] = respJson.data
        const roleItems: RoleItem[] = []
        const newRolesRecord: Record<string, RoleData> = {}

        rolesData.forEach((role) => {
          newRolesRecord[role.id] = role
          const roleItem = RoleDataToItem(role, true)
          roleItems.push(roleItem)
        })
        setRolesRecord(newRolesRecord)
        setRoles(roleItems)
      })
      .catch(err => {
        console.warn(err)
      })
  }

  function getUserRoles (): void {
    sendUserRolesRequest()
      .then(async (response) => {
        const respJson = await response.json()
        const userRoles: UserRole[] = respJson.data

        setUserRoles(userRoles)
      })
      .catch(err => {
        console.warn(err)
      })
  }

  function getUsers (): void {
    sendUsersRequest([], true)
      .then(async (response) => {
        const respJson = await response.json()
        const users: UserData[] = respJson.data
        const usersDict: Record<string, UserData> = {}

        users.forEach((user) => {
          usersDict[user.id] = user
        })

        setUsers(users)
        setUsersRecord(usersDict)
      })
      .catch(err => {
        console.warn(err)
      })
  }

  function getOrganizations (): void {
    sendOrganizationsRequest()
      .then(async (response) => {
        const respJson = await response.json()
        const organizations: OrgData[] = respJson.data
        const orgsDict: Record<string, OrgData> = {}

        organizations.forEach((organization) => {
          orgsDict[organization.id] = organization
        })

        setOrganizations(organizations)
        setOrganizationsRecord(orgsDict)
      })
      .catch(err => {
        console.warn(err)
      })
  }

  useEffect(() => {
    getRoles()
    getUserRoles()
    getUsers()
    getOrganizations()
  }, [])

  function onRoleChange (): void {
    setEditRole(null)
    getRoles()
  }

  function onEditClick (row: any): void {
    if (row === null || row === undefined) {
      setEditRole(null)
      return
    }

    const role = row as RoleItem
    setEditRole(role)
  }

  function onUserRoleChange (): void {
    getUserRoles()
  }

  return (
    <div id={id}>
      <div className="columns">
        <div className="column is-two-thirds mb-6">
          <RolesTable rows={roles} onEditClick={onEditClick} />
        </div>
        <div className="column">
          <RoleForm editRole={editRole} onRoleChange={onRoleChange} onReset={onRoleChange} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-two-thirds mb-6">
          <RolesUsersTable onUserRoleDelete={onUserRoleChange} userRoles={userRoles} roles={rolesRecord} users={usersRecord} organizations={organizationsRecord} />
        </div>
        <div className="column">
          <UserRoleForm onUserRoleChange={onUserRoleChange} roles={roles} users={users} organizations={organizations} />
        </div>
      </div>
    </div>
  )
}

export default RolesView
