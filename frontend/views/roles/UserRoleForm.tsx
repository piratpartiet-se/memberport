import React, {
  useMemo,
  useState,
  useRef,
  type FormEvent
} from 'react'
import classNames from 'classnames'
import type UserRole from './UserRole'
import { type UserData } from '@memberport/utils/user/userdata'
import { type OrgData } from '@memberport/utils/organization/orgdata'
import type RoleItem from './RoleItem'
import { sendCreateUserRoleRequest } from '@memberport/utils/api'

interface UserRoleFormProps {
  roles: RoleItem[]
  users: UserData[]
  organizations: OrgData[]
  onUserRoleChange: () => void
  id?: string
}

interface FormMessage {
  type: 'success' | 'warning' | 'danger'
  message: string
}

export const UserRoleForm: React.FC<UserRoleFormProps> = (props) => {
  const {
    id = 'user-role-form',
    roles,
    users,
    organizations,
    onUserRoleChange
  } = props

  const getEmptyUserRole = useMemo<UserRole>(
    () => {
      const userRole: UserRole = {
        id: '',
        user: '',
        role: '',
        organization: null
      }

      return userRole
    },
    []
  )

  const [formMessage, setFormMessage] = useState<FormMessage | null>(null)

  const timeoutRef = useRef<NodeJS.Timeout | null>(null)

  const submitForm = (event: FormEvent<HTMLFormElement>): boolean => {
    event.preventDefault()

    const userRole = getEmptyUserRole
    const form = event.target
    const formData = new FormData(form as HTMLFormElement)

    userRole.user = formData.get('user') as string
    userRole.role = formData.get('role') as string
    userRole.organization = formData.get('organization') as string

    if (userRole.role === ' ') {
      setFormMessage({
        message: 'Roll måste specifieras',
        type: 'warning'
      })

      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)

      return false
    }
    if (userRole.user === ' ') {
      setFormMessage({
        message: 'Användare måste specifieras',
        type: 'warning'
      })

      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)

      return false
    }
    if (userRole.organization === ' ') {
      userRole.organization = null
    }

    const request = sendCreateUserRoleRequest(userRole.role, userRole.user, userRole.organization)

    request.then((response) => {
      if (response.status === 201) {
        setFormMessage({
          message: 'Tilldelning utförd',
          type: 'success'
        })
        onUserRoleChange()
      } else {
        setFormMessage({
          message: 'Användaren har redan rollen eller så saknar du behörighet',
          type: 'danger'
        })
      }

      // Hide message in 3 secs
      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)
    }).catch(console.error)

    return true
  }

  const listRoles = useMemo<React.JSX.Element[]>(() => {
    const listItems = roles.map(role =>
      <option key={role.id} value={role.id}>
        {role.name}
      </option>
    )

    return listItems
  }, [roles])

  const listUsers = useMemo<React.JSX.Element[]>(() => {
    const listItems = users.map(user =>
      <option key={user.id} value={user.id}>
        {
        user.name.first !== undefined
          ? user.name.first + ' ' + user.name.last + ' - #' + user.number.toString()
          : user.name as unknown as string
      }
      </option>
    )

    return listItems
  }, [users])

  const listOrganizations = useMemo<React.JSX.Element[]>(() => {
    const listItems = organizations.map(organization =>
      <option key={organization.id} value={organization.id}>
        {organization.name}
      </option>
    )

    return listItems
  }, [organizations])

  return (
    <form id={id} onSubmit={submitForm}>
      <fieldset className="box fieldset">
        <legend>Tilldela ny roll</legend>
        <div className="field">
          <label
            className="label"
            htmlFor="role"
          >
            Roll
          </label>
          <div className="control">
            <div className="select is-fullwidth">
              <select
                name="role"
                defaultValue={' '}
              >
                <option disabled value=" ">Välj roll</option>
                {listRoles}
              </select>
            </div>
          </div>
        </div>
        <div className="field">
          <label
            className="label"
            htmlFor="user"
          >
            Användare
          </label>
          <div className="control">
            <div className="select is-fullwidth">
              <select
                name="user"
                defaultValue={' '}
              >
                <option disabled value=" ">Välj användare</option>
                {listUsers}
              </select>
            </div>
          </div>
        </div>
        <div className="field">
          <label
            className="label"
            htmlFor="organization"
          >
            Organisation
          </label>
          <div className="control">
            <div className="select is-fullwidth">
              <select
                name="organization"
                defaultValue={' '}
              >
                <option value=" ">Ingen</option>
                {listOrganizations}
              </select>
            </div>
          </div>
        </div>

        {formMessage !== null &&
          <div className={classNames('notification', `is-${formMessage.type}`, 'p-2')}>
            {formMessage.message}
          </div>
        }

        <hr className="my-4" />
        <div className="field is-grouped">
          <div className="control">
            <button type="submit" className="button is-primary">Spara</button>
          </div>
          <div className="control">
            <button type="reset" className="button is-primary is-light">Avbryt</button>
          </div>
          <div className="is-flex-grow-1" />
        </div>
      </fieldset>
    </form>
  )
}

export default UserRoleForm
