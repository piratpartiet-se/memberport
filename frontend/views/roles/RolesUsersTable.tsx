import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react'
import { AgGridReact } from '@ag-grid-community/react'
import type UserRole from './UserRole'
import { type UserData } from '@memberport/utils/user/userdata'
import { type OrgData } from '@memberport/utils/organization/orgdata'
import { type RoleData } from '@memberport/utils/user/roledata'
import { ButtonRenderer } from '@memberport/ag-components/button-renderer'
import { type UserRoleRow } from './UserRole'
import { sendDeleteUserRoleRequest } from '@memberport/utils/api'
import classNames from 'classnames'

interface RolesUsersTableProps {
  userRoles: UserRole[]
  roles: Record<string, RoleData>
  users: Record<string, UserData>
  organizations: Record<string, OrgData>
  onUserRoleDelete: () => void
  id?: string
  className?: string
}

interface FormMessage {
  type: 'success' | 'warning' | 'danger'
  message: string
}

export const RolesUsersTable: React.FC<RolesUsersTableProps> = (props) => {
  const {
    id = 'roles-users-table',
    userRoles,
    roles,
    users,
    organizations,
    onUserRoleDelete
  } = props

  const [rows, setRows] = useState<UserRoleRow[]>([])

  const [formMessage, setFormMessage] = useState<FormMessage | null>(null)

  const [columnDefs] = useState<any>([
    { headerName: 'Roll', field: 'role' },
    { headerName: 'Användare', field: 'user' },
    { headerName: 'Organisation', field: 'organization' },
    { headerName: 'Ta bort', field: 'delete', cellRenderer: ButtonRenderer, suppressSizeToFit: true }
  ])

  const defaultColDef = useMemo(() => ({
    sortable: true
  }), undefined)

  const gridRef = useRef<any>()
  const timeoutRef = useRef<NodeJS.Timeout | null>(null)

  useEffect(() => {
    const newRows: UserRoleRow[] = []

    userRoles.forEach((userRole) => {
      const row: UserRoleRow = {
        id: userRole.id,
        role: userRole.role,
        user: userRole.user,
        organization: userRole.organization,
        delete: {
          style: 'is-danger',
          label: 'Ta bort',
          onClick: () => {
            const response = sendDeleteUserRoleRequest(userRole.id)

            response.then((response) => {
              if (response.status === 200) {
                setFormMessage({
                  message: 'Tilldelad roll är borttagen',
                  type: 'success'
                })
              } else {
                setFormMessage({
                  message: 'Något gick fel när den tilldelade rollen skulle tas bort',
                  type: 'danger'
                })
              }

              timeoutRef.current = setTimeout(() => {
                setFormMessage(null)
              }, 3000)

              onUserRoleDelete()
            }).catch(console.error)
          }
        }
      }

      if (roles[row.role] !== undefined) {
        row.role = roles[row.role].name
      }

      if (users[row.user] !== undefined) {
        if (users[row.user].name.first !== undefined) {
          row.user = users[row.user].name.first + ' ' + users[row.user].name.last
        } else {
          row.user = users[row.user].name as unknown as string
        }
      }

      if (row.organization !== null && organizations[row.organization] !== undefined) {
        row.organization = organizations[row.organization].name
      } else {
        row.organization = ''
      }

      newRows.push(row)
    })

    setRows(newRows)
  }, [
    userRoles,
    roles,
    users,
    organizations
  ])

  const onFirstDataRendered = useCallback(() => {
    if (gridRef.current !== undefined && gridRef.current.api !== undefined && gridRef.current.api !== null) {
      gridRef.current.api.sizeColumnsToFit()
    }
  }, [])

  return (
    <div id={id}>
      <div className="ag-theme-alpine" style={{ minHeight: 400, height: '30vh', width: '100%' }}>
        <AgGridReact ref={gridRef} defaultColDef={defaultColDef} columnDefs={columnDefs} rowData={rows} onFirstDataRendered={onFirstDataRendered} />
      </div>
      <br />
      {formMessage !== null &&
        <div className={classNames('notification', `is-${formMessage.type}`, 'p-2')}>
          {formMessage.message}
        </div>
      }
    </div>
  )
}

export default RolesUsersTable
