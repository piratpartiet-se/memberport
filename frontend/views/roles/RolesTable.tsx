import React, {
  useCallback,
  useMemo,
  useRef,
  useState
} from 'react'
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model'
import { ModuleRegistry } from '@ag-grid-community/core'
import { AgGridReact } from '@ag-grid-community/react'
import type RoleItem from './RoleItem'
import { CheckboxRenderer } from '@memberport/ag-components/checkbox-renderer'

ModuleRegistry.registerModules([
  ClientSideRowModelModule
])

interface RolesTableProps {
  rows: RoleItem[]
  onEditClick?: (row: any) => any
  id?: string
  className?: string
}

export const RolesTable: React.FC<RolesTableProps> = (props) => {
  const {
    id = 'roles-table',
    onEditClick
  } = props

  const resized = useRef(false)

  const checkboxValue = (params: any): string => {
    return params.value.checked ? '1' : '0'
  }

  const [columnDefs] = useState<any>([
    { headerName: 'ID', field: 'id', resizable: false, maxWidth: 70 },
    { headerName: 'Roll', field: 'name' },
    { headerName: 'Beskrivning', field: 'description' },
    { headerName: 'Skapad', field: 'created' },
    { headerName: 'Nyheter', field: 'communicate_news', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Kallelse', field: 'communicate_meeting', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'SMS', field: 'communicate_sms', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Se medlemmar', field: 'get_members', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Hantera medlemmar', field: 'edit_members', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Föreningar', field: 'edit_organizations', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Geografi', field: 'edit_geography', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Kalender', field: 'edit_calendar', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Ta bort användardata', field: 'delete_user_data', valueParser: checkboxValue, cellRenderer: CheckboxRenderer },
    { headerName: 'Systemrättigheter', field: 'system', valueParser: checkboxValue, cellRenderer: CheckboxRenderer }
  ])

  const defaultColDef = useMemo(() => ({
    minWidth: 50,
    sortable: true,
    resizable: true
  }), undefined)

  const gridRef = useRef<AgGridReact>(null)

  function onResize (): void {
    if (gridRef.current?.api !== undefined && gridRef.current.api !== null) {
      if (!resized.current) {
        gridRef.current.api.sizeColumnsToFit()
        resized.current = true
      } else {
        gridRef.current.api.autoSizeAllColumns(false)
        resized.current = false
      }
    }
  }

  const onSelectionChanged = useCallback(() => {
    if (gridRef.current === null || onEditClick === undefined) {
      return
    }

    const selectedRows = gridRef.current.api.getSelectedRows()
    if (selectedRows.length === 1 && onEditClick !== null && onEditClick !== undefined) {
      const selectedRow = selectedRows[0]
      onEditClick(selectedRow)
    } else if (selectedRows.length === 0) {
      onEditClick(null)
    }
  }, [])

  return (
    <div id={id} className="ag-theme-alpine" style={{ minHeight: 500, height: '40vh', width: '100%' }}>
      <button className="button is-primary" onClick={onResize}>
        <span className="icon">
          <i className="fas fa-arrows-alt-h" />
        </span>
      </button>
      <br />
      <br />
      <AgGridReact
        ref={gridRef}
        defaultColDef={defaultColDef}
        columnDefs={columnDefs}
        rowData={props.rows}
        rowSelection="single"
        onSelectionChanged={onSelectionChanged}
      />
    </div>
  )
}

export default RolesTable
