import React, {
  type FormEvent,
  useEffect,
  useMemo,
  useState,
  useRef
} from 'react'
import type RoleItem from './RoleItem'
import { RoleItemToData } from './RoleItem'
import { sendCreateRoleRequest, sendDeleteRoleRequest, sendUpdateRoleRequest } from '@memberport/utils/api'
import classNames from 'classnames'

interface RoleFormProps {
  editRole: RoleItem | null
  onRoleChange: () => void
  onReset: () => void
  id?: string
}

interface FormMessage {
  type: 'success' | 'warning' | 'danger'
  message: string
}

export const RoleForm: React.FC<RoleFormProps> = (props) => {
  const {
    id = 'role-form',
    editRole,
    onRoleChange,
    onReset
  } = props

  const getEmptyOrEditRole = useMemo<RoleItem>(
    () => {
      if (editRole !== null) {
        return editRole
      }

      const roleItem: RoleItem = {
        id: '',
        name: '',
        description: '',
        created: '',
        communicate_news: {
          checked: false,
          disabled: false
        },
        communicate_meeting: {
          checked: false,
          disabled: false
        },
        communicate_sms: {
          checked: false,
          disabled: false
        },
        get_members: {
          checked: false,
          disabled: false
        },
        edit_members: {
          checked: false,
          disabled: false
        },
        edit_organizations: {
          checked: false,
          disabled: false
        },
        edit_calendar: {
          checked: false,
          disabled: false
        },
        edit_geography: {
          checked: false,
          disabled: false
        },
        edit_roles: {
          checked: false,
          disabled: false
        },
        delete_user_data: {
          checked: false,
          disabled: false
        },
        system: {
          checked: false,
          disabled: false
        }
      }

      return roleItem
    },
    [props.editRole]
  )

  const [newRole, setNewRole] = useState<RoleItem>(getEmptyOrEditRole)

  const [formMessage, setFormMessage] = useState<FormMessage | null>(null)

  const timeoutRef = useRef<NodeJS.Timeout | null>(null)

  const handleChange = (event): void => {
    const name = event.target.name
    const value = event.target.value

    setNewRole(newRole => ({ ...newRole, [name]: value }))
  }

  const handlePermission = (event): void => {
    const name = event.target.name
    const checked = event.target.checked

    setNewRole(newRole => ({ ...newRole, [name]: { checked, disabled: false } }))
  }

  const submitForm = (event: FormEvent): boolean => {
    event.preventDefault()

    const roleData = RoleItemToData(newRole)
    const request = editRole === null ? sendCreateRoleRequest(roleData) : sendUpdateRoleRequest(roleData)

    request.then((response) => {
      if (response.status === 200) {
        setFormMessage({
          message: 'Roll uppdaterad',
          type: 'success'
        })
        onRoleChange()
      } else if (response.status === 201) {
        setFormMessage({
          message: 'Roll skapad',
          type: 'success'
        })
        onRoleChange()
      } else {
        setFormMessage({
          message: editRole === null ? 'Något gick fel i skapandet av roll' : 'Något gick fel i uppdaterandet av roll',
          type: 'danger'
        })
      }

      // Hide message in 3 secs
      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)
    }).catch(console.error)

    return true
  }

  const submitDeleteRequest = (event: FormEvent): boolean => {
    event.preventDefault()

    if (editRole === null) {
      return false
    }

    const roleID = editRole.id
    const request = sendDeleteRoleRequest(roleID)

    request.then((response) => {
      if (response.status === 200) {
        setFormMessage({
          message: 'Rollen är borttagen',
          type: 'success'
        })
        onRoleChange()
      } else {
        setFormMessage({
          message: 'Något gick fel när rollen skulle tas bort',
          type: 'danger'
        })
      }

      // Hide message in 3 secs
      timeoutRef.current = setTimeout(() => {
        setFormMessage(null)
      }, 3000)
    }).catch(console.error)

    return true
  }

  useEffect(() => {
    setNewRole(getEmptyOrEditRole)
  }, [props.editRole])

  return (
    <form id={id} onSubmit={submitForm}>
      <fieldset className="box fieldset">
        <legend>{editRole === null ? 'Lägg till ny roll' : 'Ändra'}</legend>
        <div className="field">
          <label
            className="label"
            htmlFor="description"
          >
            Namn
          </label>
          <div className="control">
            <input
              className="input"
              type="text"
              name="name"
              placeholder="Namn"
              value={newRole.name}
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="field">
          <label
            className="label"
            htmlFor="description"
          >
            Beskrivning
          </label>
          <div className="control">
            <input
              className="input"
              type="text"
              name="description"
              placeholder="Beskrivning"
              value={newRole.description}
              onChange={handleChange}
            />
          </div>
        </div>
        <div className="field">
          <label
            className="label"
            htmlFor="description"
          >
            Rättigheter
          </label>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="communicate_news"
                checked={newRole.communicate_news.checked}
                onChange={handlePermission}
              /> Skapa nyheter
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="communicate_meeting"
                checked={newRole.communicate_meeting.checked}
                onChange={handlePermission}
              /> Kalla till möte
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="communicate_sms"
                checked={newRole.communicate_sms.checked}
                onChange={handlePermission}
              /> Skicka SMS
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="get_members"
                checked={newRole.get_members.checked}
                onChange={handlePermission}
              /> Se medlemmar
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="edit_members"
                checked={newRole.edit_members.checked}
                onChange={handlePermission}
              /> Hantera medlemmar
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="edit_organizations"
                checked={newRole.edit_organizations.checked}
                onChange={handlePermission}
              /> Redigera föreningar
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="edit_geography"
                checked={newRole.edit_geography.checked}
                onChange={handlePermission}
              /> Redigera geografi
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="edit_calendar"
                checked={newRole.edit_calendar.checked}
                onChange={handlePermission}
              /> Redigera kalendrar
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="delete_user_data"
                checked={newRole.delete_user_data.checked}
                onChange={handlePermission}
              /> Ta bort användardata
            </label>
          </div>
          <div className="control">
            <label className="checkbox">
              <input
                type="checkbox"
                name="system"
                checked={newRole.system.checked}
                onChange={handlePermission}
              /> Systemrättigheter (överskrider allt)
            </label>
          </div>
        </div>

        {formMessage !== null &&
          <div className={classNames('notification', `is-${formMessage.type}`, 'p-2')}>
            {formMessage.message}
          </div>
        }

        <hr className="my-4" />
        <div className="field is-grouped">
          <div className="control">
            <button type="submit" className="button is-primary">Spara</button>
          </div>
          <div className="control">
            <button type="reset" className="button is-primary is-light" onClick={onReset}>Avbryt</button>
          </div>
          <div className="is-flex-grow-1" />
          {editRole &&
            <div className="control">
              <button type="button" className="button is-danger" onClick={submitDeleteRequest}>Radera</button>
            </div>
        }
        </div>
      </fieldset>
    </form>
  )
}

export default RoleForm
