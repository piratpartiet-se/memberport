import { type RoleData } from '@memberport/utils/user/roledata'

interface CheckboxItem {
  checked: boolean
  disabled: boolean
}

export function RoleDataToItem (roleData: RoleData, disabled: boolean): RoleItem {
  const roleItem: RoleItem = {
    id: roleData.id,
    name: roleData.name,
    description: roleData.description,
    created: roleData.created,
    communicate_news: {
      checked: false,
      disabled
    },
    communicate_meeting: {
      checked: false,
      disabled
    },
    communicate_sms: {
      checked: false,
      disabled
    },
    get_members: {
      checked: false,
      disabled
    },
    edit_members: {
      checked: false,
      disabled
    },
    edit_organizations: {
      checked: false,
      disabled
    },
    edit_calendar: {
      checked: false,
      disabled
    },
    edit_geography: {
      checked: false,
      disabled
    },
    edit_roles: {
      checked: false,
      disabled
    },
    delete_user_data: {
      checked: false,
      disabled
    },
    system: {
      checked: false,
      disabled
    }
  }

  roleData.permissions.forEach((p) => {
    roleItem[p.id].checked = true
  })

  return roleItem
}

export function RoleItemToData (roleItem: RoleItem): RoleData {
  const roleData: RoleData = {
    id: roleItem.id,
    name: roleItem.name,
    description: roleItem.description,
    created: roleItem.created,
    permissions: []
  }

  if (roleItem.communicate_news.checked) {
    roleData.permissions.push({
      id: 'communicate_news',
      name: ''
    })
  }
  if (roleItem.communicate_meeting.checked) {
    roleData.permissions.push({
      id: 'communicate_meeting',
      name: ''
    })
  }
  if (roleItem.communicate_sms.checked) {
    roleData.permissions.push({
      id: 'communicate_sms',
      name: ''
    })
  }
  if (roleItem.get_members.checked) {
    roleData.permissions.push({
      id: 'get_members',
      name: ''
    })
  }
  if (roleItem.edit_members.checked) {
    roleData.permissions.push({
      id: 'edit_members',
      name: ''
    })
  }
  if (roleItem.edit_organizations.checked) {
    roleData.permissions.push({
      id: 'edit_organizations',
      name: ''
    })
  }
  if (roleItem.edit_calendar.checked) {
    roleData.permissions.push({
      id: 'edit_calendar',
      name: ''
    })
  }
  if (roleItem.edit_geography.checked) {
    roleData.permissions.push({
      id: 'edit_geography',
      name: ''
    })
  }
  if (roleItem.edit_roles.checked) {
    roleData.permissions.push({
      id: 'edit_roles',
      name: ''
    })
  }
  if (roleItem.delete_user_data.checked) {
    roleData.permissions.push({
      id: 'delete_user_data',
      name: ''
    })
  }
  if (roleItem.system.checked) {
    roleData.permissions.push({
      id: 'system',
      name: ''
    })
  }

  return roleData
}

export default interface RoleItem {
  id: string
  name: string
  description: string
  created: string
  communicate_news: CheckboxItem
  communicate_meeting: CheckboxItem
  communicate_sms: CheckboxItem
  get_members: CheckboxItem
  edit_members: CheckboxItem
  edit_organizations: CheckboxItem
  edit_geography: CheckboxItem
  edit_calendar: CheckboxItem
  edit_roles: CheckboxItem
  delete_user_data: CheckboxItem
  system: CheckboxItem
}
