export default interface UserRole {
  id: string
  role: string
  user: string
  organization: string | null
}

export interface UserRoleRow {
  id: string
  role: string
  user: string
  organization: string | null
  delete: {
    style: 'is-danger'
    label: 'Ta bort'
    onClick: () => void
  }
}
