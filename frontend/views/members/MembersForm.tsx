import React, {
  type FormEvent,
  useState,
  useRef,
  useMemo,
  useEffect
} from 'react'
import classNames from 'classnames'
import { type GeoData } from '@memberport/utils/geography/geodata'
import { type UserData } from '@memberport/utils/user/userdata'
import { sendCreateUserRequest, sendUpdateUserRequest } from '@memberport/utils/api'

interface MembersFormProp {
  id?: string
  countries: Record<string, GeoData>
  municipalities: Record<string, GeoData>
  onMemberChange: () => void
  onReset: () => void
  selectedMember: UserData | null
}

interface FormMessage {
  type: 'success' | 'warning' | 'danger'
  message: string
}

export const MembersForm: React.FC<MembersFormProp> = (props) => {
  const {
    id = 'members-form',
    countries,
    municipalities,
    onMemberChange,
    onReset,
    selectedMember
  } = props

  const getNewOrSelectedMember = useMemo<UserData>(() => {
    if (selectedMember !== null) {
      return selectedMember
    }

    const emptyMember: UserData = {
      id: '',
      schema: 'member',
      number: '',
      name: {
        first: '',
        last: ''
      },
      email: '',
      phone: '',
      postal_address: {
        street: '',
        postal_code: '',
        city: ''
      },
      municipality_id: '',
      gender: 'choose',
      birthday: '',
      verified: '',
      created: ''
    }

    return emptyMember
  }, [selectedMember])

  const [memberData, setMemberData] = useState<UserData>(getNewOrSelectedMember)

  const [country, setCountry] = useState<string>('')

  const [formMessage, setFormMessage] = useState<FormMessage | null>(null)

  const timeoutRef = useRef<NodeJS.Timeout | null>(null)

  const handleChange = (event): void => {
    const name = event.target.name
    const value = event.target.value

    if (name === 'first' || name === 'last') {
      const nameObject = {
        first: name === 'first' ? value : memberData.name.first,
        last: name === 'last' ? value : memberData.name.last
      }

      setMemberData({ ...memberData, name: nameObject }); return
    } else if (name === 'street' || name === 'postal_code' || name === 'city') {
      const postalAddress = {
        street: name === 'street' ? value : memberData.postal_address.street,
        postal_code: name === 'postal_code' ? value : memberData.postal_address.postal_code,
        city: name === 'city' ? value : memberData.postal_address.city
      }

      setMemberData({ ...memberData, postal_address: postalAddress }); return
    }

    setMemberData({ ...memberData, [name]: value })
  }

  const handleCountryChange = (event): void => {
    const value = event.target.value

    setMemberData({ ...memberData, municipality_id: '' })
    setCountry(value)
  }

  const submitForm = (event: FormEvent): boolean => {
    event.preventDefault()

    const request = selectedMember === null ? sendCreateUserRequest(memberData) : sendUpdateUserRequest(memberData)

    request.then((response) => {
      if (selectedMember === null) {
        if (response.status === 201) {
          setFormMessage({
            message: 'Ny medlem skapad',
            type: 'success'
          })
          onMemberChange()
        } else if (response.status === 400) {
          setFormMessage({
            message: 'Gick inte att skapa medlem, är telefonnumret i rätt form?',
            type: 'danger'
          })
        } else if (response.status === 409) {
          setFormMessage({
            message: 'Mejladress eller telefonnummer finns redan registrerad',
            type: 'danger'
          })
        } else {
          setFormMessage({
            message: 'Något gick fel i skapandet av ny medlem',
            type: 'danger'
          })
        }
      } else {
        if (response.status === 200) {
          setFormMessage({
            message: 'Medlem uppdaterad',
            type: 'success'
          })
          onMemberChange()
        } else if (response.status === 409) {
          setFormMessage({
            message: 'Mejladress eller telefonnummer finns redan registrerad',
            type: 'danger'
          })
        } else {
          setFormMessage({
            message: 'Något gick fel i ändringen av medlem',
            type: 'danger'
          })
        }
      }
    }).catch(console.error)

    timeoutRef.current = setTimeout(() => {
      setFormMessage(null)
    }, 3000)

    return true
  }

  const listCountries = useMemo<React.JSX.Element[]>(() => {
    const countriesList = Object.values(countries)
    const listItems = countriesList.map(country =>
      <option key={country.id} value={country.id}>
        {country.name}
      </option>
    )

    return listItems
  }, [countries])

  const listMunicipalities = useMemo<React.JSX.Element[]>(() => {
    const municipalitiesList = Object.values(municipalities)
    const municipalitiesByCountry = municipalitiesList.filter(municipality => {
      return municipality.country_id === country
    })

    const listItems = municipalitiesByCountry.map(municipality =>
      <option key={municipality.id} value={municipality.id}>
        {municipality.name}
      </option>
    )

    return listItems
  }, [municipalities, country])

  useEffect(() => {
    const memberData = getNewOrSelectedMember
    setMemberData({ ...memberData })

    if (memberData.municipality_id !== '') {
      const municipality = municipalities[memberData.municipality_id]

      if (municipality === undefined) {
        setCountry('')
        return
      }

      setCountry(municipality.country_id ?? '')
    } else {
      setCountry('')
    }
  }, [props.selectedMember])

  return (
    <form id={id} onSubmit={submitForm}>
      <fieldset className="box fieldset">
        <legend>{selectedMember === null ? 'Lägg till ny medlem' : 'Ändra'}</legend>
        <div className="field is-horizontal">
          <div className="field-body">
            <div className="field">
              <label
                className="label"
                htmlFor="firstname"
              >
                Förnamn
              </label>
              <div className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  name="first"
                  placeholder="Förnamn"
                  value={memberData.name.first}
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
            <div className="field">
              <label
                className="label"
                htmlFor="lastname"
              >
                Efternamn
              </label>
              <div className="control is-expanded">
                <input
                  className="input"
                  type="text"
                  name="last"
                  placeholder="Efternamn"
                  value={memberData.name.last}
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
          </div>
        </div>
        <div className="field is-horizontal">
          <div className="field-body">
            <div className="field">
              <label
                className="label"
                htmlFor="email"
              >
                E-post
              </label>
              <div className="control is-expanded">
                <input
                  className="input"
                  type="email"
                  name="email"
                  placeholder="E-post"
                  value={memberData.email}
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
            <div className="field">
              <label
                className="label"
                htmlFor="phone"
              >
                Telefonnummer
              </label>
              <div className="control is-expanded">
                <input
                  className="input"
                  type="tel"
                  name="phone"
                  placeholder="Telefonnummer"
                  value={memberData.phone}
                  onChange={handleChange}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="field is-horizontal">
          <div className="field-body">
            <div className="field">
              <label
                className="label"
                htmlFor="street"
              >
                Gatuadress
              </label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  name="street"
                  placeholder="Gatuadress"
                  value={memberData.postal_address.street}
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
            <div className="field">
              <label
                className="label"
                htmlFor="postal"
              >
                Postnummer
              </label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  name="postal_code"
                  placeholder="Postnummer"
                  value={memberData.postal_address.postal_code}
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
            <div className="field">
              <label
                className="label"
                htmlFor="city"
              >
                Postort
              </label>
              <div className="control">
                <input
                  className="input"
                  type="text"
                  name="city"
                  placeholder="Postort"
                  value={memberData.postal_address.city}
                  onChange={handleChange}
                  required
                />
              </div>
            </div>
          </div>
        </div>
        <div className="field">
          <label
            className="label"
            htmlFor="country"
          >
            Land
          </label>
          <div className="control">
            <div className="select is-fullwidth">
              <select
                name="country"
                value={country}
                onChange={handleCountryChange}
                required
              >
                <option disabled value="">Välj land</option>
                {listCountries}
              </select>
            </div>
          </div>
        </div>
        <div className="field">
          <label
            className="label"
            htmlFor="municipality_id"
          >
            Kommun
          </label>
          <div className="control">
            <div className="select is-fullwidth">
              <select
                name="municipality_id"
                value={memberData.municipality_id}
                onChange={handleChange}
                disabled={country === ''}
                required
              >
                <option disabled value="">Välj kommun</option>
                {listMunicipalities}
              </select>
            </div>
          </div>
        </div>
        <div className="field is-horizontal">
          <div className="field-body">
            <div className="field">
              <label
                className="label"
                htmlFor="birthday"
              >
                Födelsedag
              </label>
              <div className="control">
                <input
                  className="input"
                  type="date"
                  name="birthday"
                  placeholder="Födelsedag"
                  value={memberData.birthday} onChange={handleChange}
                  required
                />
              </div>
            </div>
            <div className="field">
              <label
                className="label"
                htmlFor="gender"
              >
                Kön
              </label>
              <div className="control">
                <div className="select is-fullwidth">
                  <select name="gender" value={memberData.gender} onChange={handleChange} required>
                    <option disabled value="choose">Välj kön</option>
                    <option value="male">Man</option>
                    <option value="female">Kvinna</option>
                    <option value="other">Annan</option>
                    <option value="unknown">Vill inte uppge</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>

        {formMessage !== null &&
          <div className={classNames('notification', `is-${formMessage.type}`, 'p-2')}>
            {formMessage.message}
          </div>
        }

        <hr className="my-4" />
        <div className="field is-grouped">
          <div className="control">
            <button type="submit" className="button is-primary">Spara</button>
          </div>
          <div className="control">
            <button type="button" className="button is-primary is-light" onClick={onReset}>Avbryt</button>
          </div>
          <div className="is-flex-grow-1" />
        </div>
      </fieldset>

    </form>
  )
}

export default MembersForm
