import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react'
import classNames from 'classnames'
import { type UserData } from '@memberport/utils/user/userdata'
import { sendAreasRequest, sendCountriesRequest, sendMunicipalityRequest, sendOrganizationsRequest, sendUsersRequest } from '@memberport/utils/api'
import { type ColDef } from '@ag-grid-community/core'
import { AgGridReact } from '@ag-grid-community/react'
import MembersForm from './MembersForm'
import { GEO_TYPES, type GeoData } from '@memberport/utils/geography/geodata'
import { displayGender } from '@memberport/utils/ui'
import MembershipsView from '../profile/MembershipsView'
import OrganizationTree from '../organization/OrganizationTree'
import GeographyTree from '../geography/GeographyTree'
import MunicipalityFilter, { type IMunicipalityFilterModel } from '@memberport/ag-components/MunicipalityFilter'
import { type OrgData } from '@memberport/utils/organization/orgdata'

interface IUser {
  id: string
  number: number
  name: string
  email: string
  phone: string
  birthday: string
  gender: string
  postalCode: string
  city: string
  street: string
  municipality: string
  municipalityID: string
  country: string
}

interface MembersViewProps {
  id?: string
  className?: string
}

export const MembersView: React.FC<MembersViewProps> = (props) => {
  const {
    id = 'members-view',
    className
  } = props

  const organizationsInitialized = useRef(false)
  const resized = useRef(true)

  const [membersData, setMembersData] = useState<UserData[]>([])
  const [rowData, setRowData] = useState<IUser[]>([])
  const [countries, setCountries] = useState<Record<string, GeoData>>({})
  const [areas, setAreas] = useState<Record<string, GeoData>>({})
  const [municipalities, setMunicipalities] = useState<Record<string, GeoData>>({})
  const [organizations, setOrganizations] = useState<Record<string, OrgData>>({})
  const [selectedMember, setSelectedMember] = useState<UserData | null>(null)
  const [selectedOrganizations, setSelectedOrganizations] = useState<string[]>([])

  const [columnDefs] = useState<ColDef[]>([
    { headerName: '#', field: 'number' },
    { headerName: 'Namn', field: 'name', filter: 'agTextColumnFilter' },
    { headerName: 'E-mail', field: 'email', filter: 'agTextColumnFilter' },
    { headerName: 'Telefon', field: 'phone' },
    { headerName: 'Födelsedag', field: 'birthday' },
    { headerName: 'Kön', field: 'gender' },
    { headerName: 'Postkod', field: 'postalCode' },
    { headerName: 'Postort', field: 'city' },
    { headerName: 'Adress', field: 'street' },
    { headerName: 'Kommun', field: 'municipality', filter: MunicipalityFilter },
    { headerName: 'Land', field: 'country' }
  ])

  const defaultColDef = useMemo(() => ({
    sortable: true,
    floatingFilter: true
  }), undefined)

  const gridRef = useRef<AgGridReact<IUser>>(null)

  const timeoutRef = useRef<NodeJS.Timeout | null>(null)

  function getMembers (): void {
    sendUsersRequest(selectedOrganizations)
      .then(async (response) => {
        if (response.status === 403) {
          return
        }

        const respJson = await response.json()
        const membersData: UserData[] = respJson.data
        setMembersData(membersData)
      })
      .catch(console.error)
  }

  function getCountries (): void {
    sendCountriesRequest().then(async (response) => {
      const respJson = await response.json()

      if (respJson.data === undefined || respJson.data === null) {
        setCountries({})
        return
      }

      const countries: Record<string, GeoData> = respJson.data

      Object.keys(countries).forEach((countryID: string) => {
        countries[countryID].type = GEO_TYPES.COUNTRY
      })

      setCountries(countries)
    }
    ).catch(console.error)
  }

  function getAreas (): void {
    sendAreasRequest(null).then(async (response) => {
      const respJson = await response.json()

      if (respJson.data === undefined || respJson.data === null) {
        setAreas({})
        return
      }

      const areas: Record<string, GeoData> = respJson.data

      Object.keys(areas).forEach((areaID: string) => {
        areas[areaID].type = GEO_TYPES.AREA
      })

      setAreas(areas)
    }
    ).catch(console.error)
  }

  function getMunicipalities (): void {
    sendMunicipalityRequest(null).then(async (response) => {
      const respJson = await response.json()

      if (respJson.data === undefined || respJson.data === null) {
        setMunicipalities({})
        return
      }

      const municipalities: Record<string, GeoData> = respJson.data

      Object.keys(municipalities).forEach((municipalityID: string) => {
        municipalities[municipalityID].type = GEO_TYPES.MUNICIPALITY
      })

      setMunicipalities(municipalities)
    }
    ).catch(console.error)
  }

  function getOrganizations (): void {
    sendOrganizationsRequest().then(async (response) => {
      if (response.status === 200) {
        const respJson = await response.json()
        const organizations: Record<string, OrgData> = respJson.data
        setOrganizations(organizations)
      } else {
        setOrganizations({})
      }
    }).catch(console.error)
  }

  function getRows (): void {
    const rowData: IUser[] = []

    membersData.forEach((member) => {
      let countryName = ''
      let municipalityName = ''

      const municipality = municipalities[member.municipality_id]
      if (municipality !== undefined) {
        municipalityName = municipality.name

        if (municipality.country_id !== undefined) {
          const country = countries[municipality.country_id]
          countryName = country !== undefined ? country.name : ''
        }
      }

      const row: IUser = {
        id: member.id,
        number: parseInt(member.number),
        name: member.name.first + ' ' + member.name.last,
        email: member.email,
        phone: member.phone,
        birthday: member.birthday,
        gender: displayGender(member.gender),
        postalCode: member.postal_address.postal_code,
        city: member.postal_address.city,
        street: member.postal_address.street,
        municipality: municipalityName,
        municipalityID: member.municipality_id,
        country: countryName
      }

      rowData.push(row)
    })

    setRowData(rowData)
  }

  const onMemberChange = useCallback(() => {
    getMembers()
  }, [selectedOrganizations])

  const onReset = useCallback(() => {
    setSelectedMember(null)
  }, [])

  function fromRowToUser (row: IUser): UserData | null {
    const memberID = row.id

    if (memberID === undefined || memberID === null) {
      return null
    }

    const member = membersData.find((member) => {
      return member.id === memberID
    })

    return member ?? null
  }

  const onSelectionChanged = useCallback(() => {
    if (gridRef.current === null) {
      return
    }

    const selectedRows = gridRef.current.api.getSelectedRows()

    if (selectedRows.length === 1) {
      const selectedRow = selectedRows[0]
      const selectedUser = fromRowToUser(selectedRow)

      setSelectedMember(selectedUser)
    } else if (selectedRows.length === 0) {
      setSelectedMember(null)
    }
  }, [membersData, rowData])

  const onOrganizationChanged = (organizationIDs: string[]): void => {
    setSelectedOrganizations(organizationIDs)
  }

  const onGeographyChanged = (selectedCountries: Record<string, boolean>, selectedAreas: Record<string, boolean>, selectedMunicipalities: Record<string, boolean>): void => {
    if (gridRef.current?.api === undefined || gridRef.current.api === null) {
      return
    }

    const model: IMunicipalityFilterModel = {
      municipalityIDs: selectedMunicipalities
    }

    gridRef.current.api.setColumnFilterModel('municipality', model).then(() => {
      gridRef.current?.api.onFilterChanged()
    }).catch(console.error)
  }

  useEffect(() => {
    getCountries()
    getAreas()
    getMunicipalities()
    getOrganizations()

    return () => {
      if (timeoutRef.current !== null) {
        clearTimeout(timeoutRef.current)
      }
    }
  }, [])

  const isMembersLoaded = useMemo<boolean>(() => {
    return Object.keys(membersData).length > 0
  }, [membersData])

  const isCountriesLoaded = useMemo<boolean>(() => {
    return Object.keys(countries).length > 0
  }, [countries])

  const isAreasLoaded = useMemo<boolean>(() => {
    return Object.keys(areas).length > 0
  }, [areas])

  const isMunicipalitiesLoaded = useMemo<boolean>(() => {
    return Object.keys(municipalities).length > 0
  }, [municipalities])

  const isOrganizationsLoaded = useMemo<boolean>(() => {
    if (!organizationsInitialized.current) {
      const isLoaded = Object.keys(organizations).length > 0 && selectedOrganizations.length > 0

      if (isLoaded) {
        organizationsInitialized.current = true
      }

      return isLoaded
    }

    return true
  }, [selectedOrganizations, organizations])

  useEffect(() => {
    if (isCountriesLoaded && isAreasLoaded && isMunicipalitiesLoaded && isOrganizationsLoaded) {
      getMembers()
    }
  }, [selectedOrganizations, countries, areas, municipalities, organizations])

  useEffect(() => {
    if (isMembersLoaded && isCountriesLoaded && isMunicipalitiesLoaded) {
      getRows()
    }
  }, [membersData, municipalities, countries])

  const onFirstDataRendered = useCallback(() => {
    if (gridRef.current !== null) {
      gridRef.current.api.sizeColumnsToFit()
    }
  }, [])

  function onResize (): void {
    if (gridRef.current !== null) {
      if (!resized.current) {
        gridRef.current.api.sizeColumnsToFit()
        resized.current = true
      } else {
        gridRef.current.api.autoSizeAllColumns(false)
        resized.current = false
      }
    }
  }

  return (
    <div id={id} className={classNames('section', className)}>
      <div className="container is-fluid">
        <h1 className="title">Medlemmar</h1>
        <div className="columns">
          <div className="column is-half">
            <OrganizationTree onSelectionChanged={onOrganizationChanged} organizations={organizations} />
          </div>
          <div className="column is-half">
            <GeographyTree onSelectionChanged={onGeographyChanged} countries={countries} areas={areas} municipalities={municipalities} />
          </div>
        </div>
        <br />
        <br />
        <div className="columns">
          <div className="column is-two-thirds">
            <div className="ag-theme-alpine" style={{ minHeight: 400, height: '60vh', width: '100%' }}>
              <button className="button is-primary" onClick={onResize}>
                <span className="icon">
                  <i className="fas fa-arrows-alt-h" />
                </span>
              </button>
              <br />
              <br />
              <AgGridReact
                ref={gridRef}
                defaultColDef={defaultColDef}
                columnDefs={columnDefs}
                rowData={rowData}
                rowSelection="single"
                onFirstDataRendered={onFirstDataRendered}
                onSelectionChanged={onSelectionChanged}
              />
            </div>
          </div>
          <div className="column is-third">
            <MembersForm countries={countries} municipalities={municipalities} onMemberChange={onMemberChange} onReset={onReset} selectedMember={selectedMember} />
            <MembershipsView userID={selectedMember !== null ? selectedMember.id : ''} municipalityID={selectedMember !== null ? selectedMember.municipality_id : null} preloadedOrgs={organizations} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default MembersView
