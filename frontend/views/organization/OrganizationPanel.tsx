import { type OrgData } from '@memberport/utils/organization/orgdata'
import classNames from 'classnames'
import React, {
  useState
} from 'react'

interface OrganizationPanelProps {
  id?: string
  className?: string
  organizationName: string
  tree: Record<string, OrgData[]>
  initiallySelected: boolean
  onSelectionChanged: (organizationID: string, selected: boolean) => void
}

export const OrganizationPanel: React.FC<OrganizationPanelProps> = (props) => {
  const {
    id = 'organization-panel',
    className,
    organizationName,
    tree,
    initiallySelected,
    onSelectionChanged
  } = props

  const [showChildren, setShowChildren] = useState<boolean>(true)
  const [selected, setSelected] = useState<boolean>(initiallySelected)

  function handleSelected (event): void {
    event.stopPropagation()

    const newSelected = !selected

    setSelected(newSelected)
    onSelectionChanged(id, newSelected)
  }

  return (
    <div id={id} className={classNames(selected ? 'organization' : '', className)}>
      <div className="panel-block">
        <button type="button" className="no-button" onClick={handleSelected}>
          <label className="checkbox">
            <input type="checkbox" checked={selected} readOnly />
          </label>
          <span className="panel-icon" style={{ verticalAlign: 'middle' }}>
            <i className="fas fa-sitemap" />
          </span>
          {organizationName}
        </button>
        <button type="button" className="is-flex is-flex-grow-1 is-justify-content-flex-end is-align-items-center is-align-self-stretch no-button" onClick={() => { setShowChildren(!showChildren) }}>
          <span className="is-align-self-stretch">
            <i className={classNames('fas', showChildren ? 'fa-angle-down' : 'fa-angle-up')} />
          </span>
        </button>
      </div>
      {showChildren
        ? tree[id].map((organization) => (
          <OrganizationPanel
            key={organization.id}
            id={organization.id}
            className="indent"
            organizationName={organization.name}
            tree={tree}
            initiallySelected={false}
            onSelectionChanged={onSelectionChanged}
          />
        ))
        : null}
    </div>
  )
}

export default OrganizationPanel
