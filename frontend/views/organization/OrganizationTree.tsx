import { type OrgData } from '@memberport/utils/organization/orgdata'
import React, {
  useEffect,
  useState
} from 'react'
import OrganizationPanel from './OrganizationPanel'
import classNames from 'classnames'

interface OrganizationTreeProps {
  id?: string
  className?: string
  onSelectionChanged: (organizationIDs: string[]) => void
  organizations: Record<string, OrgData>
}

export const OrganizationTree: React.FC<OrganizationTreeProps> = (props) => {
  const {
    id = 'organization-tree',
    className,
    onSelectionChanged,
    organizations
  } = props

  const [roots, setRoots] = useState<OrgData[]>([])
  const [tree, setTree] = useState<Record<string, OrgData[]>>({})
  const [searchText, setSearchText] = useState<string>('')
  const [selectedOrganizations, setSelectedOrganizations] = useState<string[]>([])

  const onOrganizationSelected = (organizationID: string, selected: boolean): void => {
    const newSelection = [...selectedOrganizations]

    if (selected) {
      newSelection.push(organizationID)
    } else {
      const index = newSelection.indexOf(organizationID)
      newSelection.splice(index, 1)
    }

    setSelectedOrganizations(newSelection)
  }

  function onSearchChange (event): void {
    const value = event.target.value

    setSearchText(value)
  }

  useEffect(() => {
    const tree: Record<string, OrgData[]> = {}
    const roots: OrgData[] = []

    Object.values(organizations).forEach(organization => {
      const paths = organization.path.split('.')

      if (paths.length > 1) {
        const parent = paths[paths.length - 2]

        if (!tree[parent]) {
          tree[parent] = []
        }

        tree[parent].push(organization)
      } else {
        roots.push(organization)
      }

      if (!tree[organization.id]) {
        tree[organization.id] = []
      }
    })

    const selected: string[] = []

    roots.forEach((root) => {
      selected.push(root.id)
    })

    setSelectedOrganizations(selected)
    setRoots(roots)
    setTree(tree)
  }, [organizations])

  useEffect(() => {
    onSelectionChanged(selectedOrganizations)
  }, [selectedOrganizations])

  return (
    <div id={id} className={classNames(className, 'panel', 'is-primary')}>
      <p className="panel-heading">
        Föreningar
      </p>
      <div className="panel-block">
        <p className="control has-icons-left">
          <input className="input is-primary" type="text" placeholder="Sök" value={searchText} onChange={onSearchChange} />
          <span className="icon is-left">
            <i className="fas fa-search" aria-hidden="true" />
          </span>
        </p>
      </div>
      {roots.map(organization => (
        <OrganizationPanel
          key={organization.id}
          id={organization.id}
          organizationName={organization.name}
          tree={tree}
          initiallySelected
          onSelectionChanged={onOrganizationSelected}
        />
      ))}
    </div>
  )
}

export default OrganizationTree
