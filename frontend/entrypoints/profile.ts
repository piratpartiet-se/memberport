import React from 'react'
import ReactDOM from 'react-dom/client'
import { updateMunicipalities } from '@memberport/utils/api'
import { afterPageLoad } from '../utils/after-page-load'
import MembershipsView from '@memberport/views/profile/MembershipsView'

afterPageLoad().then(() => {
  const countrySelect = document.getElementById('country')

  if (countrySelect !== null) {
    countrySelect.onchange = updateMunicipalities
  }

  const deleteButtons = document.getElementsByClassName('delete')

  for (let index = 0; index < deleteButtons.length; index++) {
    const deleteButton = deleteButtons.item(index) as HTMLButtonElement

    deleteButton.onclick = () => {
      const notification = deleteButton.parentElement
      notification?.classList.add('is-hidden')
    }
  }

  const membershipsViewRootEl = document.getElementById('memberships-view-root')

  if (membershipsViewRootEl !== null) {
    const userID = membershipsViewRootEl.getAttribute('user-id') ?? ''
    const municipalityID = membershipsViewRootEl.getAttribute('municipality-id')
    const preloadedOrgs = null

    ReactDOM
      .createRoot(membershipsViewRootEl)
      .render(React.createElement(MembershipsView, { userID, municipalityID, preloadedOrgs }))
  }
}).catch(console.error)
