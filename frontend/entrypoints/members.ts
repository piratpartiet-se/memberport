import React, { StrictMode } from 'react'
import ReactDOM from 'react-dom/client'
import { afterPageLoad } from '../utils/after-page-load'
import MembersView from '@memberport/views/members/MembersView'
import '@memberport/sass/selection.scss'
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model'
import { ModuleRegistry } from '@ag-grid-community/core'

let membersViewRootEl: HTMLElement | null = null

ModuleRegistry.registerModules([
  ClientSideRowModelModule
])

afterPageLoad().then(() => {
  membersViewRootEl = document.getElementById('members-view-root')
  if (membersViewRootEl !== null) {
    ReactDOM
      .createRoot(membersViewRootEl)
      .render(React.createElement(StrictMode, null, React.createElement(MembersView, {})))
  }
}).catch(console.error)
