import React from 'react'
import ReactDOM from 'react-dom/client'
import { afterPageLoad } from '../utils/after-page-load'
import RolesView from '@memberport/views/roles/RolesView'
import { ModuleRegistry } from '@ag-grid-community/core'
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model'

let rolesViewRootEl: HTMLElement | null = null

ModuleRegistry.registerModules([
  ClientSideRowModelModule
])

afterPageLoad().then(() => {
  rolesViewRootEl = document.getElementById('roles-view-root')
  if (rolesViewRootEl !== null) {
    ReactDOM
      .createRoot(rolesViewRootEl)
      .render(React.createElement(RolesView, {}))
  }
}).catch(console.error)
