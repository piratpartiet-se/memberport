import type { GeoData } from './geography/geodata'
import { type RoleData } from './user/roledata'
import { type UserData } from './user/userdata'

type DataBody = Record<string, string>

function convertDictToBody (dict: DataBody): string {
  return Object.keys(dict).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(dict[key])).join('&')
}

export function updateMunicipalities (): void {
  const countryInput = document.getElementById('country') as HTMLSelectElement
  const countryName = countryInput.value

  sendMunicipalityRequest(countryName)
    .then(async (response: Response) => {
      return await response.json()
    })
    .then((response: Record<string, any>) => {
      const municipalities = document.getElementById('traits.municipality_id')
      if (municipalities == null) {
        return
      }

      while (municipalities.firstChild != null) {
        municipalities.removeChild(municipalities.firstChild)
      }

      const selectedValue = municipalities.dataset.value

      let newHTML = '<option disabled selected value>Välj din kommun</option>'

      for (const [_id, municipality] of Object.entries<DataBody>(response.data)) {
        const selectedStr = selectedValue === municipality.name ? ' selected' : ''
        newHTML += '<option value="' + municipality.id + '" ' + selectedStr + '>' + municipality.name + '</option>\n'
      }

      municipalities.innerHTML = newHTML
    })
    .catch((error: string) => {
      console.error(error)
    })
}

export async function sendMunicipalityRequest (countryID: string | null): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  let urlParam = ''
  if (countryID !== null) {
    urlParam = `?country=${countryID}`
  }

  const response = await fetch('/api/geography/municipalities' + urlParam, {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendMembershipsRequest (userID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/user/memberships/' + userID, {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendCreateMembershipRequest (userID: string, orgID: string): Promise<Response> {
  const data = {
    organization: orgID,
    user: userID
  }

  const dataBody = convertDictToBody(data)
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/membership', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    body: dataBody,
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendEndMembershipRequest (membershipID: string, reason: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/membership/' + membershipID, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify({ reason }),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendRenewMembershipRequest (membershipID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/membership/' + membershipID, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendCountriesRequest (): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/countries', {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateCountryDataRequest (country: GeoData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/country/' + country.id, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(country),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendAreasRequest (countryID: string | null): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  let urlParam = ''
  if (countryID !== null) {
    urlParam = `?country=${countryID}`
  }

  const response = await fetch('/api/geography/areas' + urlParam, {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateAreaDataRequest (area: GeoData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/area/' + area.id, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(area),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateAreasRequest (areas: Record<string, GeoData>): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/areas', {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(areas),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateMunicipalityDataRequest (municipality: GeoData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/municipality/' + municipality.id, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(municipality),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateMunicipalitiesRequest (municipalities: Record<string, GeoData>): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/municipalities', {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(municipalities),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendDeleteCountryRequest (countryID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/country/' + countryID, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    body: null,
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendDeleteAreaRequest (areaID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/area/' + areaID, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    body: null,
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendDeleteMunicipalityRequest (municipalityID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/geography/municipality/' + municipalityID, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    body: null,
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendOrganizationsRequest (municipalityID: string | null = null): Promise<Response> {
  let urlArgument = ''

  if (municipalityID !== null) {
    urlArgument = '?municipality=' + municipalityID
  }

  const response = await fetch('/api/organizations' + urlArgument, {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  })

  return response
}

export async function sendCreateOrganizationRequest (
  name: string,
  description: string,
  active: boolean,
  showOnSignUp: boolean,
  parentID: string | null,
  countries: string | null,
  areas: string | null,
  municipalities: string | null
): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement
  const data: DataBody = {
    name,
    description,
    active: String(active),
    show_on_signup: String(showOnSignUp)
  }

  if (parentID != null) {
    data.parent_id = parentID
  }
  if (countries != null) {
    data.countries = countries
  }
  if (areas != null) {
    data.areas = areas
  }
  if (municipalities != null) {
    data.municipalities = municipalities
  }

  const body = convertDictToBody(data)
  const response = await fetch('/api/organization', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body
  })

  return response
}

export async function sendUpdateOrganizationRequest (
  organizationID: string,
  name: string,
  description: string,
  active: boolean,
  showOnSignUp: boolean,
  parentID: string | null,
  countries: string | null,
  areas: string | null,
  municipalities: string | null
): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement
  const data: DataBody = {
    name,
    description,
    active: String(active),
    show_on_signup: String(showOnSignUp)
  }

  if (parentID != null) {
    data.parent_id = parentID
  }
  if (countries != null) {
    data.countries = countries
  }
  if (areas != null) {
    data.areas = areas
  }
  if (municipalities != null) {
    data.municipalities = municipalities
  }

  const body = convertDictToBody(data)
  const response = await fetch('/api/organization/' + organizationID, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body
  })

  return response
}

export async function sendCreatePostRequest (title: string, content: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement
  const data: DataBody = {
    title,
    content
  }

  const body = convertDictToBody(data)
  const response = await fetch('/api/feed/post', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body
  })

  return response
}

export async function sendFetchCalendarsRequest (): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/calendars', {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  })

  return response
}

export async function sendCreateCalendarRequest (description: string | null, url: string | null): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement
  const data: DataBody = {}

  if (description !== null) {
    data.description = description
  }
  if (url !== null) {
    data.url = url
  }

  const body = convertDictToBody(data)

  const response = await fetch('/api/calendar', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body
  })

  return response
}

export async function sendUpdateCalendarRequest (id: string, description: string | null, url: string | null): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement
  const data: DataBody = {}

  if (description !== null) {
    data.description = description
  }
  if (url !== null) {
    data.url = url
  }

  const body = convertDictToBody(data)

  const response = await fetch('/api/calendar/' + id, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body
  })

  return response
}

export async function sendDeleteCalendarRequest (id: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/calendar/' + id, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  })

  return response
}

export async function sendHealthRequest (): Promise<Response> {
  const response = await fetch('/api/health', {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'same-origin',
    redirect: 'follow',
    referrerPolicy: 'no-referrer'
  })

  return response
}

export async function sendCreateUserRequest (user: UserData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/user', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(user),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateUserRequest (user: UserData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/user/' + user.id, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(user),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUsersRequest (organizationIDs: string[], includeBots: boolean = false): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  let urlArgument = ''

  if (organizationIDs.length > 0) {
    urlArgument = '?organization_ids='

    organizationIDs.forEach((organizaitonID, index) => {
      if (index > 0) {
        urlArgument += ','
      }

      urlArgument += organizaitonID
    })
  }

  if (includeBots) {
    if (urlArgument.length === 0) {
      urlArgument += '?'
    } else {
      urlArgument += '&'
    }

    urlArgument += 'bots=true'
  }

  const response = await fetch('/api/users' + urlArgument, {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendRolesRequest (): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/roles', {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUserRolesRequest (): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/user/roles', {
    method: 'GET',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendCreateRoleRequest (role: RoleData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/role', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(role),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendUpdateRoleRequest (role: RoleData): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/role/' + role.id, {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    body: JSON.stringify(role),
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendDeleteRoleRequest (roleID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch('/api/role/' + roleID, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}

export async function sendCreateUserRoleRequest (roleID: string, userID: string, orgID: string | null): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement
  const data: DataBody = {
    role_id: roleID,
    user_id: userID
  }

  if (orgID !== null) {
    data.organization_id = orgID
  }

  const response = await fetch('/api/user/role', {
    method: 'POST',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin',
    body: JSON.stringify(data)
  })

  return response
}

export async function sendDeleteUserRoleRequest (userRoleID: string): Promise<Response> {
  const xsrf = document.getElementsByName('_xsrf')[0] as HTMLInputElement

  const response = await fetch(`/api/user/role/${userRoleID}`, {
    method: 'DELETE',
    cache: 'no-cache',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json',
      'X-XSRFToken': xsrf.value
    },
    redirect: 'error',
    referrerPolicy: 'same-origin'
  })

  return response
}
