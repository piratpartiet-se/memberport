import { type GeoData, GEO_TYPES, getNodeType } from './geodata'
import { sendUpdateCountryDataRequest, sendUpdateAreaDataRequest, sendUpdateMunicipalityDataRequest } from '../api'
import { createMessage } from '../ui'

export function sendChangeNameRequest (id: string, geodata: Record<string, GeoData>): void {
  const node = document.getElementById(id)
  const newNameInput = document.getElementById('newName') as HTMLInputElement

  if (node === null || newNameInput === null) {
    return
  }

  const newName = newNameInput.value

  const nodeType = getNodeType(id, geodata)
  geodata[id].name = newName

  if (nodeType === GEO_TYPES.COUNTRY) {
    renameCountry(id, newName, geodata)
  } else if (nodeType === GEO_TYPES.AREA) {
    renameArea(id, newName, geodata)
  } else if (nodeType === GEO_TYPES.MUNICIPALITY) {
    renameMunicipality(id, newName, geodata)
  }
}

function changeNodeName (id: string, newName: string, geodata: Record<string, GeoData>): boolean {
  const node = document.getElementById(id)

  if (node === null) {
    return false
  }

  geodata[id].name = newName
  const nameBox = node.getElementsByClassName('content')[0] as HTMLElement
  nameBox.innerText = newName
  return true
}

function renameCountry (id: string, newName: string, geodata: Record<string, GeoData>): void {
  const oldName = geodata[id].name
  geodata[id].name = newName

  sendUpdateCountryDataRequest(geodata[id])
    .then(async (response: Response) => {
      return await response.json()
    })
    .then((data: Record<string, any>) => {
      if (data.success === false) {
        geodata[id].name = oldName
        throw new Error(data.reason)
      } else if (!changeNodeName(id, newName, geodata)) {
        geodata[id].name = oldName
        throw new Error('NODE NOT FOUND')
      }

      createMessage('Ändrat namn på landet till: ' + newName, 'is-success')
    }).catch((error: string) => {
      geodata[id].name = oldName
      console.error('Error:', error)
      createMessage('Någonting gick fel när namnet på landet skulle uppdateras', 'is-danger')
    })
}

function renameArea (id: string, newName: string, geodata: Record<string, GeoData>): void {
  const oldName = geodata[id].name
  geodata[id].name = newName

  sendUpdateAreaDataRequest(geodata[id])
    .then(async function (response: Response) {
      return await response.json()
    })
    .then(function (data: Record<string, any>) {
      if (data.success === false) {
        geodata[id].name = oldName
        throw new Error(data.reason)
      } else if (!changeNodeName(id, newName, geodata)) {
        geodata[id].name = oldName
        throw new Error('NODE NOT FOUND')
      }

      createMessage('Ändrat namn på området till: ' + newName, 'is-success')
    }).catch((error: string) => {
      geodata[id].name = oldName
      console.error('Error:', error)
      createMessage('Någonting gick fel när namnet på området skulle uppdateras', 'is-danger')
    })
}

function renameMunicipality (id: string, newName: string, geodata: Record<string, GeoData>): void {
  const oldName = geodata[id].name
  geodata[id].name = newName

  sendUpdateMunicipalityDataRequest(geodata[id])
    .then(async function (response: Response) {
      return await response.json()
    })
    .then(function (data: Record<string, any>) {
      if (data.success === false) {
        geodata[id].name = oldName
        throw new Error(data.reason)
      } else if (!changeNodeName(id, newName, geodata)) {
        geodata[id].name = oldName
        throw new Error('NODE NOT FOUND')
      }

      createMessage('Ändrat namn på kommunen till: ' + newName, 'is-success')
    }).catch((error: string) => {
      geodata[id].name = oldName
      console.error('Error:', error)
      createMessage('Någonting gick fel när namnet på kommunen skulle uppdateras', 'is-danger')
    })
}
