import { type OrgData } from '../organization/orgdata'

export class RoleData {
  id: string
  name: string
  description: string
  created: string
  permissions: PermissionData[]
}

export class RoleUserData {
  id: string
  role_id: string
  user_id: string
  organization: OrgData
}

export class PermissionData {
  id: string
  name: string
}
