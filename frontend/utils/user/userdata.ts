export class UserData {
  id: string
  schema: string
  number: string
  name: {
        first: string
        last: string
    }

  email: string
  phone: string
  postal_address: {
        street: string
        postal_code: string
        city: string
    }

  municipality_id: string
  gender: string
  birthday: string
  verified: string
  created: string

  constructor (
    id: string,
    schema: string,
    number: string,
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
    street: string,
    postalCode: string,
    city: string,
    municipalityID: string,
    gender: string,
    birthday: string,
    verified: string,
    created: string
  ) {
    this.id = id
    this.schema = schema
    this.number = number
    this.name = {
      first: firstName,
      last: lastName
    }
    this.email = email
    this.phone = phone
    this.postal_address = {
      street,
      postal_code: postalCode,
      city
    }
    this.municipality_id = municipalityID
    this.gender = gender
    this.birthday = birthday
    this.verified = verified
    this.created = created
  }
}
