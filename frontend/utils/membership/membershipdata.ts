export class MembershipData {
  id: string
  user_id: string
  organization_id: string
  created: string
  renewal: string
}
