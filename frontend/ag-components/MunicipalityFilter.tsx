import { type IDoesFilterPassParams } from '@ag-grid-community/core'
import { type CustomFilterProps, useGridFilter } from '@ag-grid-community/react'
import React, { useCallback } from 'react'

export interface IMunicipalityFilterModel {
  municipalityIDs: Record<string, boolean>
}

export default ({ model }: CustomFilterProps): React.JSX.Element => {
  const doesFilterPass = useCallback(
    (params: IDoesFilterPassParams) => {
      const { node } = params

      if (model === null || !('municipalityID' in node.data)) {
        return true
      }

      const municipalityModel: IMunicipalityFilterModel = model
      const value: string = node.data.municipalityID

      return municipalityModel.municipalityIDs[value]
    },
    [model]
  )

  // Register filter handler with the grid
  useGridFilter({
    doesFilterPass
  })

  return (
    <div className="ag-filter-wrapper ag-filter-body-wrapper ag-simple-filter-body-wrapper municipality-filter">
      <div>
        Filtrerar på kommuner medlemmar tillhör.
      </div>
    </div>
  )
}
