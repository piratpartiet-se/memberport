import json
import random
import requests
from faker import Faker

response = requests.get('http://127.0.0.1:8888/kratos/self-service/login/api', timeout=10)

json_body = json.loads(response.content)

headers = {
    'Accept': 'application/json'
}

data = {
    'method': 'password',
    'identifier': 'admin@piratpartiet.se',
    'password': 'test1234',
}

response = requests.post('http://127.0.0.1:8888/kratos/self-service/login?flow=' + json_body["id"], headers=headers, data=data, timeout=10)

json_body = json.loads(response.content)

session_token = "Bearer " + json_body["session_token"]
print(session_token)

headers = {
    'Content-Type': 'application/json',
    'Authentication': session_token
}

fake = Faker(['sv_SE', 'cs_CZ'])

for i in range(0, 5000):
    first = fake.first_name()
    last = fake.last_name()
    email = first + "." + last + str(random.randint(0, 9)) + "@piratpartiet.se"  # noqa: S311
    phone = fake.phone_number()

    address = fake.address()

    data = {
        "schema": "member",
        "name": {
            "first": first,
            "last": last
        },
        "email": email,
        "postal_address": {
            "street": fake.street_address(),
            "postal_code": fake.postcode(),
            "city": fake.city()
        },
        "municipality_id": str(random.randint(1, 220)),  # noqa: S311
        "gender": random.choice(["male", "female", "other", "unknown"]),  # noqa: S311
        "birthday": str(fake.date_of_birth(minimum_age=12, maximum_age=100))
    }

    response = requests.post('http://127.0.0.1:8888/api/user', headers=headers, json=data, timeout=10)

    print(f"{i} request: {response.status_code} {response.reason}")
